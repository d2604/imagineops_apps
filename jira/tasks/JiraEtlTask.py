import json
import os
import time
import traceback
from datetime import datetime, timedelta
from ImagineOpsBaseTask import ImagineOpsBaseTask


class JiraEtlTask(ImagineOpsBaseTask):
    '''
    JiraEtlTask is responsible for extracting facts from Jira.
    Currently supports:

    - issues
    - releases
    - sprints
    - worklogs
    '''
    def __init__(self, hub):
        super(JiraEtlTask, self).__init__(hub)
        self.app_name = "imagineops_jira"
        self._init_cache("status")
        self._init_cache("priority")
        self.late_threshold = 0 if "lateThreshold" not in self.hub.config["properties"] else self.hub.config["properties"]["lateThreshold"]
        self.database = self.hub.connections["database"].new_connection()

    def _check_dependencies(self):
        return super()._check_dependencies() or len(self.hub.get_connections_by_type("jira")) > 0

    ###########################################################################
    # task_runner.BaseTask API
    ###########################################################################

    def do_discovery(self):
        self.discover_projects()
        self.discover_components()
        self.discover_dimension("issue_type")
        self.discover_dimension("priority")
        self.discover_dimension("resolution")
        self.discover_dimension("status")
        self._init_cache("status")
        self.discover_fields()
        self.discover_users()

    def do_hourly(self):
        self.synchronize_issues()
        self.synchronize_sprints()

    def do_daily(self):
        self.do_discovery()
        self.synchronize_issues(sync_mode="Open")
        self.synchronize_releases(do_scope=True)
        self.synchronize_worklogs()
        self.discover_avatars()

    def do_monday(self):
        self.fix_orphan_issues()
        self.synchronize_issues(sync_mode="updated >= -30d")

    def do_test(self):
        self.do_discovery()
        self.do_hourly()
        self.do_daily()
        self.discover_avatars()
        self.synchronize_releases(do_scope=True)
        self.synchronize_sprints()
        self.fix_orphan_issues()

    def do_bespoke(self):
        if "task" not in self.args:
            return
        if self.args["task"] == "issues":
            sync_mode = "Change" if "sync_mode" not in self.args else self.args["sync_mode"]
            self.synchronize_issues(sync_mode=sync_mode)
        elif self.args["task"] == "releases":
            self.synchronize_releases()
        elif self.args["task"] == "sprints":
            self.synchronize_sprints()
        elif self.args["task"] == "worklogs":
            self.synchronize_worklogs()

    ###########################################################################
    # internal helper functions
    ###########################################################################

    def _parse_issue_history(self, connection, issue: dict, sprint_name: str = None, release_name: str = None) -> dict:  # noqa: C901
        '''
        Parse issue history for key dates & time in status

        Args:
            connection     :                    Connection to target Jira server
            issue(dict):                        Jira issue dictionary
            sprint_name(string):                Sprint to determine sprint_scoped_at date
            release_name(string):               Release to determine release_scoped_at date
        Returns:
            dictionary

        .. csv-table::
            :header: "Property", "Description"

            "closer","User who last closed the issue"
            "assigned_at","Latest date the issue was assigned"
            "started_at","Latest date the issue was transitioned to In Progress"
            "reopened_at","Latest date the issue was re-opened"
            "ended_at","Latest date the issue was closed"
            "release_scoped_at","Date issue was scoped to specified release"
            "sprint_scoped_at","Date issue was scoped to specified sprint"
            "open_hours","Number of hours the issue has been in an open state"
            "active_hours","Number of hours the issue has been in an In Progress state"
            "blocked_hours","Number of hours the issue has been in a blocked state"
            "review_hours","Number of hours the issue has been in a Reviewing state"
            "test_hours","Number of hours the issue has been in a Testing state"
            "slack","Number of hours from when the issue was created until it was in progress"
            "attempts","Number of times the issue has been re-opened"
            "recidivism","Number of times the issue transitioned backwards from the default flow"
        '''
        closer = assignee = None
        attempts = 1
        assigned_at = started_at = ended_at = implemented_at = reviewed_at = tested_at = built_at = reopened_at = sprint_scoped_at = release_scoped_at = None
        previous_block = previous_review = previous_test = previous_build = previous_active = None
        open_hours = active_hours = review_hours = test_hours = build_hours = blocked_hours = slack = recidivism = 0
        previous_open = previous_slack = datetime.strptime(issue["fields"]["created"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
        sprint_field, _ = connection.get_custom_field("Sprint")

        try:
            url = f"""/issue/{issue["key"]}/changelog"""
            params = {"maxResults": 1000}
            # hacks for Jira Server vs Cloud
            if connection.connection_config["properties"]["deploymentType"] == "Server":
                url = f"""issue/{issue["key"]}"""
                params = {"expand": "changelog"}
            http_response = connection.call_rest_api(url, params=params)
            change_response = json.loads(http_response.text)

            # hacks for Jira Server vs Cloud
            changes = []
            if connection.connection_config["properties"]["deploymentType"] == "Server":
                if "changelog" in change_response:
                    changes = change_response["changelog"]["histories"]
            else:
                changes = change_response["values"]

            for change in changes:
                for item in change["items"]:
                    if item["field"] == "assignee":
                        assignee = item["toString"]
                        # determine when the issue was first assigned
                        if item["from"] is None and assigned_at is None:
                            assigned_at = change["created"][0:19].replace("T", " ")
                    elif item["field"] == "status":
                        # calculate the time in given status based on issue transition history
                        slack, previous_slack, _ = self._analyze_state(item, change, self.cache["status"]["Open"], slack, previous_slack)
                        blocked_hours, previous_block, _ = self._analyze_state(item, change, self.cache["status"]["Blocked"], blocked_hours, previous_block)
                        review_hours, previous_review, reviewed_at = self._analyze_state(item, change, self.cache["status"]["Reviewing"], review_hours, previous_review, reviewed_at)
                        test_hours, previous_test, tested_at = self._analyze_state(item, change, self.cache["status"]["Testing"], test_hours, previous_test, tested_at)
                        build_hours, previous_build, built_at = self._analyze_state(item, change, self.cache["status"]["Building"], build_hours, previous_build, built_at)
                        active_hours, previous_active, implemented_at = self._analyze_state(item, change, self.cache["status"]["In Progress"], active_hours, previous_active, implemented_at)

                        # determine whether the state transitioned "backwards"
                        from_state = self._map_status_to_workflow(item["fromString"])
                        to_state = self._map_status_to_workflow(item["toString"])
                        if to_state < from_state:
                            recidivism += 1

                        # open_hours is calculated inversely by time NOT in Closed state
                        if item["toString"].lower() in self.cache["status"]["Closed"] and previous_open is not None:
                            open_hours += ((datetime.strptime(change["created"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S") - previous_open).total_seconds() / 3600.0)
                            previous_open = None
                            closer = assignee or connection.get_username_from_field(change["author"])
                        if item["fromString"].lower() in self.cache["status"]["Closed"] and item["toString"].lower() not in self.cache["status"]["Closed"]:
                            previous_open = datetime.strptime(change["created"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")

                        # determine ended_at
                        if item["toString"].lower() in self.cache["status"]["Closed"]:
                            ended_at = datetime.strptime(change["created"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
                    elif item["field"] == "resolution":
                        if closer is None:
                            closer = assignee or connection.get_username_from_field(change["author"])
                        if item["to"] is None:
                            attempts += 1
                            if not reopened_at:
                                reopened_at = change["created"][0:19].replace("T", " ")
                    elif item["field"] == "fixVersions":
                        if release_name is not None:
                            if item["toString"].lower() == release_name.lower():
                                release_scoped_at = change["created"][0:19].replace("T", " ")
                    elif "fieldId" in item:
                        if sprint_name is not None and item["fieldId"] == sprint_field["id"]:
                            if item["toString"].lower() == sprint_name.lower():
                                sprint_scoped_at = change["created"][0:19].replace("T", " ")
            # handle hours for unclosed states using the min of resolutiondate OR now
            d = datetime.today() if issue["fields"]["resolutiondate"] is None else datetime.strptime(issue["fields"]["resolutiondate"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")

            # finalize hours in state for states that are still active
            if previous_slack is not None:
                slack += ((d - previous_slack).total_seconds() / 3600.0)
            if previous_block is not None:
                blocked_hours += ((d - previous_block).total_seconds() / 3600.0)
            if previous_review is not None:
                review_hours += ((d - previous_review).total_seconds() / 3600.0)
            if previous_test is not None:
                test_hours += ((d - previous_test).total_seconds() / 3600.0)
            if previous_build is not None:
                build_hours += ((d - previous_build).total_seconds() / 3600.0)
            if previous_active is not None:
                active_hours += ((d - previous_active).total_seconds() / 3600.0)
            if previous_open is not None:
                open_hours += ((d - previous_open).total_seconds() / 3600.0)
        except Exception as e:
            self.hub.logger.warn("Error analysing Jira issue history")
            self.hub.logger.trace(str(e))
            self.hub.logger.trace(traceback.format_exc())

        return {
            "closer": closer,
            "assigned_at": assigned_at,
            "started_at": started_at,
            "implemented_at": implemented_at,
            "reviewed_at": reviewed_at,
            "tested_at": tested_at,
            "built_at": built_at,
            "ended_at": ended_at,
            "reopened_at": reopened_at,
            "release_scoped_at": release_scoped_at,
            "sprint_scoped_at": sprint_scoped_at,
            "open_hours": open_hours,
            "active_hours": active_hours,
            "blocked_hours": blocked_hours,
            "review_hours": review_hours,
            "test_hours": test_hours,
            "build_hours": build_hours,
            "slack": slack,
            "attempts": attempts,
            "recidivism": recidivism
        }

    def _analyze_state(self, item: dict, change: dict, status_map: list, hours_in_state: int, previous_timestamp: datetime, max_timestamp: datetime = None) -> tuple:
        '''Calculates the time in a specific status'''
        change_time = datetime.strptime(change["created"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
        if item["fromString"].lower() in status_map:
            if previous_timestamp is not None:
                hours_in_state += ((change_time - previous_timestamp).total_seconds() / 3600.0)
            max_timestamp = change_time if max_timestamp is None or change_time > max_timestamp else max_timestamp
            previous_timestamp = None
        if item["toString"].lower() in status_map:
            previous_timestamp = change_time
        return hours_in_state, previous_timestamp, max_timestamp

    def _get_issue_jql(self, connection, sync_mode: str = "Changed") -> str:
        '''Generates JQL for issue search'''
        jql = connection.get_project_jql()[4:]

        if sync_mode == "Open":
            # refresh all open issues
            jql += " AND statuscategory not in (Done)"
        elif sync_mode == "All":
            jql += ""
        elif sync_mode.find("=") > -1:
            jql += f" AND {sync_mode}"
        else:
            # only fetch issues since last successful load
            fake_url = connection.get_issue_url("")
            sql = f"""SELECT COALESCE(MAX(updated_at) - INTERVAL '2 DAYS', '{self.hub.config["properties"]["minSyncDate"]}') FROM issues WHERE url LIKE '{fake_url}%';"""
            result = self.hub.connections["database"].connection.execute(sql)
            row = result.fetchone()
            since = row[0]
            jql += f""" AND updated >= {since.strftime("%Y-%m-%d")}"""

        # filter by projects
        jql += " ORDER BY updated ASC"
        return jql

    def _is_sprint_active(self, sprint: str) -> bool:
        '''
        Determine if sprint is active
        '''
        is_sprint_active = None
        sql = f"""SELECT is_open FROM sprints WHERE name = '{sprint["name"]}';"""
        result = self.hub.connections["database"].connection.execute(sql)
        row = result.fetchone()
        if row is not None:
            is_sprint_active = True if row[0] == "1" else False
        return is_sprint_active

    def _map_status_to_workflow(self, status: str) -> int:
        workflow = 0
        if status.lower() in self.cache["status"]["Open"]:
            workflow = 1
        elif status.lower() in self.cache["status"]["In Progress"]:
            workflow = 2
        elif status.lower() in self.cache["status"]["Reviewing"]:
            workflow = 3
        elif status.lower() in self.cache["status"]["Testing"]:
            workflow = 4
        elif status.lower() in self.cache["status"]["Building"]:
            workflow = 5
        elif status.lower() in self.cache["status"]["Deploying"]:
            workflow = 6
        elif status.lower() in self.cache["status"]["Closed"]:
            workflow = 7
        return workflow

    def _update_sprint_grade(self, sprint: dict):
        '''
        Determine Sprint quality

        Args:
            sprint(dict):                   Sprint dictionary
        '''
        sql = f"""SELECT story_points, story_points_done, story_points_churn, team, started_at FROM sprints WHERE url = '{sprint["url"]}';"""
        result = self.hub.connections["database"].connection.execute(sql)
        row = result.fetchone()
        if row is None:
            return
        # use max to avoid div/zero bugs
        story_points_goal = max(row[0], 1)
        story_points = row[1]
        story_points_added = row[2]
        team = row[3]
        started_at = row[4]

        # get average velocity
        sql = """
            SELECT COALESCE(AVG(story_points), 0)
            FROM sprints
            WHERE name IN (
                SELECT name FROM sprints s2 WHERE s2.status IN ('Closed') AND s2.team = {{ team }} AND s2.started_at < {{ started_at }} ORDER BY s2.ended_at DESC LIMIT 6
            );"""
        sql, bind_parameters = self.hub.connections["database"].sqlize(sql, {"team": team, "started_at": started_at})
        result = self.hub.connections["database"].connection.execute(sql, *bind_parameters)
        velocity = max(float(result.fetchone()[0]), 1)

        score = 10
        # not enough complete
        if 100.0 * (story_points / story_points_goal) <= 50:
            score += -8
        elif 100.0 * (story_points / story_points_goal) <= 60:
            score += -5
        elif 100.0 * (story_points / story_points_goal) <= 70:
            score += -3
        elif 100.0 * (story_points / story_points_goal) <= 80:
            score += -1

        # too much churn?
        if 100.0 * (story_points_added / story_points_goal) >= 50:
            score += -4
        elif 100.0 * (story_points_added / story_points_goal) >= 30:
            score += -2
        elif 100.0 * (story_points_added / story_points_goal) >= 20:
            score += -1

        # is velocity down?
        if velocity > 10:
            if 100.0 * (story_points / velocity) <= 50:
                score += -3
            elif 100.0 * (story_points / velocity) <= 70:
                score += -2
            elif 100.0 * (story_points / velocity) <= 80:
                score += -1

            # is velocity up?
            if 100.0 * (story_points / velocity) > 150:
                score += 3
            if 100.0 * (story_points / velocity) > 120:
                score += 2

        # evaluate score
        grade = self.hub.helpers["GradingHelper"]._get_grade(score)
        sql = f"""UPDATE sprints SET grade = '{grade}', rag = CASE WHEN '{grade}' IN ('A', 'B') THEN 'g' WHEN '{grade}' IN ('C', 'D') THEN 'a' WHEN '{grade}' IN ('E', 'F') THEN 'r' ELSE ' ' END WHERE url = '{sprint["url"]}';"""
        self.database.execute(sql)
        self.database.commit()

    ###########################################################################
    # Tasks
    ###########################################################################

    def discover_avatars(self):
        '''Discover Jira user avatars'''
        self.hub.logger.info("discover_avatars started")
        start = time.monotonic()
        events = 0
        # find all users without an avatar
        sql = """
            SELECT d.dimension
            FROM dimensions d
            INNER JOIN dimension_attributes da ON
                d.dimension_type = da.dimension_type
                AND d.dimension = da.dimension
                AND da.attribute_type = 'avatar'
            WHERE
                d.dimension_type = 'user'
                AND d.dimension = d.alias
                AND d.status = 'active'
                AND COALESCE(da.attribute, '') IN ('');"""
        results = self.hub.connections["database"].connection.execute(sql).fetchall()
        for result in results:
            events += 1 if self.discover_avatar(result[0]) else 0
        self.hub.logger.perf("discover_avatars done", events, time.monotonic() - start)

    def discover_avatar(self, user: str) -> bool:
        # find all possible aliases for user
        avatar_path = ""
        possible_user_keys = ["emailAddress", "key", "displayName", "name", "accountId"]
        sql = "SELECT dimension FROM dimensions WHERE dimension_type = 'user' AND alias = {{ user }};"
        sql, bind_parameters = self.hub.connections["database"].sqlize(sql, {"user": user})
        aliases = self.hub.connections["database"].connection.execute(sql, *bind_parameters).fetchall()
        for row in aliases:
            alias = row[0]
            for connection in self.hub.get_connections_by_type("jira"):
                # we cannot access users from the user api, need to work around by finding assigned/reported issues
                try:
                    jql = f"""assignee = \"{user}\" or reporter = \"{user}\""""
                    jira_issues = connection.search_issues(jql)
                    if len(jira_issues) == 0:
                        continue
                    # check assignee
                    jira_user = jira_issues[0].fields.reporter
                    is_correct_user = False
                    for possible_user_key in possible_user_keys:
                        if jira_user is not None and possible_user_key in jira_user.raw and jira_user.raw[possible_user_key] == alias:
                            is_correct_user = True
                            break
                    if not is_correct_user:
                        jira_user = jira_issues[0].fields.assignee
                    avatar = jira_user.raw["avatarUrls"]["48x48"]

                    # download avatar
                    avatar_filename = user.replace("@", "_").replace(" ", "_").replace(".", "_")
                    avatar_path = os.path.join(self.path, "ext", "wwwroot", "images", "avatars", f"{avatar_filename}.png")
                    # self.download_artifact(avatar, avatar_path)
                    response = connection.call_rest_api("", url=avatar, enable_stream=False)
                    # Jira injects a default avatar as an SVG, otherwise its a PNG
                    if response.headers["Content-Type"].find("svg") > -1:
                        continue
                    with open(avatar_path, "wb") as f:
                        f.write(response.content)

                    self.synchronize_dimension_attribute("user", user, "avatar", f"/ext/images/avatars/{avatar_filename}.png")
                    self.synchronize_dimension_attribute("user", user, "is_jira", "true")
                    if hasattr(jira_user, "jiraAccountID"):
                        self.synchronize_dimension_attribute("user", user, "jiraAccountID", jira_user.accountId)
                    return True
                except Exception as e:
                    # users is restricted to admins; safe to ignore as we can discover users from issues as a fail-safe
                    self.hub.logger.warn("Unable to discover users from Jira")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
                    continue
        return False

    def discover_components(self):
        '''Discover Jira components'''
        self.hub.logger.info("discover_components started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("jira"):
            projects = connection.get_project_keys()
            for project in projects:
                self.hub.logger.info(f"Synchronizing {project}")
                jira_response = connection.call_rest_api(f"project/{project}/components")
                components = json.loads(jira_response.text)
                for component in components:
                    events += 1
                    self.hub.logger.debug(f"""Discovering component {component["name"]}""")
                    self.synchronize_dimension("component", component["name"][:255])
                    self.synchronize_dimension_attribute("component", component["name"][:255], "is_jira", "true")
        self.hub.logger.perf("discover_components done", events, time.monotonic() - start)

    def discover_dimension(self, jira_dimension_type: str, dimension_type: str = ""):
        '''
        Discover Jira dimensions

        Args:
            jira_dimension_type(string):        Jira API to access dimension data
            dimension_type(string):             ImagineOps dimension type
        '''
        dimension_type = dimension_type if dimension_type != "" else jira_dimension_type
        self.hub.logger.info(f"discover_dimension {dimension_type} started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("jira"):
            jira_response = connection.call_rest_api(jira_dimension_type.replace("_", ""))
            dimensions = json.loads(jira_response.text)
            for dimension in dimensions:
                self.hub.logger.debug(f"""Discovering {dimension_type} {dimension["name"]}""")
                self.synchronize_dimension(dimension_type, dimension["name"][:255])
                self.synchronize_dimension_attribute(dimension_type, dimension["name"][:255], "is_jira", "true")
                events += 1
        self.hub.logger.perf(f"discover_dimension {dimension_type} done", events, time.monotonic() - start)

    def discover_fields(self):
        '''Discover Jira fields'''
        self.hub.logger.info("discover_fields started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("jira"):
            try:
                jira_response = connection.call_rest_api("field")
                fields = json.loads(jira_response.text)
                for field in fields:
                    self.hub.logger.debug(f"""Discovering field {field["id"]}""")
                    self.synchronize_dimension("field", field["id"], field["name"])
                    self.synchronize_dimension_attribute("field", field["id"], "is_jira", "true")
                    events += 1
            except Exception as e:
                self.hub.logger.warn("Unable to discover fields from Jira")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf("discover_fields done", events, time.monotonic() - start)

    def discover_projects(self):
        '''Discover Jira projects'''
        self.hub.logger.info("discover_projects started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("jira"):
            jira_projects = connection.projects()
            for jira_project in jira_projects:
                self.hub.logger.debug(f"Discovering project {jira_project.key}")
                self.synchronize_dimension("project", jira_project.name[:255])
                self.synchronize_dimension("project", jira_project.key)
                self.synchronize_dimension_attribute("project", jira_project.key, "is_jira", "true")
                events += 1
        self.hub.logger.perf("discover_projects done", events, time.monotonic() - start)

    def discover_users(self):
        '''Discover Jira users'''
        self.hub.logger.info("discover_users started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("jira"):
            try:
                jira_response = connection.call_rest_api("users")
                dimensions = json.loads(jira_response.text)
                for dimension in dimensions:
                    # safe to ignore system and inactive accounts
                    if dimension["accountType"] in ["app"] or not dimension["active"]:
                        continue
                    username = dimension["displayName"]
                    if "emailAddress" in dimension:
                        username = dimension["emailAddress"]
                    self.hub.logger.debug(f"Discovering user {username}")
                    self.synchronize_dimension("user", username)
                    self.synchronize_dimension_attribute("user", username, "is_jira", "true")
                    events += 1
            except Exception as e:
                # users is restricted to admins; safe to ignore as we can discover users from issues as a fail-safe
                self.hub.logger.warn("Unable to discover users from Jira")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf("discover_users done", events, time.monotonic() - start)

    def fix_orphan_issues(self):
        '''Find Jira issues moved or deleted'''
        self.hub.logger.info("fix_orphan_issues started")
        for connection in self.hub.get_connections_by_type("jira"):
            if not self._is_fact_enabled(connection.connection_config, "issues"):
                continue
            self.fix_orphan_issues_for_connection(connection)
        self.hub.logger.info("fix_orphan_issues done")

    def fix_orphan_issues_for_connection(self, connection):
        sql = f"""
            SELECT issue_id
            FROM issues
            WHERE
                COALESCE(alias_id, '') = ''
                AND updated_at BETWEEN now() - INTERVAL '365 days' AND now()
                AND connection = '{connection.connection_config["id"]}';"""
        results = self.hub.connections["database"].connection.execute(sql)
        for result in results.fetchall():
            usql = ""
            try:
                jira_issue = connection.issue(result[0])
                if jira_issue.key != result[0]:
                    usql = f"""
                        UPDATE issues SET
                            alias_id = '{jira_issue.key}',
                            status = 'Closed',
                            is_active = false,
                            is_open = false,
                            resolution = 'Duplicate'
                        WHERE issue_id = '{result[0]}';"""
            except Exception as e:
                # issue was deleted
                usql = f"""
                    UPDATE issues SET
                        alias_id = '{result[0]}',
                        status = 'Closed',
                        is_active = false,
                        is_open = false,
                        resolution = 'Invalid'
                    WHERE issue_id = '{result[0]}';"""

            if usql != "":
                self.hub.connections["database"].connection.execute(usql)
                self.hub.connections["database"].connection.commit()

    def synchronize_issues(self, sync_mode: str = "Changed"):
        '''Synchronize Jira issues'''
        self.hub.logger.info("synchronize_issues started")
        for connection in self.hub.get_connections_by_type("jira"):
            if not self._is_fact_enabled(connection.connection_config, "issues"):
                continue
            if sync_mode in ["Known", "TeamBackfill"]:
                sql = f"""SELECT issue_id FROM issues WHERE url LIKE '%{connection.connection_config["server"]}%'"""
                results = self.hub.connections["database"].connection.execute(sql)
                for result in results.fetchall():
                    self.synchronize_issues_for_connection(connection, f"issue={result[0]}")
            else:
                self.synchronize_issues_for_connection(connection, sync_mode)
        self.hub.logger.info("synchronize_issues done")

    def synchronize_issues_for_connection(self, connection, sync_mode: str = "Changed", max_results: int = None, start_at: int = 0, jql_override: str = None):
        '''Synchronize Jira issues from connection'''
        self.hub.logger.info(f"""Synchronizing {connection.connection_config["server"]}""")
        start = time.monotonic()
        events = 0

        # synchronize issues since last update
        max_results = max_results if max_results is not None else connection.connection_config["properties"]["maxResults"]
        jql = jql_override if jql_override is not None else self._get_issue_jql(connection, sync_mode)
        jira_issues = connection.search_api(jql, max_results=max_results, start_at=start_at)

        for jira_issue in jira_issues:
            events += 1
            issue = self.parse_issue(connection, jira_issue)
            self.synchronize_issue(issue)
            self.synchronize_issue_properties(connection, issue)

        # use recursion to process all issues
        self.hub.logger.perf("synchronize_issues_for_connection done", events, time.monotonic() - start)
        if events == max_results:
            start_at = start_at + max_results
            self.synchronize_issues_for_connection(connection, sync_mode, max_results, start_at)

    def parse_issue(self, connection, issue: dict) -> dict:
        '''Parse Jira issue'''
        url = connection.get_issue_url(issue["key"])
        resolution = None if issue["fields"]["resolution"] is None else self.hub.helpers["MetaHelper"].map_dimension("resolution", issue["fields"]["resolution"]["name"])
        priority = "P3" if issue["fields"]["priority"] is None else self.hub.helpers["MetaHelper"].map_dimension("priority", issue["fields"]["priority"]["name"])
        status = self.hub.helpers["MetaHelper"].map_dimension("status", issue["fields"]["status"]["name"])
        ended_at = None if issue["fields"]["resolutiondate"] is None else datetime.strptime(issue["fields"]["resolutiondate"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")

        # parse change history
        history_details = self._parse_issue_history(connection, issue)

        # jira is sometimes inconsistent on populating the resolutiondate field even though the issue is in a closed state
        # in this case, use history to determine ended_at
        if ended_at is None and status == "Closed":
            ended_at = history_details["ended_at"]
        # ended_at must be none if issue is still open!
        if status != "Closed" and ended_at is not None:
            ended_at = None

        # determine story points
        story_points = 0
        _, issue_field = connection.get_custom_field("Story Points", issue)
        if issue_field is not None:
            if isinstance(issue_field, float):
                story_points = int(issue_field)

        _, issue_field = connection.get_custom_field("Epic Link", issue)
        epic_link = "" if issue_field is None else issue_field

        # hack for non-cloud Jira
        _, issue_field = connection.get_custom_field("Sprint", issue)
        sprint = ""
        sprint_attempts = 0
        if issue_field is not None:
            sprint_attempts = len(issue_field)
            if isinstance(issue_field[0], dict):
                if "name" in issue_field[0]:
                    sprint = issue_field[0]["name"]
            elif isinstance(issue_field[0], str):
                if issue_field[0].find("name=") > -1:
                    sprint = issue_field[0][issue_field[0].find("name=") + 5:]
                    sprint = sprint[:sprint.find(",")]

        is_late = False
        if ended_at and "duedate" in issue["fields"]:
            if issue["fields"]["duedate"] is not None:
                due_date = datetime.strptime(issue["fields"]["duedate"], "%Y-%m-%d")
                is_late = True if ended_at > due_date + timedelta(days=self.late_threshold) else False

        project = self.hub.helpers["MetaHelper"].map_dimension("project", issue["fields"]["project"]["name"])
        issue.update({
            "url": url,
            "connection": connection.connection_config["id"],
            "id": issue["key"],
            "parent_id": "" if "parent" not in issue["fields"] else issue["fields"]["parent"]["key"],
            "summary": issue["fields"]["summary"][:1024],
            "status": status[:64],
            "issue_type": self.hub.helpers["MetaHelper"].map_dimension("issue_type", issue["fields"]["issuetype"]["name"])[:32],
            "project": project,
            "priority": "P3" if priority not in ["P1", "P2", "P3", "P4"] else priority[:64],
            "resolution": None if resolution is None else resolution[:63],
            "username": connection.get_username_from_field(issue["fields"]["assignee"]),
            "reporter": connection.get_username_from_field(issue["fields"]["reporter"]),
            "closer": history_details["closer"],
            "team": self.hub.helpers["MetaHelper"].map_dimension("team", connection.get_team_from_issue(issue)[:64]),
            "component": "" if not issue["fields"]["components"] else self.hub.helpers["MetaHelper"].map_dimension("component", issue["fields"]["components"][0]["name"][:127]),
            "epic": epic_link,
            "sprint": sprint[:127],
            "release": "" if not issue["fields"]["fixVersions"] else issue["fields"]["fixVersions"][0]["name"][:128],
            "labels": ",".join(issue["fields"]["labels"])[:2047],
            "created_at": issue["fields"]["created"][0:19].replace("T", " "),
            "assigned_at": history_details["assigned_at"] or history_details["started_at"] or history_details["implemented_at"] or ended_at,
            "started_at": history_details["started_at"] or history_details["implemented_at"] or ended_at,
            "implemented_at": history_details["implemented_at"] or ended_at,
            "reviewed_at": history_details["reviewed_at"],
            "tested_at": history_details["tested_at"],
            "built_at": history_details["built_at"],
            "ended_at": ended_at,
            "due_at": None if "duedate" not in issue["fields"] else issue["fields"]["duedate"],
            "reopened_at": history_details["reopened_at"],
            "updated_at": issue["fields"]["updated"][0:19].replace("T", " "),
            "upvotes": 0 if "votes" not in issue["fields"] else issue["fields"]["votes"]["votes"],
            "downvotes": 0,
            "attempts": history_details["attempts"],
            "sprint_attempts": sprint_attempts,
            "recidivism": history_details["recidivism"],
            "effort_estimate": 0 if "originalEstimateSeconds" not in issue["fields"]["timetracking"] else issue["fields"]["timetracking"]["originalEstimateSeconds"] / 3600.0,
            "effort": 0 if "timeSpentSeconds" not in issue["fields"]["timetracking"] else issue["fields"]["timetracking"]["timeSpentSeconds"] / 3600.0,
            "story_points": story_points,
            "open_hours": history_details["open_hours"],
            "active_hours": history_details["active_hours"],
            "review_hours": history_details["review_hours"],
            "test_hours": history_details["test_hours"],
            "blocked_hours": history_details["blocked_hours"],
            "slack": history_details["slack"],
            "is_open": status not in ["Closed"],
            "is_active": status not in ["Open", "Blocked", "Closed", "Failed"],
            "is_noise": False if not resolution else not self.hub.helpers["MetaHelper"].is_valid_resolution(resolution),
            "is_incident": False,
            "is_reopened": history_details["attempts"] > 1,
            "is_priority": self.hub.helpers["MetaHelper"].is_high_priority(priority),
            "is_late": is_late
        })
        issue["grade"] = self.hub.helpers["GradingHelper"].get_issue_grade(issue)
        issue["rag"] = self.grade_to_rag_map[issue["grade"]]
        return issue

    def synchronize_issue(self, issue: dict):
        '''Synchronize Jira issue'''
        self.hub.logger.debug(f"""Synchronizing issue {issue["key"]}""")
        isql = """
            INSERT INTO issues(url, connection,
                issue_id, parent_id, summary,
                status, rag, grade,
                project, issue_type, priority, resolution,
                username, reporter, closer,
                team, component,
                epic, sprint, release, labels,
                created_at, assigned_at, started_at,
                implemented_at, reviewed_at, tested_at, built_at,
                ended_at, due_at, reopened_at, updated_at,
                upvotes, downvotes, attempts, sprint_attempts, recidivism,
                effort_estimate, effort, story_points,
                open_hours, active_hours, review_hours,
                test_hours, blocked_hours, slack,
                is_open, is_active, is_noise,
                is_priority, is_reopened, is_incident, is_late)
            VALUES ({{ url }}, {{ connection }},
                {{ id }}, {{ parent_id }}, {{ summary }},
                {{ status }}, {{ rag }}, {{ grade }},
                {{ project }}, {{ issue_type }}, {{ priority }}, {{ resolution }},
                {{ username }}, {{ reporter }}, {{ closer }},
                {{ team }}, {{ component }},
                {{ epic }}, {{ sprint }}, {{ release }}, {{ labels }},
                {{ created_at }}, {{ assigned_at }}, {{ started_at }},
                {{ implemented_at }}, {{ reviewed_at }}, {{ tested_at }}, {{ built_at }},
                {{ ended_at }}, {{ due_at }}, {{ reopened_at }}, {{ updated_at }},
                {{ upvotes }}, {{ downvotes }}, {{ attempts }}, {{ sprint_attempts }}, {{ recidivism }},
                {{ effort_estimate }}, {{ effort }}, {{ story_points }},
                {{ open_hours }}, {{ active_hours }}, {{ review_hours }},
                {{ test_hours }}, {{ blocked_hours }}, {{ slack }},
                {{ is_open }}, {{ is_active }}, {{ is_noise }},
                {{ is_priority }}, {{ is_reopened }}, {{ is_incident }}, {{ is_late }})
            ON CONFLICT (url) DO
            UPDATE SET
                issue_id = EXCLUDED.issue_id, parent_id = EXCLUDED.parent_id, summary = EXCLUDED.summary,
                status = EXCLUDED.status, rag = EXCLUDED.rag, grade = EXCLUDED.grade,
                project = EXCLUDED.project, issue_type = EXCLUDED.issue_type, priority = EXCLUDED.priority, resolution = EXCLUDED.resolution,
                username = EXCLUDED.username, reporter = EXCLUDED.reporter, closer = EXCLUDED.closer,
                team = EXCLUDED.team, component = EXCLUDED.component,
                epic = EXCLUDED.epic, sprint = EXCLUDED.sprint, release = EXCLUDED.release, labels = EXCLUDED.labels,
                created_at = EXCLUDED.created_at, assigned_at = EXCLUDED.assigned_at, started_at = EXCLUDED.started_at,
                implemented_at = EXCLUDED.implemented_at, reviewed_at = EXCLUDED.reviewed_at, tested_at = EXCLUDED.tested_at, built_at = EXCLUDED.built_at,
                ended_at = EXCLUDED.ended_at, due_at = EXCLUDED.due_at, updated_at = EXCLUDED.updated_at,
                upvotes = EXCLUDED.upvotes, downvotes = EXCLUDED.downvotes,
                attempts = EXCLUDED.attempts, sprint_attempts = EXCLUDED.sprint_attempts, recidivism = EXCLUDED.recidivism,
                effort_estimate = EXCLUDED.effort_estimate, effort = EXCLUDED.effort, story_points = EXCLUDED.story_points,
                open_hours = EXCLUDED.open_hours, active_hours = EXCLUDED.active_hours, review_hours = EXCLUDED.review_hours,
                test_hours = EXCLUDED.test_hours, blocked_hours = EXCLUDED.blocked_hours, slack = EXCLUDED.slack,
                is_open = EXCLUDED.is_open, is_active = EXCLUDED.is_active, is_noise = EXCLUDED.is_noise,
                is_priority = EXCLUDED.is_priority, is_reopened = EXCLUDED.is_reopened, is_incident = EXCLUDED.is_incident, is_late = EXCLUDED.is_late;"""
        self._upsert(isql, issue)

    def synchronize_issue_properties(self, connection, issue: dict):
        '''Synchronize Jira issue custom fields'''
        self.hub.logger.debug("synchronize_issue_properties started")
        start = time.monotonic()
        events = 0
        if "fields" not in connection.connection_config["properties"]:
            return

        isql = """
            INSERT INTO issue_properties(url, connection, issue_id, property, value)
            VALUES ({{ url }}, {{ connection }}, {{ id }}, {{ property }}, {{ value }})
            ON CONFLICT (url, property) DO
            UPDATE SET
                issue_id = EXCLUDED.issue_id, value = EXCLUDED.value;"""

        # iterate over all configured fields and determine if issue has field & a value
        for jira_field in connection.connection_config["properties"]["fields"]:
            field_id, _ = connection.get_custom_field(jira_field)

            # skip if field not present
            if field_id["id"] not in issue["fields"]:
                continue
            value = connection.get_field_value(issue["fields"][field_id["id"]])
            if value is None or str(value) == "":
                continue
            issue_properties = {
                "url": issue["url"],
                "connection": connection.connection_config["id"],
                "id": issue["id"],
                "property": jira_field,
                "value": str(value)[0:2047]
            }
            self._upsert(isql, issue_properties)
            events += 1
        self.hub.logger.perf("synchronize_issue_properties done", events, time.monotonic() - start)

    def synchronize_releases(self, do_scope: bool = False):
        '''Synchronize Jira releases'''
        self.hub.logger.info("synchronize_releases started")

        for connection in self.hub.get_connections_by_type("jira"):
            if not self._is_fact_enabled(connection.connection_config, "releases"):
                continue
            projects = connection.get_project_keys()
            for project in projects:
                self.synchronize_release_for_project(connection, project, do_scope)
        self.hub.logger.info("synchronize_releases done")

    def synchronize_release_for_project(self, connection, project: str, do_scope: bool = False):
        self.hub.logger.info(f"synchronize_release_for_project {project} started")
        start = time.monotonic()
        events = 0
        jira_response = connection.call_rest_api(f"project/{project}/versions")
        jira_releases = json.loads(jira_response.text)
        for jira_release in jira_releases:
            jira_release["project"] = project
            # ignore old releases
            if "startDate" in jira_release and datetime.strptime(jira_release["startDate"], "%Y-%m-%d") <= datetime.now() - timedelta(days=365):
                continue
            release = self.parse_release(connection, jira_release)
            self.synchronize_release(release)
            events += 1
            if do_scope:
                # we need to clear the records here as the synchronize_release_scope will recursively call itself until all records are fetched
                self.database.execute(f"""DELETE FROM release_scope WHERE release = '{release["name"]}';""")
                self.database.commit()
                self.synchronize_release_scope(connection, release)
        self.hub.logger.perf(f"synchronize_release_for_project {project} done", events, time.monotonic() - start)

    def parse_release(self, connection, release: dict) -> dict:
        '''Parse Jira release'''
        status = "Open"
        if "startDate" in release and datetime.strptime(release["startDate"], "%Y-%m-%d") <= datetime.now():
            status = "In Progress"
        release.update({
            "url": release["self"],
            "connection": connection.connection_config["id"],
            "name": release["name"][:127],
            "status": "Closed" if release["released"] else status,
            "started_at": None if "startDate" not in release else release["startDate"],
            "ended_at": None if "releaseDate" not in release else release["releaseDate"],
            "is_open": False if status == "Closed" else True
        })
        return release

    def synchronize_release(self, release: dict):
        '''Synchronize Jira release'''
        self.hub.logger.debug(f"""Synchronizing release {release["name"]}""")
        isql = """
            INSERT INTO releases(url, connection,
                name, status,
                team, project,
                started_at, ended_at,
                is_open)
            VALUES ({{ url }}, {{ connection }},
                {{ name }}, {{ status }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes da WHERE da.dimension_type = 'project' AND da.dimension = {{ project }} AND da.attribute_type = 'team'),
                    (SELECT i.team FROM issues i INNER JOIN release_scope ss ON i.issue_id = ss.issue_id WHERE ss.release = {{ name }} GROUP BY i.team ORDER BY COUNT(i.issue_id) DESC LIMIT 1),
                    ''),
                {{ project }},
                {{ started_at }}, {{ ended_at }},
                {{ is_open }})
            ON CONFLICT (url) DO
            UPDATE SET
                name = EXCLUDED.name, status = EXCLUDED.status,
                team = EXCLUDED.team, project = EXCLUDED.project,
                started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at,
                is_open = EXCLUDED.is_open;"""
        self._upsert(isql, release)

    def synchronize_release_scope(self, connection, release: str, max_results: int = None, start_at: int = 0):
        '''
        Synchronize Jira issues related to release

        Args:
            release(dict):                      Release dictionary
            max_results(integer):               Number of issues to query from Jira
            start_at(integer):                  Pagination of Jira results
        '''
        self.hub.logger.debug("synchronize_release_scope started")
        start = time.monotonic()
        events = 0

        isql = """
            INSERT INTO release_scope(url, connection, release, issue_id, scoped_at)
            SELECT {{ url }}, {{ connection }}, {{ name }}, {{ issue }}, {{ scoped_at }}
            WHERE NOT EXISTS (SELECT 1 FROM release_scope WHERE url = {{ url }} AND issue_id = {{ issue }});"""

        # find all issues in this release
        max_results = max_results if max_results is not None else connection.connection_config["properties"]["maxResults"]
        jql = f"""fixVersion = "{release["name"]}" {connection.get_project_jql()} ORDER BY updated ASC"""
        issues = connection.search_api(jql, max_results=max_results, start_at=start_at)
        for issue in issues:
            try:
                events += 1
                release["issue"] = issue["key"]
                release["connection"] = connection.connection_config["id"]
                history_details = self._parse_issue_history(connection, issue, release_name=release["name"])
                release["scoped_at"] = history_details["release_scoped_at"] if history_details["release_scoped_at"] is not None else issue["fields"]["created"][0:19].replace("T", " ")
                release["issue"] = issue["key"]
                self._upsert(isql, release)
            except Exception as e:
                self.hub.logger.warn(f"""Issue not found {issue["key"]}""")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
                self.schedule_task("JiraEtlTask", "bespoke", {"task": "issues", "sync_mode": f"""key={issue["key"]}"""})

        if events == max_results:
            self.synchronize_release_scope(connection, release, max_results, start_at + max_results)
        self.hub.logger.perf("synchronize_release_scope done", events, time.monotonic() - start)

    def synchronize_sprints(self):
        '''Synchronize Jira sprints'''
        self.hub.logger.info("synchronize_sprints started")
        for connection in self.hub.get_connections_by_type("jira"):
            if not self._is_fact_enabled(connection.connection_config, "sprints"):
                continue
            try:
                self.synchronize_boards(connection)
            except Exception as e:
                self.hub.logger.warn(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_sprints done")

    def synchronize_boards(self, connection, start_at: int = 0):
        '''Synchronize Jira sprint boards'''
        self.hub.logger.info(f"""Synchronizing {connection.connection_config["server"]}""")
        # Jira API max results for sprints is 50
        max_results = 50

        # iterate over Jira boards to find sprints
        url = f"""{connection.connection_config["server"]}/rest/agile/1.0/board"""
        params = {
            "maxResults": max_results,
            "startAt": start_at,
            "type": "scrum"
        }
        http_response = connection.call_rest_api("", url=url, params=params)
        boards = json.loads(http_response.text)
        for board in boards["values"]:
            try:
                self.synchronize_board_sprints(connection, board)
            except Exception as e:
                self.hub.logger.warn(f"""Unable to synchronize {board["name"]}""")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())

        if not boards["isLast"]:
            self.synchronize_boards(connection, start_at + max_results)

    def synchronize_board_sprints(self, connection, board, start_at: int = 0):
        # find sprints associated with board
        self.hub.logger.info(f"""synchronize_board_sprints {board["id"]} started""")
        max_results = 50
        start = time.monotonic()
        events = 0
        url = f"""{connection.connection_config["server"]}/rest/agile/1.0/board/{board["id"]}/sprint"""
        sprint_params = {
            "maxResults": max_results,
            "startAt": start_at
        }

        http_response = connection.call_rest_api("", params=sprint_params, url=url)
        sprints = json.loads(http_response.text)
        for sprint in sprints["values"]:
            if "startDate" not in sprint:
                continue
            # if sprint is > 1 year old, skip it
            s = datetime.strptime(sprint["startDate"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
            if (datetime.now() - s).days > 365:
                continue
            # if the sprint is already complete, skip it, if None then this is a new sprint
            is_sprint_active = self._is_sprint_active(sprint)
            if is_sprint_active is not None and not is_sprint_active:
                continue

            sprint = self.parse_sprint(connection, board, sprint)
            events += 1

            # we need to clear the records here as the synchronize_sprint_scope will recursively call itself until all records are fetched
            self.database.execute(f"""DELETE FROM sprint_scope WHERE url = '{sprint["self"]}';""")
            self.database.commit()
            self.synchronize_sprint(sprint)
            self.synchronize_sprint_scope(connection, sprint)
            self.synchronize_sprint(sprint)
            # self._update_sprint_grade(sprint)

        # recursively call until all sprints synchronized
        self.hub.logger.perf(f"""synchronize_board_sprints {board["id"]} done""", events, time.monotonic() - start)
        if not sprints["isLast"]:
            self.synchronize_board_sprints(connection, board, start_at + max_results)

    def parse_sprint(self, connection, board: dict, sprint: dict) -> dict:
        '''Parse Jira sprint'''
        # update sprint properties
        project = "" if "location" not in board else self.hub.helpers["MetaHelper"].map_dimension("project", board["location"]["projectKey"])
        sprint.update({
            "url": sprint["self"],
            "connection": connection.connection_config["id"],
            "project": project,
            "status": self.hub.helpers["MetaHelper"].map_dimension("status", sprint["state"].replace("active", "In Progress")),
            "started_at": sprint["startDate"][0:19].replace("T", " "),
            "ended_at": sprint["endDate"][0:19].replace("T", " ")
        })
        sprint["is_open"] = sprint["status"] != "Closed"
        return sprint

    def synchronize_sprint(self, sprint: dict):
        '''Synchronize Jira sprint'''
        self.hub.logger.info(f"""Synchronizing sprint {sprint["name"]}""")
        isql = """
            INSERT INTO sprints(url, connection,
                name, status,
                team, project,
                started_at, ended_at,
                is_open)
            VALUES({{ url }}, {{ connection }},
                {{ name }}, {{ status }},
                COALESCE((SELECT i.team FROM issues i INNER JOIN sprint_scope ss ON i.issue_id = ss.issue_id WHERE ss.sprint = {{ name }} GROUP BY i.team ORDER BY COUNT(i.issue_id) DESC LIMIT 1), ''),
                COALESCE((SELECT i.project FROM issues i INNER JOIN sprint_scope ss ON i.issue_id = ss.issue_id WHERE ss.sprint = {{ name }} GROUP BY project ORDER BY COUNT(i.issue_id) DESC LIMIT 1), ''),
                {{ started_at }}, {{ ended_at }},
                {{ is_open }})
            ON CONFLICT (url) DO
            UPDATE SET
                name = EXCLUDED.name, status = EXCLUDED.status,
                team = EXCLUDED.team, project = EXCLUDED.project,
                started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at,
                is_open = EXCLUDED.is_open;"""
        self._upsert(isql, sprint)

    def synchronize_sprint_scope(self, connection, sprint: dict, max_results: int = None, start_at: int = 0):
        '''
        Synchronize Jira issues related to sprint

        Args:
            sprint(dict):                       Sprint details
            max_results(integer):               Number of issues to query from Jira
            start_at(integer):                  Pagination of Jira results
        '''
        self.hub.logger.info(f"""synchronize_sprint_scope started {sprint["name"]}""")
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO sprint_scope(url, connection, sprint, issue_id, scoped_at)
            VALUES ({{ url }}, {{ connection }}, {{ name }}, {{ issue_id }}, {{ scoped_at }});"""

        # find issues in sprint
        max_results = max_results if max_results is not None else connection.connection_config["properties"]["maxResults"]
        jql = f"""sprint in ("{sprint["name"]}") {connection.get_project_jql()}"""
        issues = connection.search_api(jql, max_results, start_at)
        for issue in issues:
            events += 1
            sprint["issue_id"] = issue["key"]
            # get issue history to find scoped date...
            history_details = self._parse_issue_history(connection, issue, sprint_name=sprint["name"])
            sprint["scoped_at"] = history_details["sprint_scoped_at"] or sprint["startDate"][0:19].replace("T", " ")

            try:
                self._upsert(isql, sprint)
            except Exception as e:
                # prefer processing as many as possible vs failing on one bad
                self.hub.logger.warn(f"""Unable to add sprint_scope {sprint["issue_id"]}""")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf("synchronize_sprint_scope done", events, time.monotonic() - start)
        if events == max_results:
            self.synchronize_sprint_scope(connection, sprint, max_results, start_at + max_results)

    def synchronize_worklogs(self):
        '''Synchronize Jira worklogs'''
        self.hub.logger.info("synchronize_worklogs started")
        start = time.monotonic()
        events = 0

        for connection in self.hub.get_connections_by_type("jira"):
            if not self._is_fact_enabled(connection.connection_config, "worklogs"):
                continue
            # find last record to synchronize from
            since = self._get_since("SELECT COALESCE(MAX(ended_at), {{ minSyncDate }}) FROM worklogs WHERE url LIKE {{ url }};",
                {"url": connection.connection_config["server"] + "%", "minSyncDate": self.hub.config["properties"]["minSyncDate"]})

            # only fetch worklogs since last successful load
            jql = f"""worklogDate >= {since.strftime("%Y-%m-%d")} {connection.get_project_jql()} ORDER BY updated ASC"""
            issues = connection.search_api(jql)
            for issue in issues:
                for worklog in issue["fields"]["worklog"]["worklogs"]:
                    logged = datetime.strptime(worklog["updated"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
                    if logged.date() < since.date():
                        self.hub.logger.debug(f"""Skipping {issue["key"]}""")
                        continue
                    worklog = self.parse_worklog(connection, issue, worklog)
                    if worklog["username"] == "":
                        continue
                    self.synchronize_worklog(worklog)
                    events += 1
        self.hub.logger.perf("synchronize_worklogs done", events, time.monotonic() - start)

    def parse_worklog(self, connection, issue: dict, worklog: dict) -> dict:
        '''Parse Jira worklog'''
        worklog.update({
            "url": worklog["self"],
            "connection": connection.connection_config["id"],
            "issue": issue["key"],
            "summary": "" if "comment" not in worklog else worklog["comment"][:255],
            "username": connection.get_username_from_field(worklog["author"]),
            "ended_at": datetime.strptime(worklog["started"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S"),
            "timeSpent": worklog["timeSpentSeconds"] / 3600.0
        })
        return worklog

    def synchronize_worklog(self, worklog: dict):
        ''''Synchronize Jira worklog'''
        self.hub.logger.debug(f"""Synchronizing worklog by {worklog["username"]} on {worklog["ended_at"]}""")
        isql = """
            INSERT INTO worklogs(url, connection,
                issue_id, summary, username,
                team,
                ended_at, effort)
            SELECT {{ url }}, {{ connection }},
                {{ issue }}, {{ summary }}, {{ username }},
                COALESCE((SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }}), ''),
                {{ ended_at }}, {{ timeSpent }}
            WHERE NOT EXISTS (SELECT 1 FROM worklogs WHERE url = {{ url }});"""
        self._upsert(isql, worklog)
