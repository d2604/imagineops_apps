import json
from argparse import Namespace
from flask_apispec import doc, marshal_with, use_kwargs
from core.hub import Hub
from ext.shared.JiraConnection import JiraConnection
from api.core_api import BaseResource
from api.core_model import BaseResponseSchema, BaseRequestSchema
from api.connectivity import ConnectionTestRequestSchema


args = Namespace(config_file="config.json")
hub = Hub.get_instance(args, "Flask")


# REST SCHEMA
class JiraConfig(BaseResource):
    @doc(description="Jira Configuration", tags=["connectivity"])
    @marshal_with(BaseResponseSchema)
    def get(self, id):
        return {"data": hub.connections[id].connection_config}

    @doc(description="Jira Configuration", tags=["connectivity"])
    @use_kwargs(BaseRequestSchema, location="json")
    @marshal_with(BaseResponseSchema)
    def put(self, id, **kwargs):
        new_config = {
            "connections": [kwargs["data"]]
        }
        hub.config_service.update(new_config)
        hub.reload()
        return kwargs


class JiraField(BaseResource):
    @doc(description="Jira Field", tags=["connectivity"])
    @marshal_with(BaseResponseSchema)
    def get(self, id):
        http_response = hub.connections[id].call_rest_api("field")
        fields = json.loads(http_response.text)
        items = []
        row_count = 0
        for field in fields:
            row_count += 1
            items.append({
                "name": field["id"],
                "value": field["name"]})
        return {
            "meta": {
                "page": 1,
                "total": row_count,
                "results": row_count
            },
            "dataset": sorted(items, key=lambda d: d["value"])
        }


class JiraProject(BaseResource):
    @doc(description="Jira Field", tags=["connectivity"])
    @marshal_with(BaseResponseSchema)
    def get(self, id):
        http_response = hub.connections[id].call_rest_api("project")
        projects = json.loads(http_response.text)
        items = []
        row_count = 0
        for project in projects:
            row_count += 1
            items.append({
                "name": project["key"],
                "value": project["name"]})
        return {
            "meta": {
                "page": 1,
                "total": row_count,
                "results": row_count
            },
            "dataset": sorted(items, key=lambda d: d["value"])
        }


class JiraTest(BaseResource):
    @doc(description="Jira Connnection Test", tags=["connectivity"])
    @use_kwargs(ConnectionTestRequestSchema, location="query")
    @marshal_with(BaseResponseSchema)
    def put(self, **kwargs):
        http_response_code = 202
        success = True

        try:
            tester = JiraConnection(hub, kwargs)  # noqa: F841
        except Exception as e:  # noqa: F841
            http_response_code = 401
            success = False
        return {"value": success}, http_response_code


# register API
hub.helpers["flask_api"].add_resource(JiraField, "/api/latest/config/connectivity/jira/<id>/field")
hub.helpers["swagger"].register(JiraField)
hub.helpers["flask_api"].add_resource(JiraProject, "/api/latest/config/connectivity/jira/<id>/project")
hub.helpers["swagger"].register(JiraProject)
hub.helpers["flask_api"].add_resource(JiraTest, "/api/latest/config/connectivity/test/jira")
hub.helpers["swagger"].register(JiraTest)
