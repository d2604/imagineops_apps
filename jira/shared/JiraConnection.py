from __future__ import annotations
import jira
import json
import requests
import time


class JiraConnection(jira.JIRA):
    '''
    JiraConnection extends Atlassian Jira library + adds REST support

    **Configuration**

    .. code-block:: json

        {
            "properties": {
                "retryLimit": 3
            },
            "connections": [
                {"id": "jira", "type": "jira", "server": "https://your-jira-server.com", "username": "your-username", "password": "your-password",
                    "properties": {
                        "deploymentType": "Server|Cloud",
                        "maxResults": 500,
                        "projects": ["*"],
                        "teamDiscovery": {
                            "map_by_field": false, "map_fields": [],
                            "map_by_user": true,
                            "map_by_component": true
                        }
                    }
                }
            ]
        }

    **Usage**

    .. code-block:: python

        connection_config = {"id": "jira", "type": "jira", "server": "https://your-jira-server.com", "username": "your-username", "password": "your-password"}
        jira = JiraConnection(hub, connection_config)
        issue = jira.issue("ISS-1")

    **Notes**

    - Depends on: https://atlassian-python-api.readthedocs.io/jira.html
    - Requires Hub have a MetaHelper initialized
    '''
    def __init__(self, hub, connection_config):
        self.hub = hub
        self.rag = "R"
        self.connection_config = connection_config
        self.hub.logger.info(f"""Connecting to Jira {connection_config["server"]}""")
        # check if this is Jira cloud vs hosted
        if connection_config["properties"]["deploymentType"] == "Server":
            headers = jira.JIRA.DEFAULT_OPTIONS["headers"].copy()
            headers["Authorization"] = f"""Bearer {connection_config["password"]}"""
            super(JiraConnection, self).__init__(server=connection_config["server"], options={"headers": headers})
        else:
            super(JiraConnection, self).__init__({"server": connection_config["server"]}, basic_auth=(connection_config["username"], connection_config["password"]))
        self.check_connection(on_init=True)
        self._init_config()
        self._init_cache()

    def _init_cache(self):
        '''Pre-load Jira config details'''
        self.cache = {}
        http_response = self.call_rest_api("field")
        self.cache["custom_fields"] = json.loads(http_response.text)

    def _init_config(self):
        self.max_results = 500 if "maxResults" not in self.connection_config["properties"] else self.connection_config["properties"]["maxResults"]
        self.project_filter = ["*"] if "projects" not in self.connection_config["properties"] else self.connection_config["properties"]["projects"]
        self.retry_interval = 5 if not self.hub.config_service.exists("properties.pollInterval") else self.hub.config["properties"]["pollInterval"]
        self.retry_limit = 3 if not self.hub.config_service.exists("properties.retryLimit") else self.hub.config["properties"]["retryLimit"]

    ###########################################################################
    # Internal API
    ###########################################################################

    def _get_team_from_component(self, issue):
        component = ""
        map_fields = self.connection_config["properties"]["teamDiscovery"]["component_fields"]
        for map_field in map_fields:
            # find jira field definition(s)
            field_definition, issue_field = self.get_custom_field(map_field, issue)
            if issue_field is not None:
                component = self.get_field_value(issue_field)
                sql = """SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND LOWER(dimension) = LOWER({{ component }}) AND attribute_type = 'team';"""
                sql, bind_parameters = self.hub.connections["database"].sqlize(sql, {"component": component})
                result = self.hub.connections["database"].connection.execute(sql, *bind_parameters)
                row = result.fetchone()
                if row is not None:
                    return row[0]
        return ""

    def _get_team_from_field(self, issue):
        if self.connection_config["properties"]["teamDiscovery"]["map_by_field"]:
            map_fields = self.connection_config["properties"]["teamDiscovery"]["map_fields"]
            for map_field in map_fields:
                # find jira field definition(s)
                field_definition, issue_field = self.get_custom_field(map_field, issue)
                if issue_field is not None:
                    return self.get_field_value(issue_field)
        return ""

    def _get_team_from_project(self, issue):
        project = issue["fields"]["project"]["name"]
        sql = """SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND LOWER(dimension) = LOWER({{ project }}) AND attribute_type = 'team';"""
        sql, bind_parameters = self.hub.connections["database"].sqlize(sql, {"project": project})
        result = self.hub.connections["database"].connection.execute(sql, *bind_parameters)
        row = result.fetchone()
        if row is not None:
            return row[0]
        return ""

    def _get_team_from_user(self, issue):
        map_fields = self.connection_config["properties"]["teamDiscovery"]["user_fields"]
        username = ""
        for map_field in map_fields:
            username = self.get_username_from_field(issue["fields"][map_field])
            if username != "":
                sql = """SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND LOWER(dimension) = LOWER({{ username }}) AND attribute_type = 'team';"""
                sql, bind_parameters = self.hub.connections["database"].sqlize(sql, {"username": username})
                result = self.hub.connections["database"].connection.execute(sql, *bind_parameters)
                row = result.fetchone()
                if row is not None:
                    return row[0]
        return ""

    ###########################################################################
    # Public API
    ###########################################################################

    def call_rest_api(self, rest_url, params={}, method="GET", url=None, enable_stream=False, attempts=0):
        '''
        Call REST API

        Args:
            rest_url(string):       REST URL to invoke (does not include server)
            params(list):           List of name:value properties to send in HTTP request header
            method(string):         HTTP method: GET, PUT, POST, DELETE
            url(string):            Over-ride of full URL
            enable_stream(boolean): allows results to be streamed vs block wait
            attempts(int):          number of retries already processed

        Returns:
            HTTP response
        '''
        headers = {"content-type": "application/json", "cache-control": "no-cache"}
        auth = requests.auth.HTTPBasicAuth(self.connection_config["username"], self.connection_config["password"])
        if self.connection_config["properties"]["deploymentType"] == "Server":
            # use Personal Access Token for Server API
            headers["Authorization"] = f"""Bearer {self.connection_config["password"]}"""
            auth = None
        if url is None:
            url = f"""{self.connection_config["server"]}/rest/api/latest/{rest_url}"""
        if "maxResults" not in params:
            params["maxResults"] = self.max_results

        if method == "GET":
            response = requests.get(url, params=params, auth=auth, headers=headers, stream=enable_stream, timeout=180)
        elif method == "PUT":
            response = requests.put(url, data=params, auth=auth, headers=headers, timeout=60)
        elif method == "POST":
            response = requests.post(url, data=params, auth=auth, headers=headers, timeout=60)
        elif method == "DELETE":
            response = requests.delete(url, params=params, auth=auth, headers=headers, timeout=60)
        else:
            response = requests.get(url, params=params, auth=auth, headers=headers, timeout=180)

        if response.status_code in [429, 502]:
            # handle HTTP 429: Too Many Requests
            retry_interval = self.retry_interval
            if response.status_code == 429:
                if "Retry-After" in response.headers:
                    retry_interval = response.headers["Retry-After"]
            if attempts < self.retry_limit:
                time.sleep(retry_interval)
                self.hub.logger.warn("Server overloaded, trying again...")
                return self.call_rest_api(rest_url, params=params, method=method, url=url, enable_stream=enable_stream, attempts=attempts + 1)
        return response

    def check_connection(self, on_init=False):
        '''
        Determines connection status
            R = unable to connect (bad username/password, etc)
            A = connection isnt reliable (rate limiting, etc)
            G = good
        '''
        success = True
        self.rag = "G"

        if not on_init:
            self.__init__(self.hub, self.connection_config)
        return success

    def get_custom_field(self, field_name, issue=None):
        '''Lookup Jira internal field id by name

        Args:
            field_name(string):             the Jira diplay name for the field
        Returns:
            Jira field object
        '''
        is_found = False
        issue_field = None
        for field in self.cache["custom_fields"]:
            if field["id"].lower() == field_name.lower() or field["name"].lower() == field_name.lower():
                is_found = True
                break
        if issue is not None and is_found:
            if field["id"] in issue["fields"]:
                issue_field = issue["fields"][field["id"]]
        return field, issue_field

    def get_field_id(self, default_id):
        '''
        Atlassian is inconsistent with how fields are named through their REST API and their JSON export format

        Args:
            default_id(string):     original field ID

        Returns:
            string
        '''
        return default_id.replace("issuetype", "issueType").replace("fixVersions", "fixedVersions")

    def get_field_value(self, value):
        '''
        Returns actual value from issue

        Args:
            value(field):                   Jira issue field
        Returns:
            string
        '''
        valid_keys = ["value", "name", "emailAddress", "displayName", "key", "id", "percent", "total"]
        # determine if value is a dict or list, and break apart
        if isinstance(value, list):
            if len(value) == 0:
                value = None
            elif isinstance(value[0], list):
                # WARN: somehow we have lists embedded in lists, break out of it!
                value = value[0]
            elif isinstance(value[0], dict):
                # list of dictionaries
                new_values = []
                for item in value:
                    for valid_key in valid_keys:
                        if valid_key in item:
                            new_values.append(item[valid_key])
                            break
                value = new_values
        if isinstance(value, dict):
            for valid_key in valid_keys:
                if valid_key in value:
                    value = value[valid_key]
                    break
        return value

    def get_issue_url(self, issue_key):
        '''
        Return URL to Jira Issue

        Args:
            issue_key(string):          Unique Jira ID
        Returns:
            string
        '''
        return f"""{self.connection_config["server"]}/browse/{issue_key}"""

    def get_project_jql(self, include_and=True):
        '''
        Returns project filtering JQL prefixed with "AND" as needed

        Returns:
            string
        '''
        if "*" in self.project_filter:
            return ""
        jql = "" if not include_and else " AND"
        jql += f""" project IN ({",".join(self.project_filter)})"""
        return jql

    def get_project_keys(self, project_category="*"):
        '''
        Returns a list of Jira project keys associated with the specified category

        Args:
            project_category(string):       filter by project category (or include all if '*')

        Returns:
            list
        '''
        project_keys = []
        jira_response = self.call_rest_api("project")

        if jira_response.status_code in [200, 204]:
            search_results = json.loads(jira_response.text)
            for project in search_results:
                if "*" not in self.project_filter and project["key"] not in self.project_filter:
                    continue
                elif "projectCategory" in project and project_category != "*":
                    if project["projectCategory"]["name"] == project_category:
                        project_keys.append(project["key"])
                else:
                    project_keys.append(project["key"])
        else:
            self.hub.logger.error(f"Unexpected response {jira_response.status_code}: {jira_response.text}")
        return project_keys

    def get_team_from_issue(self, issue):
        '''
        Determine team for issue using following approach:

        1. map by Jira field
        2. map by project
        3. map by Component
        4. map by user

        Args:
            issue(dict):            Jira issue
        Returns:
            string
        '''
        team = self._get_team_from_field(issue)
        if team == "" and self.connection_config["properties"]["teamDiscovery"]["map_by_project"]:
            team = self._get_team_from_project(issue)
        if team == "" and self.connection_config["properties"]["teamDiscovery"]["map_by_component"]:
            team = self._get_team_from_component(issue)
        if team == "" and self.connection_config["properties"]["teamDiscovery"]["map_by_user"]:
            team = self._get_team_from_user(issue)

        if isinstance(team, list):
            team = team[0]
        return team

    def get_username_from_field(self, user_field):
        if user_field is None:
            return None
        valid_fields = ["emailAddress", "username", "displayName"]
        for valid_field in valid_fields:
            if valid_field in user_field:
                return self.hub.helpers["MetaHelper"].map_dimension("user", user_field[valid_field]).lower()
        return None

    def search_api(self, jql, max_results=500, start_at=0, fields=["*all"]):
        '''
        Search for Jira issues using REST API
        It is the responsibility of the caller to search multiple times if the query result exceeds max_results

        Args:
            jql(string):            Jira query
            max_results(int):       Number of results to fetch on each request
            start_at(int):          Starting position to fetch results
            fields(list):           Optional argument to identify which fields to return from Jira

        Returns:
            list of Jira issues

        '''
        self.hub.logger.debug(jql)
        issues = []
        jira_response = self.call_rest_api("search", params={"jql": jql, "maxResults": max_results, "startAt": start_at, "fields": ["*all"]})

        if jira_response.status_code in [200, 204]:
            search_results = json.loads(jira_response.text)
            issues = search_results["issues"]
        else:
            self.hub.logger.error(f"Unexpected response {jira_response.status_code}: {jira_response.text}")
        return issues

    def update_api(self, issue_id, fields):
        '''
        Update Jira issue using REST API

        Args:
            issue_id(string):       Jira key
            fields(dictionary):     Fields to update

        Returns:
            boolean of success
        '''
        success = True
        jira_response = self.call_rest_api("issue/" + issue_id, method="PUT", params=fields)

        if jira_response.status_code not in [200, 204]:
            success = False
            self.hub.logger.error(f"Unexpected response {jira_response.status_code}: {jira_response.text}")
        return success
