/* global $, BaseConnection, dashboardManager */
class JiraConnection extends BaseConnection
{
    edit()
    {
        this.setDefaults();
        let html = this.getDefaultEditHtml();
        html += `
        <h5>Advanced</h5>
        <div id="connectivityAccordion" class="accordion accordion-flush">
        <div class="accordion-item">
            <h2 id="connectivityAdvanced" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityAdvancedConfig" aria-expanded="false" aria-controls="connectivityAdvancedConfig">
            Projects
            </button>
            </h2>
            <div id="connectivityAdvancedConfig" class="accordion-collapse collapse" aria-labelledby="connectivityAdvanced" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-floating mb-3">
                <select id="jiraDeploymentType" class="form-select" aria-label="Deployment type">
                    <option value="Cloud" ${(this.connection.properties.deploymentType === "Cloud" ? "selected" : "")}>Cloud</option>
                    <option value="Server" ${(this.connection.properties.deploymentType === "Server" ? "selected" : "")}>Server</option>
                </select>
                <label for="jiraDeploymentType">Deployment Type</label>
                </div>
                <br>
                <div class="form-floating mb-3">
                <select id="jiraProjects" class="form-select form-select-lg" multiple aria-label="Jira projects"></select>
                <label for="jiraProjects">Projects</label>
                </div>
            </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 id="teamDiscoveryHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#teamDiscoveryContent" aria-expanded="false" aria-controls="teamDiscoveryContent">
            Team Discovery
            </button>
            </h2>
            <div id="teamDiscoveryContent" class="accordion-collapse collapse" aria-labelledby="teamDiscoveryHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="input-group mb-3"><div class="input-group-text">
                    <input id="map_by_field" class="form-check-input mt-0" type="checkbox" value="${this.connection.properties.teamDiscovery.map_by_field}" ${(this.connection.properties.teamDiscovery.map_by_field ? "checked" : "")} aria-label="Map by field">
                    <label for="map_by_field">Map by Field</label>
                </div>
                <input id="map_fields" type="text" class="form-control" aria-label="Map fields" value="${this.connection.properties.teamDiscovery.map_fields}">
                </div>

                <div class="input-group mb-3"><div class="input-group-text">
                    <input id="map_by_user" class="form-check-input mt-0" type="checkbox" value="${this.connection.properties.teamDiscovery.map_by_user}" ${(this.connection.properties.teamDiscovery.map_by_user ? "checked" : "")} aria-label="Map by user">
                    <label for="map_by_user">Map by User</label>
                </div>
                <input id="jiraUserFields" type="text" class="form-control" aria-label="User fields" value="${this.connection.properties.teamDiscovery.user_fields}">
                </div>

                <div class="input-group mb-3"><div class="input-group-text">
                    <input id="map_by_component" class="form-check-input mt-0" type="checkbox" value="${this.connection.properties.teamDiscovery.map_by_component}" ${(this.connection.properties.teamDiscovery.map_by_component ? "checked" : "")} aria-label="Map by component">
                    <label for="map_by_component">Map by Component</label>
                </div>
                <input id="jiraComponentFields" type="text" class="form-control" aria-label="Component fields" value="${this.connection.properties.teamDiscovery.component_fields}">
                </div>
            </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 id="connectivityFactsHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityFactsConfig" aria-expanded="false" aria-controls="connectivityFactsConfig">
            Facts
            </button>
            </h2>
            <div id="connectivityFactsConfig" class="accordion-collapse collapse" aria-labelledby="connectivityFactsHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-check"><input id="do_issues" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_issues ? "checked" : "")}>
                <label class="form-check-label" for="do_issues">Issues</label>
                </div>
                <div class="form-check"><input id="do_releases" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_releases ? "checked" : "")}>
                <label class="form-check-label" for="do_releases">Releases</label>
                </div>
                <div class="form-check"><input id="do_sprints" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_sprints ? "checked" : "")}>
                <label class="form-check-label" for="do_sprints">Sprints</label>
                </div>
                <div class="form-check"><input id="do_worklogs" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_worklogs ? "checked" : "")}>
                <label class="form-check-label" for="do_worklogs">Worklogs</label>
                </div>
                <div class="form-check"><input id="do_users" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_users ? "checked" : "")}>
                <label class="form-check-label" for="do_users">Users</label>
                </div>
            </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 id="jiraFieldsHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#jiraFieldsContent" aria-expanded="false" aria-controls="jiraFieldsContent">
            Fields
            </button>
            </h2>
            <div id="jiraFieldsContent" class="accordion-collapse collapse" aria-labelledby="jiraFieldsHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-floating mb-3">
                <select id="jiraFields" class="form-select form-select-lg" multiple aria-label="Jira fields">
                </select>
                <label for="jiraFields">Jira Custom Fields</label>
                </div>
            </div>
            </div>
        </div>
        </div>`;
        return html;
    }

    populateOptions()
    {
        this.populateProjects();
        this.populateFields();
    }

    populateFields()
    {
        const url = `/api/latest/config/connectivity/jira/${this.connection.id}/field`;
        $.ajax({
            url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            error: function() {},
            success: function(data)
            {
                const d = document.getElementById("jiraFields");
                let html = "";

                if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "fields"))
                {
                    this.connection.properties.fields = [];
                }

                for (const jiraField of data.dataset)
                {
                    const selected = (this.connection.properties.fields.includes(jiraField.value) ? "selected" : "");
                    html += `<option value="${jiraField.value}" ${selected}>${jiraField.value}</option>`;
                }
                d.innerHTML = html;
            }.bind(this)
        });
    }

    populateProjects()
    {
        const d = document.getElementById("jiraProjects");
        let html = "";
        for (const project of this.connection.properties.projects)
        {
            html += `<option value="${project}" selected>${project}</option>`;
        }
        d.innerHTML = html;

        const url = "/api/latest/config/dimension?type=project&has_attribute=is_jira";
        $.ajax({
            url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            error: function() {},
            success: function(data)
            {
                const d = document.getElementById("jiraProjects");
                let html = "";
                for (const dimension of data.dataset)
                {
                    if (!this.connection.properties.projects.includes(dimension.dimension))
                    {
                        html += `<option value="${dimension.dimension}">${dimension.dimension}</option>`;
                    }
                }
                d.innerHTML += html;
            }.bind(this)
        });
    }

    setDefaults()
    {
        if (!Object.prototype.hasOwnProperty.call(this.connection, "properties")) this.connection.properties = {};
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "deploymentType")) this.connection.properties.deploymentType = "Cloud";
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "projects")) this.connection.properties.projects = [];
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_issues")) this.connection.properties.do_issues = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_releases")) this.connection.properties.do_releases = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_sprints")) this.connection.properties.do_sprints = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_users")) this.connection.properties.do_users = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_worklogs")) this.connection.properties.do_worklogs = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "teamDiscovery")) this.connection.properties.teamDiscovery = {};
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties.teamDiscovery, "map_by_field")) this.connection.properties.teamDiscovery.map_by_field = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties.teamDiscovery, "map_fields")) this.connection.properties.teamDiscovery.map_fields = "";
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties.teamDiscovery, "map_by_user")) this.connection.properties.teamDiscovery.map_by_user = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties.teamDiscovery, "user_fields")) this.connection.properties.teamDiscovery.user_fields = "";
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties.teamDiscovery, "map_by_component")) this.connection.properties.teamDiscovery.map_by_component = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties.teamDiscovery, "component_fields")) this.connection.properties.teamDiscovery.component_fields = "";
        return this.connection;
    }

    update()
    {
        this.connection.properties.deploymentType = document.getElementById("jiraDeploymentType").value;
        this.connection.properties.do_issues = document.getElementById("do_issues").checked;
        this.connection.properties.do_releases = document.getElementById("do_releases").checked;
        this.connection.properties.do_sprints = document.getElementById("do_sprints").checked;
        this.connection.properties.do_worklogs = document.getElementById("do_worklogs").checked;
        this.connection.properties.teamDiscovery.map_by_field = document.getElementById("map_by_field").checked;
        this.connection.properties.teamDiscovery.map_fields = document.getElementById("map_fields").value.split(",");
        this.connection.properties.teamDiscovery.map_by_user = document.getElementById("map_by_user").checked;
        this.connection.properties.teamDiscovery.user_fields = document.getElementById("jiraUserFields").value.split(",");
        this.connection.properties.teamDiscovery.map_by_component = document.getElementById("map_by_component").checked;
        this.connection.properties.teamDiscovery.component_fields = document.getElementById("jiraComponentFields").value.split(",");

        const projects = [];
        for (const project of document.getElementById("jiraProjects").getElementsByTagName("option"))
        {
            if (project.selected)
            {
                projects.push(project.value);
            }
        }
        this.connection.properties.projects = projects;

        const fields = [];
        for (const field of document.getElementById("jiraFields").getElementsByTagName("option"))
        {
            if (field.selected)
            {
                fields.push(field.value);
            }
        }
        this.connection.properties.fields = fields;
        return this.connection;
    }
}
dashboardManager.connections.jira = { class: JiraConnection };
