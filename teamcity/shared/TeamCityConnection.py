from __future__ import annotations
from dohq_teamcity import TeamCity


class TeamCityConnection(TeamCity):
    '''
    TeamCity Connection
    '''
    def __init__(self, hub, connection_config):
        self.hub = hub
        self.rag = "R"
        self.connection_config = connection_config
        self.hub.logger.info(f"""Connecting to TeamCity {connection_config["server"]}""")
        super(TeamCityConnection, self).__init__(connection_config["server"], auth=(connection_config["username"], connection_config["password"]))
        self.check_connection(on_init=True)

    ###########################################################################
    # Public API
    ###########################################################################

    def check_connection(self, on_init=False):
        '''
        Determines connection status
            R = unable to connect (bad username/password, etc)
            A = connection isnt reliable (rate limiting, etc)
            G = good
        '''
        success = True
        self.rag = "G"

        if not success and not on_init:
            self.__init__(self.hub, self.connection_config)
        return success
