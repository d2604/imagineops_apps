/* global $, BaseConnection, dashboardManager */
class TeamCityConnection extends BaseConnection
{
    edit()
    {
        this.setDefaults();
        let html = this.getDefaultEditHtml();
        html += `
        <h5>Advanced</h5>
        <div id="connectivityAccordion" class="accordion accordion-flush">
    
        <div class="accordion-item">
            <h2 id="connectivityAdvanced" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityAdvancedConfig" aria-expanded="false" aria-controls="connectivityAdvancedConfig">
            Projects
            </button>
            </h2>
            <div id="connectivityAdvancedConfig" class="accordion-collapse collapse" aria-labelledby="connectivityAdvanced" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <select id="teamCityProjects" class="form-select" multiple aria-label="TeamCity Projects">
                </select>
            </div>
            </div>
        </div>
    
        <div class="accordion-item">
            <h2 id="connectivityFactsHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityFactsConfig" aria-expanded="false" aria-controls="connectivityFactsConfig">
            Facts
            </button>
            </h2>
            <div id="connectivityFactsConfig" class="accordion-collapse collapse" aria-labelledby="connectivityFactsHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-check"><input id="do_builds" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_builds ? "checked" : "")}>
                <label class="form-check-label" for="do_builds">Builds/Workflows</label>
                </div>
                <div class="form-check"><input id="do_users" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_users ? "checked" : "")}>
                <label class="form-check-label" for="do_users">Users</label>
                </div>
            </div>
            </div>
        </div>
        </div>`;
        return html;
    }

    populateOptions()
    {
        const d = document.getElementById("teamCityProjects");
        let html = "";
        for (const project of this.connection.properties.projects)
        {
            html += `<option value="${project}" selected>${project}</option>`;
        }
        d.innerHTML = html;

        const url = "/api/latest/config/dimension?type=project&has_attribute=is_teamcity";
        $.ajax({
            url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            error: function() {},
            success: function(data)
            {
                const d = document.getElementById("teamCityProjects");
                let html = "";
                for (const dimension of data.dataset)
                {
                    if (!this.connection.properties.projects.includes(dimension.dimension))
                    {
                        html += `<option value="${dimension.dimension}">${dimension.dimension}</option>`;
                    }
                }
                d.innerHTML += html;
            }.bind(this)
        });
    }

    setDefaults()
    {
        if (!Object.prototype.hasOwnProperty.call(this.connection, "properties")) this.connection.properties = {};
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "projects")) this.connection.properties.projects = [];
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_builds")) this.connection.properties.do_builds = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_users")) this.connection.properties.do_users = false;
        return this.connection;
    }

    update()
    {
        // update the in-memory version of the connection object so that it can be persisted via REST
        this.connection.properties.do_builds = document.getElementById("do_builds").checked;
        this.connection.properties.do_users = document.getElementById("do_users").checked;

        const projects = [];
        for (const project of document.getElementById("teamCityProjects").getElementsByTagName("option"))
        {
            if (project.selected)
            {
                projects.push(project.value);
            }
        }
        this.connection.properties.projects = projects;
        return this.connection;
    }
}
dashboardManager.connections.teamcity = { class: TeamCityConnection };
