import re
import time
import traceback
from datetime import datetime
from ImagineOpsBaseTask import ImagineOpsBaseTask


class TeamCityEtlTask(ImagineOpsBaseTask):
    '''TeamCityEtlTask synchronizes build data from TeamCity'''
    def __init__(self, hub):
        super(TeamCityEtlTask, self).__init__(hub)
        self.app_name = "imagineops_teamcity"
        self.database = self.hub.connections["database"].new_connection()
        self.cache["build_types"] = {}
        self.status_map = {
            "SUCCESS": "Closed",
            "FAILURE": "Failed",
            "UNKNOWN": "Building"
        }

    ###########################################################################
    # task_runner.BaseTask API
    ###########################################################################

    def do_discovery(self):
        self.discover_projects()
        self.discover_build_stages()

    def do_hourly(self):
        self.synchronize_builds()

    def do_daily(self):
        self.do_discovery()

    def do_monday(self):
        self.synchronize_builds(duration=30)

    def do_test(self):
        self.do_discovery()
        self.do_hourly()

    def do_bespoke(self):
        if "task" not in self.args:
            return
        if self.args["task"] == "builds":
            self.synchronize_builds()

    ###########################################################################
    # Tasks
    ###########################################################################

    def discover_build_stages(self):
        '''Discover TeamCity build configuration templates as build stages'''
        self.hub.logger.info("discover_build_stages started")
        start = time.monotonic()
        events = 0
        cached_stages = []
        for connection in self.hub.get_connections_by_type("teamcity"):
            build_types = connection.build_types.get_build_types()
            for build_type in build_types:
                self.cache["build_types"][build_type.id] = build_type.name
                if build_type.name in cached_stages:
                    continue
                cached_stages.append(build_type.name)
                self.hub.logger.debug(f"Discovering stage {build_type.name}")
                events += 1
                self.synchronize_dimension("status", build_type.name)
                self.synchronize_dimension_attribute("status", build_type.name, "is_teamcity", "true")

            projects = connection.projects.get_projects()
            for project in projects:
                templates = project.get_templates_in_project()
                for template in templates:
                    if template.name in cached_stages:
                        continue
                    cached_stages.append(template.name)
                    self.hub.logger.debug(f"Discovering stage {template.name}")
                    events += 1
                    self.synchronize_dimension("status", template.name)
                    self.synchronize_dimension_attribute("status", template.name, "is_teamcity", "true")
        self.hub.logger.perf("discover_build_stages done", events, time.monotonic() - start)

    def discover_projects(self):
        '''Discover TeamCity projects'''
        self.hub.logger.info("discover_projects started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("teamcity"):
            projects = connection.projects.get_projects()
            for project in projects:
                if project.id in ["_Root"] or project.archived:
                    continue
                self.hub.logger.debug(f"Discovering project: {project.id}")
                events += 1
                # TeamCity doesnt handle slashes well and replaces them with underscores
                # here, we auto create an alias to get better synergy with Git*
                # TeamCity also has child projects to store different job types that we want to ignore
                project_alias = project.id[:255].replace("_", "/")
                self.synchronize_dimension("project", project.id[:255], project_alias)
                self.synchronize_dimension("project", project_alias[:255])
                self.synchronize_dimension_attribute("project", project_alias[:255], "is_teamcity", "true")
        self.hub.logger.perf("discover_projects done", events, time.monotonic() - start)

    def synchronize_builds(self, duration=2):
        '''Synchronize TeamCity builds'''
        self.hub.logger.info("synchronize_builds started")
        start = time.monotonic()
        for connection in self.hub.get_connections_by_type("teamcity"):
            for project in connection.connection_config["properties"]["projects"]:
                try:
                    if time.monotonic() - start > 54000:
                        continue
                    project = re.sub("\\\\", "_", project).replace("/", "_")
                    self.synchronize_builds_for_project(connection, project, duration)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {project}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_builds done")

    def synchronize_builds_for_project(self, connection, project: str, duration=2) -> int:
        '''
        Synchronize TeamCity builds for selected project
        TeamCity has implemented the concept of a 'pipeline' as multiple build instances
        that share a common Build.number

        Args:
            connection:        Connection to TeamCity
            project(str):               TeamCity project name
        '''
        start = time.monotonic()
        events = 0
        project_alias = self.hub.helpers["MetaHelper"].map_dimension("project", project)
        self.hub.logger.info(f"synchronize_builds_for_project {project_alias} started")
        project = connection.projects.get(project)

        # find the latest record to synchronize from
        since_sql = "SELECT COALESCE(MAX(ended_at) - INTERVAL '1 DAY', {{ minSyncDate }}) FROM builds WHERE repository = {{ repository }};"
        since = self._get_since(since_sql.replace("2 DAYS", f"{duration} DAYS"),
            {"repository": project_alias, "minSyncDate": self.hub.config["properties"]["minSyncDate"]})

        for build_type in project.build_types.build_type:
            teamcity_builds = connection.build_api.get_all_builds(count=5000, build_type=build_type.id, locator=f"""defaultFilter:false,affectedProject:{project.id},finishDate:{since.strftime("%Y%m%dT%H%M%S+0000")}""")
            for teamcity_build in teamcity_builds:
                teamcity_build = connection.build_api.get(teamcity_build.id)
                build, build_scope = self.parse_build(connection, project, teamcity_build)
                self.synchronize_build(build)
                self.synchronize_build_scope(connection, build["url"], build["started_at"], build_scope)
                events += 1
        self.hub.logger.perf(f"synchronize_builds_for_project {project_alias} done", events, time.monotonic() - start)

    def parse_build(self, connection, project, teamcity_build):
        '''Synchronize TeamCity builds'''
        project_alias = self.hub.helpers["MetaHelper"].map_dimension("project", project.id)
        stages_pass = stages_fail = stages_skipped = 0
        tests_pass = tests_fail = tests_skipped = 0
        build_hours = test_hours = deploy_hours = 0
        build_slack = test_slack = deploy_slack = 0
        test_stage_started_at = test_stage_ended_at = None
        deploy_stage_started_at = deploy_stage_ended_at = None
        built_at = tested_at = deployed_at = None
        build_scope = []
        created_at = datetime.strptime(teamcity_build.queued_date[:14], "%Y%m%dT%H%M%S")
        started_at = datetime.strptime(teamcity_build.start_date[:14], "%Y%m%dT%H%M%S")
        ended_at = datetime.strptime(teamcity_build.finish_date[:14], "%Y%m%dT%H%M%S")
        build_stage_started_at = started_at
        build_stage_ended_at = ended_at

        # find user that triggered the build
        username = teamcity_build.user
        if teamcity_build.user is None:
            teamcity_changes = connection.changes.get_changes(count=10, build=teamcity_build.id)
            if teamcity_changes.change is not None:
                for teamcity_change in teamcity_changes.change:
                    username = teamcity_change.username
                    break

        # determine test results
        if hasattr(teamcity_build, "test_occurrences") and teamcity_build.test_occurrences is not None:
            tests_pass += teamcity_build.test_occurrences.passed or 0
            tests_fail += teamcity_build.test_occurrences.failed or 0

        # build scope (commits & issues)
        teamcity_changes = teamcity_build.changes.change
        if teamcity_changes is not None:
            for teamcity_change in teamcity_changes:
                build_scope.append({"connection": connection.connection_config["id"], "commit_id": teamcity_change.version, "issue_id": ""})

        teamcity_issues = teamcity_build.get_build_related_issues()
        if teamcity_issues is not None and teamcity_issues.issue_usage is not None:
            for teamcity_issue in teamcity_issues.issue_usage:
                issue_id = "" if not hasattr(teamcity_issue, "issue") else teamcity_issue.issue.id
                build_scope.append({"connection": connection.connection_config["id"], "commit_id": "", "issue_id": issue_id})

        # find related build stages to determine time in build stage
        # teamcity_stages = connection.build_api.get_all_builds(count=100, locator=f"defaultFilter:false,number:{teamcity_build.number}")
        teamcity_stages = [] if not hasattr(teamcity_build.snapshot_dependencies, "build") else teamcity_build.snapshot_dependencies.build
        for dependency_stage in teamcity_stages:
            # teamcity lazy loads everything
            teamcity_stage = connection.build_api.get(dependency_stage.id)

            # stage success
            stages_pass += 1 if teamcity_stage.status in ["SUCCESS"] else 0
            stages_fail += 1 if teamcity_stage.status not in ["SUCCESS"] else 0

            # job hasnt started|finished yet
            if None in [teamcity_stage.queued_date, teamcity_stage.start_date, teamcity_stage.finish_date]:
                continue
            elif teamcity_stage.start_date < teamcity_build.start_date:
                continue

            job_stage = "Building"
            if teamcity_stage.build_type_id in self.cache["build_types"]:
                job_stage = self.cache["build_types"][teamcity_stage.build_type_id]
            else:
                teamcity_stage_build_type = connection.build_types.get(teamcity_stage.build_type_id)
                job_stage = teamcity_stage_build_type.name
                self.cache["build_types"][teamcity_stage_build_type.id] = teamcity_stage_build_type.name
            job_stage = self.hub.helpers["MetaHelper"].map_dimension("status", job_stage)
            job_stage = "Building" if job_stage not in ["Building", "Testing", "Deploying"] else job_stage

            stage_created_at = datetime.strptime(teamcity_stage.queued_date[:14], "%Y%m%dT%H%M%S")
            stage_started_at = datetime.strptime(teamcity_stage.start_date[:14], "%Y%m%dT%H%M%S")
            stage_ended_at = datetime.strptime(teamcity_stage.finish_date[:14], "%Y%m%dT%H%M%S")

            # process job stages
            if job_stage in ["Building"]:
                built_at = max(stage_ended_at, built_at or stage_ended_at)
                build_hours += (stage_ended_at - stage_started_at).total_seconds()
                build_slack += (stage_started_at - stage_created_at).total_seconds()
                build_stage_started_at = min(stage_started_at, build_stage_started_at or stage_started_at)
                build_stage_ended_at = max(stage_ended_at, build_stage_ended_at or stage_ended_at)
            elif job_stage in ["Testing"]:
                tested_at = max(stage_ended_at, tested_at or stage_ended_at)
                test_hours += (stage_ended_at - stage_started_at).total_seconds()
                test_slack += (stage_started_at - stage_created_at).total_seconds()
                test_stage_started_at = min(stage_started_at, test_stage_started_at or stage_started_at)
                test_stage_ended_at = max(stage_ended_at, test_stage_ended_at or stage_ended_at)
            elif job_stage in ["Deploying"]:
                deployed_at = max(stage_ended_at, deployed_at or stage_ended_at)
                deploy_hours += (stage_ended_at - stage_started_at).total_seconds()
                deploy_slack += (stage_started_at - stage_created_at).total_seconds()
                deploy_stage_started_at = min(stage_started_at, deploy_stage_started_at or stage_started_at)
                deploy_stage_ended_at = max(stage_ended_at, deploy_stage_ended_at or stage_ended_at)

        build = {
            "url": teamcity_build.web_url,
            "connection": connection.connection_config["id"],
            "status": "Failed" if teamcity_build.get_aggregated_build_status() not in self.status_map else self.status_map[teamcity_build.get_aggregated_build_status()],
            "branch": teamcity_build.branch_name,
            "repository": project_alias,
            "build_type": teamcity_build.build_type.name,
            "component": "",
            "version": self.hub.helpers["SourceCodeHelper"].parse_version(teamcity_build.branch_name),
            "username": self.hub.helpers["MetaHelper"].map_dimension("user", username),
            "created_at": created_at,
            "started_at": started_at,
            "ended_at": ended_at,
            "built_at": built_at,
            "tested_at": tested_at,
            "deployed_at": deployed_at,
            "updated_at": datetime.strptime(teamcity_build.finish_date[:14], "%Y%m%dT%H%M%S"),
            "stages": len(teamcity_stages),
            "stages_pass": stages_pass,
            "stages_fail": stages_fail,
            "stages_skipped": stages_skipped,
            "tests": tests_pass + tests_fail,
            "tests_pass": tests_pass,
            "tests_fail": tests_fail,
            "tests_skipped": tests_skipped,
            "build_hours": build_hours / 3600.0,
            "build_slack": build_slack / 3600.0,
            "build_duration": 0 if None in [started_at, ended_at] else (ended_at - started_at).total_seconds() / 3600.0,
            "test_hours": test_hours / 3600.0,
            "test_slack": test_slack / 3600.0,
            "test_duration": 0 if None in [test_stage_started_at, test_stage_ended_at] else (test_stage_ended_at - test_stage_started_at).total_seconds() / 3600.0,
            "deploy_hours": deploy_hours / 3600.0,
            "deploy_slack": deploy_slack / 3600.0,
            "deploy_duration": 0 if None in [deploy_stage_started_at, deploy_stage_ended_at] else (deploy_stage_ended_at - deploy_stage_started_at).total_seconds() / 3600.0,
            "open_hours": (ended_at - created_at).total_seconds() / 3600.0,
            "artifacts": 0 if not hasattr(teamcity_build, "artifacts") else (teamcity_build.artifacts.count or 0),
            "slack": build_slack + test_slack + deploy_slack,
            "is_deployed": deployed_at is not None,
            "is_pass": teamcity_build.get_aggregated_build_status() == "SUCCESS",
            "is_tested": tests_pass + tests_fail > 0,
            "is_trunk": teamcity_build.default_branch
        }
        build["grade"] = self.hub.helpers["GradingHelper"].get_build_grade(build)
        build["rag"] = self.grade_to_rag_map[build["grade"]]
        return build, build_scope

    def synchronize_build(self, build: dict):
        '''Synchronzie TeamCity build'''
        self.hub.logger.debug(f"""Synchronizing build {build["url"]}""")
        isql = """
            INSERT INTO builds(url, connection,
                repository, branch, build_type,
                component, version,
                status, rag, grade,
                team, username,
                created_at, started_at, ended_at, updated_at,
                built_at, tested_at, deployed_at,
                artifacts,
                stages, stages_pass, stages_fail, stages_skipped,
                tests, tests_pass, tests_fail, tests_skipped,
                open_hours, slack,
                build_hours, build_slack, build_duration,
                test_hours, test_slack, test_duration,
                deploy_hours, deploy_slack, deploy_duration,
                is_deployed, is_pass, is_tested, is_trunk)
            VALUES ({{ url }}, {{ connection }},
                {{ repository }}, {{ branch }}, {{ build_type }},
                {{ component }}, {{ version }},
                {{ status }}, {{ rag }}, {{ grade }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ username }},
                {{ created_at }}, {{ started_at }}, {{ ended_at }}, {{ updated_at }},
                {{ built_at }}, {{ tested_at }}, {{ deployed_at }},
                {{ artifacts }},
                {{ stages }}, {{ stages_pass }}, {{ stages_fail }}, {{ stages_skipped }},
                {{ tests }}, {{ tests_pass }}, {{ tests_fail }}, {{ tests_skipped }},
                {{ open_hours }}, {{ slack }},
                {{ build_hours }}, {{ build_slack }}, {{ build_duration }},
                {{ test_hours }}, {{ test_slack }}, {{ test_duration }},
                {{ deploy_hours }}, {{ deploy_slack }}, {{ deploy_duration }},
                {{ is_deployed }}, {{ is_pass }}, {{ is_tested }}, {{ is_trunk }})
            ON CONFLICT (url) DO
            UPDATE SET
                repository = EXCLUDED.repository, branch = EXCLUDED.branch, build_type = EXCLUDED.build_type,
                component = EXCLUDED.component, version = EXCLUDED.version,
                status = EXCLUDED.status, rag = EXCLUDED.rag, grade = EXCLUDED.grade,
                team = EXCLUDED.team, username = EXCLUDED.username,
                created_at = EXCLUDED.created_at, started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at, updated_at = EXCLUDED.updated_at,
                built_at = EXCLUDED.built_at, tested_at = EXCLUDED.tested_at, deployed_at = EXCLUDED.deployed_at,
                artifacts = EXCLUDED.artifacts,
                stages = EXCLUDED.stages, stages_pass = EXCLUDED.stages_pass, stages_fail = EXCLUDED.stages_fail, stages_skipped = EXCLUDED.stages_skipped,
                tests = EXCLUDED.tests, tests_pass = EXCLUDED.tests_pass, tests_fail = EXCLUDED.tests_fail, tests_skipped = EXCLUDED.tests_skipped,
                open_hours = EXCLUDED.open_hours, slack = EXCLUDED.slack,
                build_hours = EXCLUDED.build_hours, build_slack = EXCLUDED.build_slack, build_duration = EXCLUDED.build_duration,
                test_hours = EXCLUDED.test_hours, test_slack = EXCLUDED.test_slack, test_duration = EXCLUDED.test_duration,
                deploy_hours = EXCLUDED.deploy_hours, deploy_slack = EXCLUDED.deploy_slack, deploy_duration = EXCLUDED.deploy_duration,
                is_deployed = EXCLUDED.is_deployed, is_pass = EXCLUDED.is_pass, is_tested = EXCLUDED.is_tested, is_trunk = EXCLUDED.is_trunk;"""
        # upsert
        self._upsert(isql, build)

    def synchronize_build_scope(self, connection: dict, build_url: str, scoped_at: datetime, scope_details: list):
        '''Synchronize TeamCity build commits'''
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO build_scope(build_url, connection, commit_id, issue_id, scoped_at)
            VALUES ({{ build_url }}, {{ connection }}, {{ commit_id }}, {{ issue_id }}, {{ scoped_at }})
            ON CONFLICT (build_url, commit_id) DO NOTHING;"""
        self.database.execute(f"DELETE FROM build_scope WHERE build_url = '{build_url}';")
        self.database.commit()

        for scope_detail in scope_details:
            scope_detail["build_url"] = build_url
            scope_detail["scoped_at"] = scoped_at
            self._upsert(isql, scope_detail)
            events += 1
        self.hub.logger.perf("synchronize_build_scope done", events, time.monotonic() - start)
