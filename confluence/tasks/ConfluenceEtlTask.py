import re
import time
import traceback
from datetime import datetime
from ImagineOpsBaseTask import ImagineOpsBaseTask


class ConfluenceEtlTask(ImagineOpsBaseTask):
    '''ConfluenceEtlTask synchronizes data from Confluence'''
    def __init__(self, hub):
        super(ConfluenceEtlTask, self).__init__(hub)
        self.app_name = "imagineops_confluence"
        self.database = self.hub.connections["database"].new_connection()

    ###########################################################################
    # task_runner.BaseTask API
    ###########################################################################

    def do_discovery(self):
        self.discover_projects()

    def do_hourly(self):
        self.synchronize_pages()

    def do_daily(self):
        self.do_discovery()

    def do_monday(self):
        self.synchronize_pages(duration=30)

    def do_test(self):
        self.do_discovery()
        self.do_hourly()

    def do_bespoke(self):
        if "task" not in self.args:
            return
        if self.args["task"] == "pages":
            self.synchronize_pages()

    ###########################################################################
    # Internal
    ###########################################################################

    def _get_username(self, confluence_content: dict) -> str:
        for field in ["username", "email", "displayName", "publicName"]:
            if field in confluence_content and confluence_content[field] != "":
                return confluence_content[field]
        return ""

    ###########################################################################
    # Tasks
    ###########################################################################

    def discover_projects(self):
        '''Discover projects'''
        self.hub.logger.info("discover_projects started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("confluence"):
            projects = connection.get_all_spaces(start=0, limit=500)["results"]
            for project in projects:
                if project["type"] == "personal":
                    continue
                self.hub.logger.debug(f"""Discovering project: {project["key"]}""")
                events += 1
                self.synchronize_dimension("project", project["key"], project["key"])
                self.synchronize_dimension_attribute("project", project["key"], "is_confluence", "true")
        self.hub.logger.perf("discover_projects done", events, time.monotonic() - start)

    def synchronize_pages(self, duration=2):
        '''Synchronize Confluence pages'''
        self.hub.logger.info("synchronize_pages started")
        start = time.monotonic()
        for connection in self.hub.get_connections_by_type("confluence"):
            for project in connection.connection_config["properties"]["projects"]:
                try:
                    if time.monotonic() - start > 54000:
                        continue
                    self.synchronize_pages_for_project(connection, project, duration)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {project}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_pages done")

    def synchronize_pages_for_project(self, connection, project: str, duration=2) -> int:
        '''
        Synchronize page for selected project

        Args:
            connection:                 Connection to Confluence
            project(str):               project name
        '''
        start = time.monotonic()
        events = 0
        self.hub.logger.info(f"synchronize_pages_for_project {project} started")
        since_sql = "SELECT COALESCE(MAX(updated_at), {{ minSyncDate }}) FROM knowledge WHERE url LIKE {{ url }} AND project = {{ project }};"
        since = self._get_since(since_sql.replace("2 DAYS", f"{duration} DAYS"),
            {"url": connection.connection_config["server"] + "%", "minSyncDate": self.hub.config["properties"]["minSyncDate"], "project": project})

        cql = f"""space={project} AND type=page AND lastModified > {since.strftime("%Y-%m-%d")} ORDER BY lastModified ASC"""
        expand = "metadata.labels,container,version,ancestors,children.comment,comment.body.storage,children.comment.extensions.resolution,children.attachment,children.page,descendents,history"
        confluence_pages = connection.cql(start=0, limit=1000, expand=expand, cql=cql)["results"]
        for confluence_page in confluence_pages:
            try:
                if "content" in confluence_page:
                    confluence_page = confluence_page["content"]
                confluence_page = connection.get_page_by_id(confluence_page["id"], expand=expand)
                page = self.parse_page(connection, project, confluence_page)
                self.synchronize_page(page)
                self.synchronize_page_history(connection, confluence_page)
                self.synchronize_page_feedback(connection, confluence_page)
                events += 1
            except Exception as e:
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf(f"synchronize_pages_for_project {project} done", events, time.monotonic() - start)

    def parse_page(self, connection, project: str, confluence_page: dict) -> dict:
        '''Parse Confluence page'''
        page = {
            "url": connection.connection_config["server"] + confluence_page["_links"]["webui"],
            "connection": connection.connection_config["id"],
            "content_id": confluence_page["id"],
            "summary": confluence_page["title"],
            "status": "Open",
            "content_type": confluence_page["type"],
            "parent_id": "" if len(confluence_page["ancestors"]) == 0 else confluence_page["ancestors"][-1]["id"],
            "project": confluence_page["container"]["key"],
            "username": self._get_username(confluence_page["history"]["createdBy"]),
            "labels": ",".join(label["name"] for label in confluence_page["metadata"]["labels"]["results"]),
            "created_at": confluence_page["history"]["createdDate"].replace("T", " ")[:18],
            "updated_at": confluence_page["version"]["when"].replace("T", " ")[:18],
            "children": confluence_page["children"]["page"]["size"],
            "attachments": confluence_page["children"]["attachment"]["size"],
            "comments": confluence_page["children"]["comment"]["size"],
            "upvotes": len(connection.get_page_likes(confluence_page["id"]))
        }
        return page

    def synchronize_page(self, page: dict):
        '''Synchronize page'''
        self.hub.logger.debug(f"""Synchronizing page {page["summary"]}""")
        isql = """
            INSERT INTO knowledge(url, connection, content_id,
                summary, status, content_type, parent_id,
                project, username, team,
                labels,
                created_at, updated_at,
                children, attachments, comments, upvotes)
            VALUES ({{ url }}, {{ connection }}, {{ content_id }},
                {{ summary }}, {{ status }}, {{ content_type }}, {{ parent_id }},
                {{ project }}, {{ username }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes da WHERE da.dimension_type = 'project' AND da.dimension = {{ project }} AND da.attribute_type = 'team'),
                    (SELECT attribute FROM dimension_attributes da WHERE da.dimension_type = 'user' AND da.dimension = {{ username }} AND da.attribute_type = 'team'),
                    ''),
                {{ labels }},
                {{ created_at }}, {{ updated_at }},
                {{ children }}, {{ attachments }}, {{ comments }}, {{ upvotes }})
            ON CONFLICT (url) DO
            UPDATE SET
                summary = EXCLUDED.summary, status = EXCLUDED.status, content_type = EXCLUDED.content_type,
                project = EXCLUDED.project, username = EXCLUDED.username,
                team = EXCLUDED.team,
                labels = EXCLUDED.labels,
                updated_at = EXCLUDED.updated_at,
                children = EXCLUDED.children, attachments = EXCLUDED.attachments, comments = EXCLUDED.comments,
                upvotes = EXCLUDED.upvotes;"""

        # upsert
        self._upsert(isql, page)

    def synchronize_page_feedback(self, connection, confluence_page: dict):
        isql = """
            INSERT INTO knowledge_feedback(url, content_id, comment_id, username, summary, labels, created_at, team)
            VALUES ({{ url }}, {{ content_id }}, {{ comment_id }}, {{ username }}, {{ summary }}, {{ labels }}, {{ created_at }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes da WHERE da.dimension_type = 'project' AND da.dimension = {{ project }} AND da.attribute_type = 'team'),
                    (SELECT attribute FROM dimension_attributes da WHERE da.dimension_type = 'user' AND da.dimension = {{ username }} AND da.attribute_type = 'team'),
                    '')
            )
            ON CONFLICT (url, content_id, comment_id) DO NOTHING;"""
        comments = connection.get_page_comments(confluence_page["id"])

        for comment in comments["results"]:
            try:
                page_feedback = {
                    "url": connection.connection_config["server"] + confluence_page["_links"]["webui"],
                    "content_id": confluence_page["id"],
                    "comment_id": comment["id"],
                    "summary": comment["body"]["view"]["value"][:255],
                    "project": confluence_page["container"]["key"],
                    "status": self.hub.helpers["MetaHelper"].map_dimension("status", comment["extensions"]["resolution"]["status"]),
                    "username": self._get_username(comment["history"]["createdBy"]),
                    "labels": "",
                    "created_at": comment["history"]["createdDate"].replace("T", " ")[:18]
                }
                self._upsert(isql, page_feedback)
            except Exception as e:
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())

    def synchronize_page_history(self, connection, confluence_page: dict):
        isql = """
            INSERT INTO knowledge_history(url, content_id, version, summary, username, labels, created_at, team)
            VALUES ({{ url }}, {{ content_id }}, {{ version }}, {{ summary }}, {{ username }}, {{ labels }}, {{ created_at }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes da WHERE da.dimension_type = 'project' AND da.dimension = {{ project }} AND da.attribute_type = 'team'),
                    (SELECT attribute FROM dimension_attributes da WHERE da.dimension_type = 'user' AND da.dimension = {{ username }} AND da.attribute_type = 'team'),
                    '')
                )
            ON CONFLICT (url, content_id, version) DO NOTHING;"""
        try:
            history = connection.get_history(confluence_page["id"])
            for change in history["results"]:
                page_history = {
                    "url": connection.connection_config["server"] + confluence_page["_links"]["webui"],
                    "content_id": confluence_page["id"],
                    "version": str(change["number"]),
                    "project": confluence_page["container"]["key"],
                    "summary": change["message"][:255],
                    "username": self._get_username(change["by"]),
                    "labels": "",
                    "created_at": change["when"].replace("T", " ")[:18]
                }
                self._upsert(isql, page_history)
        except Exception as e:
            self.hub.logger.trace(str(e))
            self.hub.logger.trace(traceback.format_exc())
