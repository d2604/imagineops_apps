from __future__ import annotations
import base64
import json
import requests
import time
from atlassian import Confluence


class ConfluenceConnection(Confluence):
    '''
    ConfluenceConnection extends Atlassian library + adds REST access

    **Configuration**

    .. code-block:: json

        {
            "connections": [
                {"id": "yourid", "type": "confluence", "server": "https://your-confluence-server.com", "username": "your-username", "password": "your-password"}
            ]
        }

    **Usage**

    .. code-block:: python

        connection_config = {"id": "yourid", "type": "confluence", "server": "https://your-confluence-server.com", "username": "your-username", "password": "your-password"}
        confluence = ConfluenceConnection(hub, connection_config)
        confluence.find_pages_with_label("beta")

    **Notes**

    - Depends on: https://atlassian-python-api.readthedocs.io/confluence.html
    '''
    def __init__(self, hub, connection_config: dict):
        self.rag = "R"
        self.timeouts = 0
        self.hub = hub
        self.connection_config = connection_config
        self.hub.logger.info(f"""Connecting to Confluence {connection_config["server"]}""")
        if connection_config["properties"]["deploymentType"] == "Cloud":
            super(ConfluenceConnection, self).__init__(url=connection_config["server"], username=connection_config["username"], password=connection_config["password"], cloud=False)
        else:
            super(ConfluenceConnection, self).__init__(url=connection_config["server"], token=connection_config["password"])
        self.check_connection(on_init=True)

    ###########################################################################
    # Public API
    ###########################################################################

    def check_connection(self, on_init: bool = False) -> bool:
        '''
        Determines connection status
            R = unable to connect (bad username/password, etc)
            A = connection isnt reliable (rate limiting, etc)
            G = good
        '''
        success = True
        self.rag = "G"
        try:
            self.health_check()
            if self.timeouts > 10:
                self.rag = "A"
        except Exception as e:  # noqa: F841
            success = False
            self.rag = "R"
        self.timeouts = 0

        # try to reconnect
        if not success and not on_init:
            self.__init__(self.hub, self.connection_config)
        return success

    def get_history(self, page_id: int):
        url = f"rest/api/latest/content/{page_id}/version"
        if self.connection_config["properties"]["deploymentType"] == "Server":
            url = f"rest/experimental/content/{page_id}/version"
        return self.get(url)

    def get_page_comments(self, page_id: int) -> list:
        '''
        Gets comments for a wiki page

        Args:
            page_id(integer):               Confluence page id

        Returns:
            list of comments
        '''
        url = f"rest/api/latest/content/{page_id}/child/comment?location=inline&location=footer&location=resolved&expand=extension.inlineProperties,body.view,extensions.resolution,history.contributors"
        return self.get(url)

    def get_page_likes(self, page_id: int) -> list[str]:
        '''
        Gets all users who liked a page

        Args:
            page_id(integer):               Confluence page id
        Returns:
            list of users who liked the page
        '''
        url = f"rest/likes/1.0/content/{page_id}/likes"
        fields = ["username", "email", "fullName"]
        liking_users = []
        response = self.get(url)
        if "likes" in response:
            for user in response["likes"]:
                username = ""
                for field in fields:
                    if field in user["user"]:
                        username = user["user"][field]
                liking_users.append(username)
        return liking_users
