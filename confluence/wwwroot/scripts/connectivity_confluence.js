/* global $, BaseConnection, dashboardManager */
class ConfluenceConnection extends BaseConnection
{
    edit()
    {
        this.setDefaults();
        let html = this.getDefaultEditHtml();
        html += `
        <h5>Advanced</h5>
        <div id="connectivityAccordion" class="accordion accordion-flush">
        <div class="accordion-item">
            <h2 id="connectivityAdvanced" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityAdvancedConfig" aria-expanded="false" aria-controls="connectivityAdvancedConfig">
            Projects
            </button>
            </h2>
            <div id="connectivityAdvancedConfig" class="accordion-collapse collapse" aria-labelledby="connectivityAdvanced" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-floating mb-3">
                <select id="deploymentType" class="form-select" aria-label="Deployment type">
                    <option value="Cloud" ${(this.connection.properties.deploymentType === "Cloud" ? "selected" : "")}>Cloud</option>
                    <option value="Server" ${(this.connection.properties.deploymentType === "Server" ? "selected" : "")}>Server</option>
                </select>
                <label for="deploymentType">Deployment Type</label>
                </div>
                <br>
                <div class="form-floating mb-3">
                <select id="confluenceProjects" class="form-select form-select-lg" multiple aria-label="Confluence projects"></select>
                <label for="confluenceProjects">Projects</label>
                </div>
            </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 id="connectivityFactsHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityFactsConfig" aria-expanded="false" aria-controls="connectivityFactsConfig">
            Facts
            </button>
            </h2>
            <div id="connectivityFactsConfig" class="accordion-collapse collapse" aria-labelledby="connectivityFactsHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-check"><input id="do_pages" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_pages ? "checked" : "")}>
                <label class="form-check-label" for="do_pages">Pages</label>
                </div>
                <div class="form-check"><input id="do_comments" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_comments ? "checked" : "")}>
                <label class="form-check-label" for="do_comments">Comments</label>
                </div>
                <div class="form-check"><input id="do_history" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_history ? "checked" : "")}>
                <label class="form-check-label" for="do_history">History</label>
                </div>
            </div>
            </div>
        </div>

        </div>`;
        return html;
    }

    populateOptions()
    {
        this.populateProjects();
    }

    populateProjects()
    {
        const d = document.getElementById("confluenceProjects");
        let html = "";
        for (const project of this.connection.properties.projects)
        {
            html += `<option value="${project}" selected>${project}</option>`;
        }
        d.innerHTML = html;

        const url = "/api/latest/config/dimension?type=project&has_attribute=is_confluence";
        $.ajax({
            url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            error: function() {},
            success: function(data)
            {
                const d = document.getElementById("confluenceProjects");
                let html = "";
                for (const dimension of data.dataset)
                {
                    if (!this.connection.properties.projects.includes(dimension.dimension))
                    {
                        html += `<option value="${dimension.dimension}">${dimension.dimension}</option>`;
                    }
                }
                d.innerHTML += html;
            }.bind(this)
        });
    }

    setDefaults()
    {
        if (!Object.prototype.hasOwnProperty.call(this.connection, "properties")) this.connection.properties = {};
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "deploymentType")) this.connection.properties.deploymentType = "Cloud";
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "projects")) this.connection.properties.projects = [];
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_pages")) this.connection.properties.do_pages = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_comments")) this.connection.properties.do_comments = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_history")) this.connection.properties.do_history = false;
        return this.connection;
    }

    update()
    {
        this.connection.properties.deploymentType = document.getElementById("deploymentType").value;
        this.connection.properties.do_pages = document.getElementById("do_pages").checked;
        this.connection.properties.do_comments = document.getElementById("do_comments").checked;
        this.connection.properties.do_history = document.getElementById("do_history").checked;

        const projects = [];
        for (const project of document.getElementById("confluenceProjects").getElementsByTagName("option"))
        {
            if (project.selected)
            {
                projects.push(project.value);
            }
        }
        this.connection.properties.projects = projects;
        return this.connection;
    }
}
dashboardManager.connections.confluence = { class: ConfluenceConnection };
