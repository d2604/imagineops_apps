/* global $, BaseConnection, dashboardManager */
class EmailConnection extends BaseConnection
{
    edit()
    {
        this.setDefaults();
        let html = this.getDefaultEditHtml();
        html += `
        <h5>Advanced</h5>
        <div id="connectivityAccordion" class="accordion accordion-flush">
    
        <div class="accordion-item">
            <h2 id="emailSecurityHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#emailSecurityContent" aria-expanded="false" aria-controls="emailSecurityContent">
            Security
            </button>
            </h2>
            <div id="emailSecurityContent" class="accordion-collapse collapse" aria-labelledby="emailSecurityHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-check"><input id="emailEnableTls" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.enableTls ? "checked" : "")}>
                <label class="form-check-label" for="emailEnableTls">Enable TLS/Workflows</label>
                </div>
            </div>
            </div>
        </div>
        </div>`;
        return html;
    }

    setDefaults()
    {
        if (!Object.prototype.hasOwnProperty.call(this.connection, "properties")) this.connection.properties = {};
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "enableTls")) this.connection.properties.enableTls = false;
        return this.connection;
    }

    update()
    {
        this.connection = this.setDefaults();
        this.connection.properties.enableTls = document.getElementById("emailEnableTls").checked;
        return this.connection;
    }
}
dashboardManager.connections.email = { class: EmailConnection };
