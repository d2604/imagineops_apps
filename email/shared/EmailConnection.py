from __future__ import annotations
import os
import re
import smtplib
import traceback
from urllib.request import urlretrieve
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage


class EmailConnection(object):
    '''EmailConnection simplifies sending HTML-based emails'''
    def __init__(self, hub, connection_config: dict):
        self.hub = hub
        self.rag = "R"
        self.hub.logger.info(f"""Connecting to SMTP {connection_config["server"]}""")
        self.connection_config = connection_config
        # load config
        self.enable_tls = False if "enableTls" not in connection_config["properties"] else connection_config["properties"]["enableTls"]
        mail_css = "mail_style.html" if "mailCss" not in connection_config["properties"] else connection_config["properties"]["mailCss"]
        script_path = os.path.dirname(os.path.abspath(__file__))
        include_path = self.hub.config_service.find_file(mail_css, script_path)
        with open(include_path, "r") as f:
            self.css = f.read()
        self.check_connection(do_reload=False)

    ###########################################################################
    # Internal API
    ###########################################################################

    def _find_image_path(self, src: str) -> str:
        filepath = None

        # check if path is relative vs static http:
        if src.find("http") > -1:
            # download the file locally
            urlretrieve(src, os.path.basename(src))
            filepath = os.path.basename(src)
        else:
            check_paths = [".", "wwwroot", "wwwroot/images", "images", "..", "../images", "../wwwroot", "../wwwroot/images", "static", "static/images"]
            for check_path in check_paths:
                if os.path.isfile(os.path.join(check_path, src)):
                    filepath = f"{check_path}/{src}"
                    break
        return filepath

    ###########################################################################
    # Public API
    ###########################################################################

    def check_connection(self, do_reload: bool = True):
        '''Determine connection status'''
        success = self.rag == "G"
        try:
            s = smtplib.SMTP(host=self.connection_config["server"])
            if self.enable_tls:
                s.starttls()
                s.login(self.connection_config["username"], self.connection_config["password"])
            s.quit()
        except Exception as e:  # noqa: F841
            success = False
            self.rag = "R"
        return success

    def send_email(self, sender_address: str, subject: str, body: str, recipients: list[str], cc_recipients: list[str] = [], bcc_recipients: list[str] = [], reply_address: str = None, files: list[str] = [], is_html: bool = False):
        '''
        Send an email

        Args:
            sender_address (string):    email address for sender
            subject (string):           email subject
            body (string):              email body
            recipients(list):           list of email addresses
            cc_recipients(list):        list of CC email addresses
            bcc_recipients(list):       list of BCC email addresses
            reply_address(string):      optional reply email address
            files(list):                list of files (by Path) to attach to email
            is_html(boolean):           whether the email body is text or html
        '''
        try:
            assert isinstance(recipients, list)
            mail = MIMEMultipart("alternative")

            if is_html:
                image_map = []
                body = self.css + body
                # find all img src and replace with CID reference & attach file
                img_tags = re.findall(r"<img (.*?)>", body)
                for img_tag in img_tags:
                    img_tag = img_tag.replace("\"", "'")
                    img_src = re.findall(r"src='(.*?)'", img_tag)[0]
                    img_id = os.path.splitext(os.path.basename(img_src))[0]
                    img_src_new = f"cid:{img_id}"
                    img_tag_new = img_tag.replace(f"src='{img_src}'", f"src='{img_src_new}'")

                    # find image
                    img_file = self._find_image_path(img_src)
                    if img_file is None:
                        # silent ignore if we cant find the file
                        continue
                    image_map.append(img_file)
                    with open(img_file, "rb") as fp:
                        image = MIMEImage(fp.read())
                    image.add_header("Content-Disposition", "attachment", filename=img_file)
                    image.add_header("Content-ID", img_id)
                    image.add_header("X-Attachment-Id", img_id)
                    mail.attach(image)

                    # update html reference
                    body = body.replace(f"<img {img_tag}>", f"<img {img_tag_new}>")
                content = MIMEText(body, "html")
            else:
                content = MIMEText(body, "text")

            mail.attach(content)
            mail["Subject"] = subject
            mail["From"] = sender_address
            mail["To"] = ",".join(recipients)
            mail["Cc"] = ",".join(cc_recipients)
            mail["Bcc"] = ",".join(bcc_recipients)

            if reply_address:
                mail["Reply-To"] = reply_address
            s = smtplib.SMTP(host=self.connection_config["server"])

            if self.enable_tls:
                s.starttls()
                s.login(self.connection_config["username"], self.connection_config["password"])

            s.sendmail(sender_address, recipients, mail.as_string())
            s.quit()
            self.rag = "G"
        except Exception as e:
            self.hub.logger.warn(str(e))
            self.hub.logger.trace(traceback.format_exc())
            self.rag = "R"
