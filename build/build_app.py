import argparse
import datetime
import hashlib
import json
import os
import shutil
import traceback


def update_manifest(args):
    manifest_path = os.path.join(args.app, "manifest.json")
    manifest = {}
    with open(manifest_path, 'r') as json_file:
        manifest = json.load(json_file)

    components = []
    for component in manifest['components']:
        # calculate md5
        with open(os.path.join(args.app, component['component_path'], component['id']), 'rb') as file_to_check:
            data = file_to_check.read()
            md5 = hashlib.md5(data).hexdigest()
        component['md5'] = md5
        components.append(component)

    manifest['components'] = components
    manifest['version'] = datetime.datetime.now().strftime("%y.%m.%d")
    with open(manifest_path, 'w') as outfile:
        json.dump(manifest, outfile, indent=4)


def build_package(args):
    shutil.make_archive(f"builds/imagineops_{args.app}", 'zip', args.app)


def main():
    parser = argparse.ArgumentParser(
        description="Build App",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-a", "--app", dest="app", default="gitlab", help="Application to build")
    parser.add_argument("-d", "--debug", dest="debug", action="store_true", help="Enable DEBUG-level logging")
    args = parser.parse_args()

    try:
        update_manifest(args)
        build_package(args)
    except Exception as e:
        print("Error: " + str(e))
        traceback.format_exc()


if __name__ == "__main__":
    main()
