from __future__ import annotations
import gitlab
import requests
from urllib3.exceptions import InsecureRequestWarning

# Suppress only the single warning from urllib3 needed
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)


class GitLabConnection(gitlab.Gitlab):
    '''
    GitLab ImagineOps-compatible connection
    - extends the Python gitlab library https://python-gitlab.readthedocs.io/en/stable/api-usage.html

    **Usage**

    .. code-block:: python

        connection_config = {"id": "git", "type": "gitlab", "server": "https://gitlab.com", "username": "your-username", "password": "your-private-token"}
        git = GitLabConnection(hub, connection_config)
    '''
    def __init__(self, hub, connection_config: dict):
        self.hub = hub
        self.rag = "R"
        self.connection_config = connection_config
        self.hub.logger.info(f"""Connecting to Git {connection_config["server"]}""")
        gitlab.Gitlab.__init__(self, connection_config["server"], private_token=connection_config["password"])
        self.check_connection(on_init=True)

    ###########################################################################
    # Public API
    ###########################################################################

    def get_project_names(self) -> list[dict]:
        '''
        Return list of projects based on configured group
        '''
        project_names = []
        group = self.connection_config["properties"]["group"]
        git_group = self.groups.get(group)
        git_projects = git_group.projects.list(archived=False, order_by="path", include_subgroups=True, get_all=True)
        for git_project in git_projects:
            project_names.append({"name": git_project.name, "path": git_project.path})
        return project_names

    def check_connection(self, on_init: bool = False) -> bool:
        '''
        Determines connection status
            R = unable to connect (bad username/password, etc)
            A = connection isnt reliable (rate limiting, etc)
            G = good
        '''
        # assume the best and prove otherwise
        success = True
        self.rag = "G"
        try:
            self.ssl_verify = False
            self.session.verify = False
            self.timeout = 10
            self.auth()
        except Exception as e:  # noqa: F841
            success = False
            self.rag = "R"
            self.hub.logger.error(f"""Unable to connect to {self.connection_config["server"]}""")

        if not success and not on_init:
            self.__init__(self.hub, self.connection_config)
        return success
