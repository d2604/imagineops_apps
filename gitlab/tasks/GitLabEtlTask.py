import os
import re
import time
import traceback
from datetime import datetime
from ImagineOpsBaseTask import ImagineOpsBaseTask


class GitLabEtlTask(ImagineOpsBaseTask):
    '''GitLabEtlTask is responsible for synchronizing data from GitLab'''

    def __init__(self, hub):
        super(GitLabEtlTask, self).__init__(hub)
        self.app_name = "imagineops_gitlab"
        self.grade_to_rag_map = {
            "A": "g",
            "B": "g",
            "C": "a",
            "D": "a",
            "E": "r",
            "F": "r",
            " ": " "
        }
        self.database = self.hub.connections["database"].new_connection()

    ###########################################################################
    # task_runner.BaseTask API
    ###########################################################################

    def do_discovery(self):
        self.discover_projects()
        self.discover_job_stages()
        self.discover_users()

    def do_hourly(self):
        self.synchronize_commits()
        self.synchronize_reviews()
        self.synchronize_builds()
        self.synchronize_issues()

    def do_daily(self):
        self.do_discovery()

    def do_monday(self):
        self.synchronize_commits(duration=30)
        self.synchronize_reviews(duration=30)
        self.synchronize_builds(duration=30)
        self.synchronize_issues(duration=30)

    def do_test(self):
        self.do_discovery()
        self.do_hourly()

    def do_bespoke(self):
        if "task" not in self.args:
            return
        if self.args["task"] == "commits":
            self.synchronize_commits()
        elif self.args["task"] == "builds":
            self.synchronize_builds()
        elif self.args["task"] == "issues":
            self.synchronize_issues()
        elif self.args["task"] == "reviews":
            self.synchronize_reviews()

    ###########################################################################
    # Tasks
    ###########################################################################

    def discover_job_stages(self):
        '''Discover GitLab job stages'''
        self.hub.logger.info("discover_job_stages started")
        for connection in self.hub.get_connections_by_type("gitlab"):
            group = connection.connection_config["properties"]["group"]
            for project in connection.connection_config["properties"]["projects"]:
                try:
                    self.discover_job_stages_for_project(connection, f"{group}/{project}")
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {group}/{project}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("discover_job_stages done")

    def discover_job_stages_for_project(self, connection, project: str):
        self.hub.logger.info(f"discover_job_stages_for_project {project} started")
        start = time.monotonic()
        events = 0
        git_project = connection.projects.get(project)
        if git_project is None:
            self.hub.logger.warn(f"Invalid project {project}")
            return
        # find the most recent pipeline run to determine available job stages
        git_pipelines = git_project.pipelines.list(get_all=False)
        if len(git_pipelines) == 0:
            return
        git_pipeline = git_pipelines[0]
        for git_job in git_pipeline.jobs.list(get_all=True):
            self.hub.logger.debug(f"Discovering stage {git_job.stage}")
            self.synchronize_dimension("status", git_job.stage)
            self.synchronize_dimension_attribute("status", git_job.stage, "is_gitlab", "true")
            events += 1
        self.hub.logger.perf(f"discover_job_stages_for_project {project} done", events, time.monotonic() - start)

    def discover_projects(self):
        '''Discover GitLab projects'''
        self.hub.logger.info("discover_projects started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("gitlab"):
            try:
                git_group = connection.groups.get(connection.connection_config["properties"]["group"])
                for git_project in git_group.projects.list(archived=False, order_by="path", include_subgroups=True, get_all=True):
                    self.hub.logger.debug(f"Discovering project {git_project.path}")
                    self.synchronize_dimension("project", git_project.path)
                    self.synchronize_dimension_attribute("project", git_project.path, "is_gitlab", "true")
                    events += 1
            except Exception as e:
                self.hub.logger.warn(f"""Error synchronizing {connection.connection_config["properties"]["group"]}""")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf("discover_projects done", events, time.monotonic() - start)

    def discover_users(self):
        '''Discover GitLab users'''
        self.hub.logger.info("discover_users started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("gitlab"):
            if not self._is_fact_enabled(connection.connection_config, "users"):
                continue
            try:
                group = connection.connection_config["properties"]["group"]
                git_group = connection.groups.get(group)
                git_users = git_group.members.list(state="active", get_all=True)
                for git_user in git_users:
                    self.hub.logger.debug(f"Discovering user {git_user.username}")
                    self.synchronize_dimension("user", git_user.username)
                    self.synchronize_dimension_attribute("user", git_user.username, "is_gitlab", "true")
                    events += 1
            except Exception as e:
                self.hub.logger.warn(f"Error synchronizing {group}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf("discover_users done", events, time.monotonic() - start)

    def synchronize_builds(self, duration=2):
        '''Synchronize GitLab Pipelines as builds'''
        self.hub.logger.info("synchronize_builds started")
        for connection in self.hub.get_connections_by_type("gitlab"):
            if not self._is_fact_enabled(connection.connection_config, "builds"):
                continue
            for project in connection.connection_config["properties"]["projects"]:
                group = connection.connection_config["properties"]["group"]
                repository = f"{group}/{project}"
                try:
                    git_project = connection.projects.get(repository)
                    if git_project is None:
                        self.hub.logger.warn(f"Error synchronizing {repository}")
                        continue
                    self.synchronize_builds_for_project(connection, repository, git_project, duration)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_builds done")

    def synchronize_builds_for_project(self, connection: dict, repository: str, git_project, duration=2):
        '''Synchronize GitLab Pipelines for project'''
        # iterate over pipelines since last sync
        self.hub.logger.info(f"synchronize_builds_for_project {repository} started")
        start = time.monotonic()
        events = 0
        project_alias = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        since_sql = "SELECT COALESCE(MAX(updated_at) - INTERVAL '2 DAYS', {{ minSyncDate }}) FROM builds WHERE repository = {{ repository }};"
        since = self._get_since(since_sql.replace("2 DAYS", f"{duration} DAYS"),
            {"repository": project_alias, "minSyncDate": self.hub.config["properties"]["minSyncDate"]})
        git_pipelines = git_project.pipelines.list(updated_after=since, order_by="updated_at", sort="asc", get_all=True)
        for git_pipeline in git_pipelines:
            try:
                git_pipeline = git_project.pipelines.get(git_pipeline.id)
                build = self.parse_build(connection, repository, git_project, git_pipeline)
                self.synchronize_build(build)
                events += 1
            except Exception as e:
                self.hub.logger.warn(f"Error synchronizing {repository}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf(f"synchronize_builds_for_project {repository} done", events, time.monotonic() - start)

    def parse_build(self, connection: dict, repository: str, git_project, git_pipeline) -> dict:
        '''Parse GitLab pipeline'''
        # evaluate jobs
        artifacts = stages_pass = stages_fail = stages_skipped = 0
        built_at = tested_at = deployed_at = None
        build_stage_started_at = build_stage_ended_at = None
        test_stage_started_at = test_stage_ended_at = None
        deploy_stage_started_at = deploy_stage_ended_at = None
        build_hours = test_hours = deploy_hours = 0
        build_slack = test_slack = deploy_slack = 0
        git_test_report = git_pipeline.test_report_summary.get()

        git_jobs = git_pipeline.jobs.list(get_all=True)
        for git_job in git_jobs:
            # ignore unstarted jobs
            if git_job.started_at is None:
                continue
            artifacts += len(git_job.artifacts)
            # increment stage stats based on status
            if git_job.status in ["failed", "canceled"]:
                stages_fail += 1
            elif git_job.status in ["success"]:
                stages_pass += 1
            elif git_job.status in ["skipped"]:
                stages_skipped += 1

            # analyze time spent in stages
            job_stage = self.hub.helpers["MetaHelper"].map_dimension("status", git_job.stage)
            job_stage_started_at = datetime.strptime(git_job.started_at[0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
            job_stage_ended_at = datetime.strptime(git_job.finished_at[0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
            if job_stage in ["Building"]:
                built_at = None if git_job.finished_at is None else git_job.finished_at[0:19].replace("T", " ")
                build_hours += 0 if git_job.duration is None else int(git_job.duration)
                build_slack += 0 if git_job.queued_duration is None else int(git_job.queued_duration)
                build_stage_started_at = min(job_stage_started_at, build_stage_started_at or job_stage_started_at)
                build_stage_ended_at = max(job_stage_ended_at, build_stage_ended_at or job_stage_ended_at)
            elif job_stage in ["Testing"]:
                tested_at = None if git_job.finished_at is None else git_job.finished_at[0:19].replace("T", " ")
                test_hours += 0 if git_job.duration is None else int(git_job.duration)
                test_slack += 0 if git_job.queued_duration is None else int(git_job.queued_duration)
                test_stage_started_at = min(job_stage_started_at, test_stage_started_at or job_stage_started_at)
                test_stage_ended_at = max(job_stage_ended_at, test_stage_ended_at or job_stage_ended_at)
            elif job_stage in ["Deploying"]:
                deployed_at = None if git_job.finished_at is None else git_job.finished_at[0:19].replace("T", " ")
                deploy_hours += 0 if git_job.duration is None else int(git_job.duration)
                deploy_slack += 0 if git_job.queued_duration is None else int(git_job.queued_duration)
                deploy_stage_started_at = min(job_stage_started_at, deploy_stage_started_at or job_stage_started_at)
                deploy_stage_ended_at = max(job_stage_ended_at, deploy_stage_ended_at or job_stage_ended_at)

        repository = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        build = {
            "url": git_pipeline.web_url,
            "connection": connection.connection_config["id"],
            "repository": repository,
            "branch": git_pipeline.ref[:255],
            "build_type": "",
            "commit_id": git_pipeline.sha,
            "version": self.hub.helpers["SourceCodeHelper"].parse_version(git_pipeline.ref),
            "status": self.hub.helpers["MetaHelper"].map_dimension("status", git_pipeline.status),
            "component": "",
            "username": self.hub.helpers["MetaHelper"].map_dimension("user", git_pipeline.user["username"]),
            "created_at": git_pipeline.created_at[0:19].replace("T", " "),
            "built_at": built_at,
            "tested_at": tested_at,
            "deployed_at": deployed_at,
            "started_at": None if git_pipeline.started_at is None else git_pipeline.started_at[0:19].replace("T", " "),
            "ended_at": None if git_pipeline.finished_at is None else git_pipeline.finished_at[0:19].replace("T", " "),
            "updated_at": git_pipeline.updated_at[0:19].replace("T", " "),
            "artifacts": artifacts,
            "stages": len(git_jobs),
            "stages_pass": stages_pass,
            "stages_fail": stages_fail,
            "stages_skipped": stages_skipped,
            "tests": git_test_report.total["count"],
            "tests_pass": git_test_report.total["success"],
            "tests_fail": git_test_report.total["failed"],
            "tests_skipped": git_test_report.total["skipped"],
            "open_hours": 0 if git_pipeline.duration is None else git_pipeline.duration,
            "slack": 0 if git_pipeline.queued_duration is None else git_pipeline.queued_duration,
            "build_hours": build_hours / 3600.0,
            "build_slack": build_slack / 3600.0,
            "build_duration": 0 if None in [build_stage_started_at, build_stage_ended_at] else (build_stage_ended_at - build_stage_started_at).total_seconds() / 3600.0,
            "test_hours": test_hours / 3600.0,
            "test_slack": test_slack / 3600.0,
            "test_duration": 0 if None in [test_stage_started_at, test_stage_ended_at] else (test_stage_ended_at - test_stage_started_at).total_seconds() / 3600.0,
            "deploy_hours": deploy_hours / 3600.0,
            "deploy_slack": deploy_slack / 3600.0,
            "deploy_duration": 0 if None in [deploy_stage_started_at, deploy_stage_ended_at] else (deploy_stage_ended_at - deploy_stage_started_at).total_seconds() / 3600.0,
            "is_deployed": deployed_at is not None,
            "is_pass": stages_fail == 0,
            "is_trunk": git_pipeline.ref == git_project.default_branch,
            "is_tested": git_test_report.total["count"] > 0
        }
        build["grade"] = self.hub.helpers["GradingHelper"].get_build_grade(build)
        build["rag"] = self.grade_to_rag_map[build["grade"]]
        return build

    def synchronize_build(self, build):
        isql = """
            INSERT INTO builds(url, connection,
                repository, branch, build_type,
                commit_id, component, version,
                status, rag, grade,
                team, username,
                created_at, started_at, built_at, tested_at, deployed_at, ended_at, updated_at,
                artifacts,
                stages, stages_pass, stages_fail, stages_skipped,
                tests, tests_pass, tests_fail, tests_skipped,
                open_hours, slack,
                build_hours, build_slack, build_duration,
                test_hours, test_slack, test_duration,
                deploy_hours, deploy_slack, deploy_duration,
                is_deployed, is_pass, is_trunk, is_tested)
            VALUES ({{ url }}, {{ connection }},
                {{ repository }}, {{ branch }}, {{ build_type }},
                {{ commit_id }}, {{ component }}, {{ version }},
                {{ status }}, {{ rag }}, {{ grade }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ username }},
                {{ created_at }}, {{ started_at }}, {{ built_at }}, {{ tested_at }}, {{ deployed_at }}, {{ ended_at }}, {{ updated_at }},
                {{ artifacts }},
                {{ stages }}, {{ stages_pass }}, {{ stages_fail }}, {{ stages_skipped }},
                {{ tests }}, {{ tests_pass }}, {{ tests_fail }}, {{ tests_skipped }},
                {{ open_hours }}, {{ slack }},
                {{ build_hours }}, {{ build_slack }}, {{ build_duration }},
                {{ test_hours }}, {{ test_slack }}, {{ test_duration }},
                {{ deploy_hours }}, {{ deploy_slack }}, {{ deploy_duration }},
                {{ is_deployed }}, {{ is_pass }}, {{ is_trunk }}, {{ is_tested }})
            ON CONFLICT (url) DO
            UPDATE SET
                repository = EXCLUDED.repository, branch = EXCLUDED.branch, build_type = EXCLUDED.build_type,
                commit_id = EXCLUDED.commit_id, version = EXCLUDED.version,
                status = EXCLUDED.status, rag = EXCLUDED.rag, grade = EXCLUDED.grade,
                team = EXCLUDED.team, component = EXCLUDED.component, username = EXCLUDED.username,
                created_at = EXCLUDED.created_at, started_at = EXCLUDED.started_at, built_at = EXCLUDED.built_at, tested_at = EXCLUDED.tested_at,
                deployed_at = EXCLUDED.deployed_at, ended_at = EXCLUDED.ended_at, updated_at = EXCLUDED.updated_at,
                artifacts = EXCLUDED.artifacts,
                stages = EXCLUDED.stages, stages_pass = EXCLUDED.stages_pass, stages_fail = EXCLUDED.stages_fail, stages_skipped = EXCLUDED.stages_skipped,
                tests = EXCLUDED.tests, tests_pass = EXCLUDED.tests_pass, tests_fail = EXCLUDED.tests_fail, tests_skipped = EXCLUDED.tests_skipped,
                open_hours = EXCLUDED.open_hours, slack = EXCLUDED.slack,
                build_hours = EXCLUDED.build_hours, build_slack = EXCLUDED.build_slack, build_duration = EXCLUDED.build_duration,
                test_hours = EXCLUDED.test_hours, test_slack = EXCLUDED.test_slack, test_duration = EXCLUDED.test_duration,
                deploy_hours = EXCLUDED.deploy_hours, deploy_slack = EXCLUDED.deploy_slack, deploy_duration = EXCLUDED.deploy_duration,
                is_deployed = EXCLUDED.is_deployed, is_pass = EXCLUDED.is_pass, is_trunk = EXCLUDED.is_trunk, is_tested = EXCLUDED.is_tested;"""
        self._upsert(isql, build)

    def synchronize_commits(self, duration=2):
        '''Synchronize GitLab commits'''
        self.hub.logger.info("synchronize_commits started")
        for connection in self.hub.get_connections_by_type("gitlab"):
            if not self._is_fact_enabled(connection.connection_config, "commits"):
                continue
            group = connection.connection_config["properties"]["group"]
            for project in connection.connection_config["properties"]["projects"]:
                repository = f"{group}/{project}"
                try:
                    self.synchronize_commits_for_project(connection, repository, duration)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_commits done")

    def synchronize_commits_for_project(self, connection, repository: str, duration=2):
        '''Synchronize commits from GitLab project'''
        git_project = connection.projects.get(repository)
        if git_project is None:
            self.hub.logger.warn(f"Invalid project {repository}")
            return
        self.hub.logger.info(f"synchronize_commits_for_project {repository} started")
        start = time.monotonic()
        events = 0

        # iterate over each commit
        project_alias = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        since_sql = "SELECT COALESCE(MAX(started_at) - INTERVAL '2 DAYS', {{ minSyncDate }}) FROM commits WHERE repository = {{ repository }};"
        since = self._get_since(since_sql.replace("2 DAYS", f"{duration} DAYS"),
            {"repository": project_alias, "minSyncDate": self.hub.config["properties"]["minSyncDate"]})
        git_commits = git_project.commits.list(get_all=True, all=True, since=since)
        git_commits.reverse()
        for git_commit in git_commits:
            try:
                git_commit = git_project.commits.get(git_commit.id)
                commit = self.parse_commit(connection, repository, git_project, git_commit)
                self.synchronize_commit(commit)
                self.synchronize_commit_scope(connection, repository, git_commit)
                events += 1
            except Exception as e:
                self.hub.logger.error(f"Error synchronizing {git_commit.id}")
                self.hub.logger.error(str(e))
                self.hub.logger.error(traceback.format_exc())
        self.hub.logger.perf(f"synchronize_commits_for_project {repository} done", events, time.monotonic() - start)

    def parse_commit(self, connection: dict, repository: str, git_project, git_commit) -> dict:
        '''Parse GitLab commit'''
        issue_id = self.hub.helpers["SourceCodeHelper"].parse_issue(git_commit.message)
        files_added = files_deleted = files_code = files_test = lines_code = lines_test = 0
        is_unit_tested = False
        languages = []
        unique_filenames = []

        for diff in git_commit.diff(get_all=True):
            languages.append(self.hub.helpers["SourceCodeHelper"].get_language(diff["new_path"]))
            diff_stats = self.hub.helpers["SourceCodeHelper"].parse_diff_stats(diff["diff"])
            if self.hub.helpers["SourceCodeHelper"].is_test(diff["new_path"]):
                is_unit_tested = True
                lines_test += diff_stats["lines_added"]
                files_test += 1 if diff["new_path"] not in unique_filenames else 0
            elif self.hub.helpers["SourceCodeHelper"].is_code(diff["new_path"]):
                lines_code += diff_stats["lines_added"]
                files_code += 1 if diff["new_path"] not in unique_filenames else 0
            if diff["new_path"] not in unique_filenames:
                unique_filenames.append(diff["new_path"])
                files_added += 1 if diff["new_file"] else 0
                files_deleted += 1 if diff["deleted_file"] else 0

        repository = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        branches = git_commit.refs("branch", get_all=False)
        title = re.findall(r"Merge branch '(.*?)' into '(.*?)'", git_commit.title)
        branch_name = ("" if len(title) < 1 else title[0][0]) if len(branches) == 0 else branches[0]["name"]
        username = self.hub.helpers["MetaHelper"].map_dimension("user", git_commit.committer_name if not hasattr(git_commit, "committer_email") else git_commit.committer_email)
        commit = {
            "url": git_commit.web_url,
            "connection": connection.connection_config["id"],
            "commit_id": git_commit.id,
            "repository": repository,
            "branch": branch_name,
            "summary": git_commit.title[0:255],
            "language": "" if len(languages) == 0 else max(set(languages), key=languages.count),
            "issue": issue_id if not issue_id.isdigit() else f"{git_project.name}#{issue_id}",
            "status": "Closed",
            "rag": "",
            "grade": "",
            "username": username,
            "started_at": git_commit.authored_date[0:19].replace("T", " "),
            "ended_at": git_commit.committed_date[0:19].replace("T", " "),
            "comments": len(git_commit.comments.list(get_all=True)),
            "files": len(unique_filenames),
            "files_added": files_added,
            "files_deleted": files_deleted,
            "files_code": files_code,
            "files_test": files_test,
            "lines_added": git_commit.stats["additions"],
            "lines_removed": git_commit.stats["deletions"],
            "lines_code": lines_code,
            "lines_test": lines_test,
            "is_merge": git_commit.title.find("Merge") == 0,
            "is_trunk": git_project.default_branch == branch_name,
            "is_unit_tested": is_unit_tested
        }
        return commit

    def synchronize_commit(self, commit: dict):
        '''Synchronize commit'''
        isql = """
            INSERT INTO commits(
                url, connection, commit_id, duplicate_commit_id,
                repository, branch,
                summary, issue_id,
                status, rag, grade, language,
                commit_type,
                username, team,
                started_at, ended_at,
                comments,
                files, files_added, files_deleted, files_code, files_test,
                lines_added, lines_removed, lines_code, lines_test,
                is_merge, is_trunk, is_unit_tested,
                is_duplicate)
            VALUES (
                {{ url }}, {{ connection }}, {{ commit_id }},
                COALESCE((SELECT commit_id FROM commits c2 WHERE c2.commit_id <> {{ commit_id }} AND c2.repository = {{ repository }} AND c2.branch = {{ branch }} AND c2.summary = {{ summary }} AND c2.username = {{ username }} AND c2.ended_at < {{ ended_at }} ORDER BY ended_at ASC LIMIT 1), ''),
                {{ repository }}, {{ branch }},
                {{ summary }}, {{ issue }},
                {{ status }}, {{ rag }}, {{ grade }}, {{ language }},
                CASE
                    WHEN COALESCE({{ issue }}, '') <> '' THEN COALESCE((SELECT CASE WHEN issue_type IN ('Bug') THEN 'Maintenance' ELSE 'Creation' END FROM issues WHERE issue_id = {{ issue }} LIMIT 1), 'Unknown')
                    WHEN {{ lines_added }} > 1.25 * {{ lines_removed }} THEN 'Creation'
                    WHEN {{ files_added }} > {{ files_deleted }} THEN 'Creation'
                    WHEN POSITION('fix ' in LOWER({{ summary }})) > 0 THEN 'Maintenance'
                    WHEN POSITION('bug' in LOWER({{ summary }})) > 0 THEN 'Maintenance'
                    WHEN POSITION('defect' in LOWER({{ summary }})) > 0 THEN 'Maintenance'
                    ELSE 'Unknown'
                END,
                {{ username }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ started_at }}, {{ ended_at }},
                {{ comments }},
                {{ files }}, {{ files_added }}, {{ files_deleted }}, {{ files_code }}, {{ files_test }},
                {{ lines_added }}, {{ lines_removed }}, {{ lines_code }}, {{ lines_test }},
                {{ is_merge }}, {{ is_trunk }}, {{ is_unit_tested }},
                COALESCE((SELECT TRUE FROM commits c2 WHERE c2.commit_id <> {{ commit_id }} AND c2.repository = {{ repository }} AND c2.branch = {{ branch }} AND c2.summary = {{ summary }} AND c2.username = {{ username }} AND c2.ended_at < {{ ended_at }} LIMIT 1), FALSE)
                )
            ON CONFLICT (url) DO
            UPDATE SET
                commit_id = EXCLUDED.commit_id, duplicate_commit_id = EXCLUDED.duplicate_commit_id,
                repository = EXCLUDED.repository, branch = EXCLUDED.branch,
                summary = EXCLUDED.summary, issue_id = EXCLUDED.issue_id,
                status = EXCLUDED.status, rag = EXCLUDED.rag, grade = EXCLUDED.grade,
                commit_type = EXCLUDED.commit_type, language = EXCLUDED.language,
                username = EXCLUDED.username, team = EXCLUDED.team,
                started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at,
                comments = EXCLUDED.comments,
                files = EXCLUDED.files, files_added = EXCLUDED.files_added, files_deleted = EXCLUDED.files_deleted, files_code = EXCLUDED.files_code, files_test = EXCLUDED.files_test,
                lines_added = EXCLUDED.lines_added, lines_removed = EXCLUDED.lines_removed, lines_code = EXCLUDED.lines_code, lines_test = EXCLUDED.lines_test,
                is_merge = EXCLUDED.is_merge, is_trunk = EXCLUDED.is_trunk, is_unit_tested = EXCLUDED.is_unit_tested;"""
        self._upsert(isql, commit)

    def synchronize_commit_scope(self, connection: dict, repository: str, git_commit):
        '''Synchronize GitLab commit-related file changes'''
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO commit_scope(
                commit_url, connection, commit_id, filepath, filename,
                component, language, team,
                line_start, line_end,
                lines_added, lines_removed,
                is_new, is_deleted, is_code, is_config, is_test)
            VALUES (
                {{ url }}, {{ connection }}, {{ commit_id }}, {{ filepath }}, {{ filename }},
                {{ component }}, {{ language }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ line_start }}, {{ line_end }},
                {{ lines_added }}, {{ lines_removed }},
                {{ is_new }}, {{ is_deleted }}, {{ is_code }}, {{ is_config }}, {{ is_test }})
            ON CONFLICT (commit_url, filepath, filename) DO
            UPDATE SET
                commit_id = EXCLUDED.commit_id,
                component = EXCLUDED.component, language = EXCLUDED.language, team = EXCLUDED.team,
                line_start = EXCLUDED.line_start, line_end = EXCLUDED.line_end,
                lines_added = EXCLUDED.lines_added, lines_removed = EXCLUDED.lines_removed,
                is_new = EXCLUDED.is_new, is_deleted = EXCLUDED.is_deleted, is_code = EXCLUDED.is_code, is_config = EXCLUDED.is_config, is_test = EXCLUDED.is_test;"""

        # iterate over each diff in the commit
        for diff in git_commit.diff(get_all=True):
            # skip excluded files
            if self.hub.helpers["SourceCodeHelper"].is_excluded(f"""{repository}/{diff["new_path"]}"""):
                continue

            # parse the diff message to determine stats
            diff_stats = self.hub.helpers["SourceCodeHelper"].parse_diff_stats(diff["diff"])

            repository = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
            commit_scope = {
                "url": git_commit.web_url,
                "connection": connection.connection_config["id"],
                "commit_id": git_commit.id,
                "repository": repository,
                "filepath": os.path.split(diff["new_path"])[0],
                "filename": os.path.split(diff["new_path"])[1],
                "username": git_commit.committer_name if not hasattr(git_commit, "committer_email") else git_commit.committer_email,
                "component": "",
                "language": self.hub.helpers["SourceCodeHelper"].get_language(diff["new_path"]),
                "line_start": diff_stats["line_start"],
                "line_end": diff_stats["line_end"],
                "lines_added": diff_stats["lines_added"],
                "lines_removed": diff_stats["lines_removed"],
                "is_new": diff["new_file"],
                "is_deleted": diff["deleted_file"],
                "is_code": self.hub.helpers["SourceCodeHelper"].is_code(diff["new_path"]),
                "is_config": self.hub.helpers["SourceCodeHelper"].is_config(diff["new_path"]),
                "is_test": self.hub.helpers["SourceCodeHelper"].is_test(diff["new_path"])
            }
            self._upsert(isql, commit_scope)
            events += 1
        self.hub.logger.perf("synchronize_commit_scope done", events, time.monotonic() - start)

    def synchronize_issues(self, duration=2):
        '''
        Synchronize GitLab issues

        TODO:
        - IMGN-126: synchronize epics: https://docs.gitlab.com/ee/api/epics.html
        - IMGN-126: synchronize iterations/sprints: https://docs.gitlab.com/ee/api/iterations.html
        '''
        self.hub.logger.info("synchronize_issues started")
        for connection in self.hub.get_connections_by_type("gitlab"):
            if not self._is_fact_enabled(connection.connection_config, "issues"):
                continue
            group = connection.connection_config["properties"]["group"]
            for project in connection.connection_config["properties"]["projects"]:
                repository = f"{group}/{project}"
                try:
                    git_project = connection.projects.get(repository)
                    if git_project is None:
                        self.hub.logger.warn(f"Invalid project {repository}")
                        continue
                    self.synchronize_issues_for_project(connection, repository, git_project, duration)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_issues done")

    def synchronize_issues_for_project(self, connection: dict, repository: str, git_project, duration=2):
        '''Synchronize GitLab Project issues'''
        self.hub.logger.info(f"synchronize_issues_for_project {repository} started")
        start = time.monotonic()
        events = 0
        # iterate over issues updated since last sync
        project_alias = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        since_sql = "SELECT COALESCE(MAX(updated_at) - INTERVAL '2 DAYS', {{ minSyncDate }}) FROM issues WHERE url LIKE {{ repository }};"
        since = self._get_since(since_sql.replace("2 DAYS", f"{duration} DAYS"),
            {"repository": project_alias, "minSyncDate": self.hub.config["properties"]["minSyncDate"]})
        git_issues = git_project.issues.list(updated_after=since, order_by="updated_at", sort="asc", get_all=True)
        for git_issue in git_issues:
            try:
                issue = self.parse_issue(connection, repository, git_issue)
                self.synchronize_issue(issue)
                events += 1
            except Exception as e:
                self.hub.logger.debug(f"Error synchronizing {git_issue.iid}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf(f"synchronize_issues_for_project {repository} done", events, time.monotonic() - start)

    def parse_issue(self, connection: dict, repository, git_issue) -> dict:
        '''Parse GitLab issue'''
        status = self.hub.helpers["MetaHelper"].map_dimension("status", git_issue.state)
        repository = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        issue = {
            "url": git_issue.web_url,
            "connection": connection.connection_config["id"],
            "id": f"""{repository}#{git_issue.iid}""",
            "summary": git_issue.title[:1023],
            "status": status,
            "issue_type": self.hub.helpers["MetaHelper"].map_dimension("issue_type", git_issue.issue_type),
            "project": repository,
            "priority": self.hub.helpers["MetaHelper"].map_dimension("priority", git_issue.severity),
            "component": "",
            "username": self.hub.helpers["MetaHelper"].map_dimension("user", "" if git_issue.assignee is None else git_issue.assignee["username"]),
            "reporter": self.hub.helpers["MetaHelper"].map_dimension("user", "" if git_issue.author is None else git_issue.author["username"]),
            "closer": self.hub.helpers["MetaHelper"].map_dimension("user", "" if len(git_issue.closed_by()) == 0 else git_issue.closed_by()[0]["username"]),
            "created_at": git_issue.created_at[0:19].replace("T", " "),
            "started_at": git_issue.created_at[0:19].replace("T", " "),
            "ended_at": None if git_issue.closed_at is None else git_issue.closed_at[0:19].replace("T", " "),
            "due_at": None if git_issue.due_date is None else git_issue.due_date,
            "updated_at": git_issue.updated_at[0:19].replace("T", " "),
            "resolution": "Done" if status == "Done" else "",
            "labels": "" if git_issue.labels is None else ",".join(git_issue.labels)[:2047],
            "estimate": git_issue.time_stats()["time_estimate"] / 3600.0,
            "effort": git_issue.time_stats()["total_time_spent"] / 3600.0,
            "is_noise": False
        }
        return issue

    def synchronize_issue(self, issue: dict):
        isql = """
            INSERT INTO issues(url, connection, issue_id, summary, status, issue_type,
                project, priority, team, component,
                username, reporter, closer,
                created_at, started_at, ended_at, due_at, updated_at,
                resolution, labels,
                effort_estimate, effort,
                is_noise)
            VALUES ({{ url }}, {{ connection }}, {{ id }}, {{ summary }}, {{ status }}, {{ issue_type }},
                {{ project }}, {{ priority }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ project }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ component }},
                {{ username }}, {{ reporter }}, {{ closer }},
                {{ created_at }}, {{ created_at }}, {{ ended_at }},
                {{ due_at }}, {{ updated_at }},
                {{ resolution }}, {{ labels }},
                {{ estimate }}, {{ effort }},
                {{ is_noise }})
            ON CONFLICT (url) DO
            UPDATE SET
                issue_id = EXCLUDED.issue_id, summary = EXCLUDED.summary, status = EXCLUDED.status, issue_type = EXCLUDED.issue_type,
                project = EXCLUDED.project, priority = EXCLUDED.priority, team = EXCLUDED.team, component = EXCLUDED.component,
                username = EXCLUDED.username, reporter = EXCLUDED.reporter, closer = EXCLUDED.closer,
                created_at = EXCLUDED.created_at, started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at,
                due_at = EXCLUDED.due_at, updated_at = EXCLUDED.updated_at,
                resolution = EXCLUDED.resolution, labels = EXCLUDED.labels,
                effort_estimate = EXCLUDED.effort_estimate, effort = EXCLUDED.effort,
                is_noise = EXCLUDED.is_noise;"""
        self._upsert(isql, issue)

    def synchronize_reviews(self, duration=2):
        '''Synchronize GitLab merge requests as reviews'''
        self.hub.logger.info("synchronize_reviews started")
        for connection in self.hub.get_connections_by_type("gitlab"):
            if not self._is_fact_enabled(connection.connection_config, "reviews"):
                continue
            for project in connection.connection_config["properties"]["projects"]:
                group = connection.connection_config["properties"]["group"]
                repository = f"{group}/{project}"
                try:
                    git_project = connection.projects.get(repository)
                    if git_project is None:
                        self.hub.logger.warn(f"Invalid project {repository}")
                        continue
                    self.synchronize_reviews_for_project(connection, repository, git_project, duration)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_reviews done")

    def synchronize_reviews_for_project(self, connection: dict, repository: str, git_project, duration=2):
        '''Synchronize GitLab Project merge requests'''
        self.hub.logger.info(f"synchronize_reviews_for_project {repository} started")
        start = time.monotonic()
        events = 0
        # synchronize merge requests since last sync
        project_alias = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        since_sql = "SELECT COALESCE(MAX(updated_at) - INTERVAL '2 DAYS', {{ minSyncDate }}) FROM reviews WHERE repository = {{ repository }};"
        since = self._get_since(since_sql.replace("2 DAYS", f"{duration} DAYS"),
            {"repository": project_alias, "minSyncDate": self.hub.config["properties"]["minSyncDate"]})
        git_merge_requests = git_project.mergerequests.list(updated_after=since, order_by="updated_at", sort="asc", get_all=True)
        for git_merge_request in git_merge_requests:
            try:
                git_merge_request = git_project.mergerequests.get(git_merge_request.iid)
                review = self.parse_review(connection, repository, git_project, git_merge_request)
                self.synchronize_review(review)
                self.synchronize_review_feedback(connection, git_merge_request)
                self.synchronize_review_scope(connection, git_project, git_merge_request)
                self.synchronize_review(review)
                events += 1
            except Exception as e:
                self.hub.logger.warn(f"Error synchronizing {repository}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf(f"synchronize_reviews_for_project {repository} done", events, time.monotonic() - start)

    def parse_review(self, connection: dict, repository: str, git_project, git_merge_request) -> dict:
        '''Parse GitLab merge request'''
        issue_id = self.hub.helpers["SourceCodeHelper"].parse_issue(f"{git_merge_request.title}\n{git_merge_request.description}")
        work_started_at = implemented_at = commit_created_at = None
        is_new = is_unit_tested = False
        files_code = files_test = 0
        is_fork = git_merge_request.target_project_id != git_merge_request.source_project_id

        # analyze changes
        git_changes = git_merge_request.changes(get_all=True)
        unique_filenames = []
        languages = []
        lines_added = lines_removed = lines_code = lines_test = 0
        for git_change in git_changes["changes"]:
            is_new = is_new or git_change["new_file"]
            diff_stats = self.hub.helpers["SourceCodeHelper"].parse_diff_stats(git_change["diff"])
            lines_added += diff_stats["lines_added"]
            lines_removed += diff_stats["lines_removed"]

            language = self.hub.helpers["SourceCodeHelper"].get_language(git_change["new_path"])
            languages.append(language)

            if self.hub.helpers["SourceCodeHelper"].is_test(git_change["new_path"]):
                is_unit_tested = True
                lines_test += diff_stats["lines_added"]
                files_test += 1 if git_change["new_path"] not in unique_filenames else 0
            elif self.hub.helpers["SourceCodeHelper"].is_code(git_change["new_path"]):
                lines_code += diff_stats["lines_added"]
                files_code += 1 if git_change["new_path"] not in unique_filenames else 0

            if git_change["new_path"] not in unique_filenames:
                unique_filenames.append(git_change["new_path"])

        # analyze commits
        git_commits = git_merge_request.commits(get_all=True)
        for git_commit in git_commits:
            commit_created_at = datetime.strptime(git_commit.created_at[0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
            if work_started_at is None or commit_created_at < work_started_at:
                work_started_at = commit_created_at
            if implemented_at is None or commit_created_at > implemented_at:
                implemented_at = commit_created_at

        repository = self.hub.helpers["MetaHelper"].map_dimension("project", repository)
        status = "Closed" if git_merge_request.state in ["closed", "merged"] else "Open"
        username = self.hub.helpers["MetaHelper"].map_dimension("user", git_merge_request.author["username"])
        review = {
            "url": git_merge_request.web_url,
            "connection": connection.connection_config["id"],
            "id": git_merge_request.id,
            "repository": repository,
            "source_branch": git_merge_request.source_branch,
            "branch": git_merge_request.target_branch,
            "summary": git_merge_request.title[:255],
            "status": status,
            "issue": issue_id if not issue_id.isdigit() else f"{git_project.name}#{issue_id}",
            "username": username,
            "component": "",
            "language": "" if len(languages) == 0 else max(set(languages), key=languages.count),
            "work_started_at": None if work_started_at is None else work_started_at.strftime("%Y-%m-%d %H:%M:%S"),
            "implemented_at": None if implemented_at is None else implemented_at.strftime("%Y-%m-%d %H:%M:%S"),
            "created_at": git_merge_request.created_at[0:19].replace("T", " "),
            "assigned_at": None,
            "started_at": git_merge_request.created_at[0:19].replace("T", " "),
            "ended_at": None if (git_merge_request.merged_at or git_merge_request.closed_at) is None else (git_merge_request.merged_at or git_merge_request.closed_at)[0:19].replace("T", " "),
            "updated_at": git_merge_request.updated_at[0:19].replace("T", " "),
            "labels": ",".join(git_merge_request.labels)[:2047],
            "upvotes": git_merge_request.upvotes,
            "downvotes": git_merge_request.downvotes,
            "comments": git_merge_request.user_notes_count,
            "uploads": len(git_commits),
            "files": len(unique_filenames),
            "files_code": files_code,
            "files_test": files_test,
            "lines_added": lines_added,
            "lines_removed": lines_removed,
            "lines_code": lines_code,
            "lines_test": lines_test,
            "reviewers": len(git_merge_request.reviewers),
            "review_effort": 0 if git_merge_request.time_stats()["total_time_spent"] is None else (int(git_merge_request.time_stats()["total_time_spent"]) / 3600.0),
            "is_open": git_merge_request.state not in ["closed", "merged"],
            "is_approved": git_merge_request.approvals.get().approved,
            "is_new": is_new,
            "is_noise": git_merge_request.state == "closed",
            "is_draft": git_merge_request.draft or git_merge_request.work_in_progress or any(ext in git_merge_request.title.lower() for ext in ["draft", "wip"]),
            "is_fork": is_fork,
            "is_squashed": git_merge_request.squash,
            "is_trunk": git_merge_request.target_branch == git_project.default_branch,
            "is_unit_tested": is_unit_tested
        }
        review["grade"] = self.hub.helpers["GradingHelper"].get_review_grade(review)
        review["rag"] = self.grade_to_rag_map[review["grade"]]
        return review

    def synchronize_review(self, review: dict):
        # synchronize individual merge request
        isql = """
            INSERT INTO reviews(url, connection,
                review_id, repository, source_branch, branch,
                summary, status, rag, grade,
                username, issue_id,
                team, component,
                language, labels,
                work_started_at, implemented_at,
                created_at, assigned_at,
                started_at, reviewed_at, approved_at,
                ended_at, updated_at,
                upvotes, downvotes, comments,
                feedback,
                defects,
                uploads, files, files_code, files_test,
                lines_added, lines_removed, lines_code, lines_test,
                reviewers, review_effort,
                open_hours, active_hours, review_hours, slack,
                is_open, is_draft, is_fork, is_new, is_noise,
                is_squashed, is_trunk, is_unit_tested,
                is_approved, is_pre_approved, has_feedback)
            SELECT
                {{ url }}, {{ connection }},
                {{ id }}, {{ repository }}, {{ source_branch }}, {{ branch }},
                {{ summary }}, {{ status }}, {{ rag }}, {{ grade }},
                {{ username }}, {{ issue }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ component }}, {{ language }}, {{ labels }},
                {{ work_started_at }}, {{ implemented_at }},
                {{ created_at }}, {{ assigned_at }},
                COALESCE(MIN(CASE WHEN crf.feedback_type IN ('Approval', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END), {{ created_at }}::timestamp) AS started_at,
                MAX(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN crf.started_at ELSE NULL END) AS reviewed_at,
                MAX(CASE WHEN crf.feedback_type IN ('Approval') THEN crf.started_at ELSE NULL END) AS approved_at,
                {{ ended_at }}, {{ updated_at }},
                {{ upvotes }}, {{ downvotes }}, {{ comments }},
                (SELECT COUNT(*) FROM review_feedback crf WHERE crf.url = {{ url }} AND crf.username != {{ username }}) AS feedback,
                SUM(CASE WHEN crf.feedback_type IN ('Bug') THEN 1 ELSE 0 END),
                {{ uploads }}, {{ files }}, {{ files_code }}, {{ files_test }},
                {{ lines_added }}, {{ lines_removed }}, {{ lines_code }}, {{ lines_test }},
                {{ reviewers }}, {{ review_effort }},
                EXTRACT(epoch FROM LEAST({{ ended_at }}::timestamp, now()) - {{ created_at }}::timestamp) / 3600 AS open_hours,
                COALESCE(EXTRACT(epoch FROM
                    MAX(CASE WHEN crf.feedback_type IN ('Approved', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END) -
                    MIN(CASE WHEN crf.feedback_type IN ('Approved', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END)) / 3600, 0) AS active_hours,
                COALESCE(EXTRACT(epoch FROM
                    MAX(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN crf.started_at ELSE NULL END) -
                    MIN(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN crf.started_at ELSE NULL END)) / 3600, 0) AS review_hours,
                COALESCE(EXTRACT(epoch FROM
                    MIN(CASE WHEN crf.feedback_type IN ('Approval', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END) -
                    {{ created_at }}::timestamp) / 3600, 0) AS slack,
                {{ is_open }}, {{ is_draft }}, {{ is_fork }}, {{ is_new }}, {{ is_noise }},
                {{ is_squashed }}, {{ is_trunk }}, {{ is_unit_tested }},
                {{ is_approved }},
                SUM(CASE WHEN crf.feedback_type IN ('Approval') AND crf.started_at >= {{ implemented_at }}::timestamp THEN 1 ELSE 0 END) > 0 AS is_preapproved,
                SUM(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN 1 ELSE 0 END) > 0 AS has_feedback
            FROM
                (SELECT {{ url }} AS url) AS x
                LEFT OUTER JOIN review_feedback crf ON
                    x.url = crf.url
            GROUP BY x.url, crf.url
            ON CONFLICT (url) DO
            UPDATE SET
                review_id = EXCLUDED.review_id, repository = EXCLUDED.repository, source_branch = EXCLUDED.source_branch, branch = EXCLUDED.branch,
                summary = EXCLUDED.summary, status = EXCLUDED.status, rag = EXCLUDEd.rag, grade = EXCLUDED.grade,
                username = EXCLUDED.username, issue_id = EXCLUDED.issue_id,
                team = EXCLUDED.team, component = EXCLUDED.component, language = EXCLUDED.language, labels = EXCLUDED.labels,
                work_started_at = EXCLUDED.work_started_at, implemented_at = EXCLUDED.implemented_at, created_at = EXCLUDED.created_at,
                assigned_at = EXCLUDED.assigned_at, started_at = EXCLUDED.started_at, reviewed_at = EXCLUDED.reviewed_at,
                approved_at = EXCLUDED.approved_at, ended_at = EXCLUDED.ended_at, updated_at = EXCLUDED.updated_at,
                upvotes = EXCLUDED.upvotes, downvotes = EXCLUDED.downvotes, comments = EXCLUDED.comments, feedback = EXCLUDED.feedback,
                defects = EXCLUDED.defects,
                uploads = EXCLUDED.uploads, files = EXCLUDED.files, files_code = EXCLUDED.files_code, files_test = EXCLUDED.files_test,
                lines_added = EXCLUDED.lines_added, lines_removed = EXCLUDED.lines_removed, lines_code = EXCLUDED.lines_code, lines_test = EXCLUDED.lines_test,
                reviewers = EXCLUDED.reviewers, review_effort = EXCLUDED.review_effort,
                open_hours = EXCLUDED.open_hours, active_hours = EXCLUDED.active_hours, review_hours = EXCLUDED.review_hours, slack = EXCLUDED.slack,
                is_open = EXCLUDED.is_open, is_draft = EXCLUDED.is_draft, is_fork = EXCLUDED.is_fork, is_new = EXCLUDED.is_new, is_noise = EXCLUDED.is_noise,
                is_squashed = EXCLUDED.is_squashed, is_trunk = EXCLUDED.is_trunk, is_unit_tested = EXCLUDED.is_unit_tested,
                is_approved = EXCLUDED.is_approved, is_pre_approved = EXCLUDED.is_pre_approved, has_feedback = EXCLUDED.has_feedback;"""
        self._upsert(isql, review)

    def synchronize_review_feedback(self, connection, git_merge_request):
        '''Synchronize GitLab Merge Request feedback'''
        self.hub.logger.debug("synchronize_review_feedback started")
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO review_feedback(url, connection, username, started_at,
                review_id, status, feedback_type, role,
                ended_at, is_open)
            VALUES({{ url }}, {{ connection }}, {{ username }}, {{ feedback_started }},
                {{ review_id }}, {{ feedback_status }}, {{ feedback_type }}, {{ feedback_role }},
                {{ feedback_ended }}, {{ feedback_is_open }});"""

        # delete & re-create for simplicity
        self.database.execute(f"""DELETE FROM review_feedback WHERE url = '{git_merge_request.web_url}';""")
        self.database.commit()

        # process code review feedback
        git_notes = git_merge_request.notes.list(get_all=True)
        for git_note in git_notes:
            try:
                review_feedback = self.parse_review_feedback(connection, git_merge_request, git_note)
                if review_feedback is not None:
                    self._upsert(isql, review_feedback)
                    events += 1
            except Exception as e:
                self.hub.logger.debug(f"Error synchronizing {git_note.id}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf("synchronize_review_feedback done", events, time.monotonic() - start)

    def parse_review_feedback(self, connection: dict, git_merge_request, git_note):
        note_body = "" if not hasattr(git_note, "body") else git_note.body
        feedback_type = "Bug" if hasattr(git_note, "resolvable") and git_note.resolvable else "Comment"
        status = "Open"
        ended_at = None

        # ignore all system comments EXCEPT for final approval
        if git_note.system:
            if note_body.find("approved") > -1:
                feedback_type = "Approval"
                status = "Closed"
                ended_at = git_note.created_at[0:19].replace("T", " ")
            else:
                # do not persist internal system updates
                return

        # check if comment is resolved
        if hasattr(git_note, "resolved") and git_note.resolved:
            status = "Closed"
            ended_at = git_note.resolved_at[0:19].replace("T", " ")

        review_feedback = {
            "url": git_merge_request.web_url,
            "connection": connection.connection_config["id"],
            "username": self.hub.helpers["MetaHelper"].map_dimension("user", git_note.author["username"]),
            "review_id": git_merge_request.id,
            "id": git_note.id,
            "feedback_status": status,
            "feedback_type": feedback_type,
            "feedback_role": "Reviewer" if git_note.author["username"] != git_merge_request.author["username"] else "Author",
            "feedback_started": git_note.created_at[0:19].replace("T", " "),
            "feedback_ended": ended_at,
            "feedback_is_open": status != "Closed"
        }
        return review_feedback

    def synchronize_review_scope(self, connection, git_project, git_merge_request):
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO review_scope(url, connection, review_id, commit_id, scoped_at, implemented_at)
            VALUES ({{ url }}, {{ connection }}, {{ review_id }}, {{ commit_id }}, {{ scoped_at }}, {{ implemented_at }})
            ON CONFLICT (url, commit_id) DO NOTHING;"""
        git_commits = git_merge_request.commits(get_all=True)
        for git_commit in git_commits:
            git_commit = git_project.commits.get(git_commit.id)
            review_scope = {
                "url": git_merge_request.web_url,
                "connection": connection.connection_config["id"],
                "review_id": git_merge_request.id,
                "commit_id": git_commit.id,
                "scoped_at": datetime.strptime(git_commit.created_at[0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S"),
                "implemented_at": datetime.strptime(git_commit.authored_date[0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
            }
            self._upsert(isql, review_scope)
            events += 1
        self.hub.logger.perf("synchronize_review_scope done", events, time.monotonic() - start)
