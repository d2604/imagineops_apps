/* global $, BaseConnection, dashboardManager */
class GitHubConnection extends BaseConnection
{
    edit()
    {
        this.setDefaults();
        let html = this.getDefaultEditHtml();
        html += `
        <h5>Advanced</h5>
        <div id="connectivityAccordion" class="accordion accordion-flush">
    
        <div class="accordion-item">
            <h2 id="connectivityAdvanced" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityAdvancedConfig" aria-expanded="false" aria-controls="connectivityAdvancedConfig">
            Projects
            </button>
            </h2>
            <div id="connectivityAdvancedConfig" class="accordion-collapse collapse" aria-labelledby="connectivityAdvanced" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-floating mb-3">
                <input id="githubGroup" type="text" class="form-control" placeholder="${this.connection.properties.group}" value="${this.connection.properties.group}">
                <label for="githubGroup">Group</label>
                </div>
    
                <div class="form-floating mb-3">
                <select id="githubProjects" class="form-select form-select-lg" multiple aria-label="GitHub Projects"></select>
                <label for="githubProjects">Projects</label>
                </div>
            </div>
            </div>
        </div>
    
        <div class="accordion-item">
            <h2 id="connectivityFactsHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityFactsConfig" aria-expanded="false" aria-controls="connectivityFactsConfig">
            Facts
            </button>
            </h2>
            <div id="connectivityFactsConfig" class="accordion-collapse collapse" aria-labelledby="connectivityFactsHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-check"><input id="do_builds" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_builds ? "checked" : "")}>
                <label class="form-check-label" for="do_builds">Builds/Workflows</label>
                </div>
                <div class="form-check"><input id="do_commits" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_commits ? "checked" : "")}>
                <label class="form-check-label" for="do_commits">Commits</label>
                </div>
                <div class="form-check"><input id="do_issues" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_issues ? "checked" : "")}>
                <label class="form-check-label" for="do_issues">Issues</label>
                </div>
                <div class="form-check"><input id="do_reviews" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_reviews ? "checked" : "")}>
                <label class="form-check-label" for="do_reviews">Merge/Pull Requests</label>
                </div>
                <div class="form-check"><input id="do_users" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_users ? "checked" : "")}>
                <label class="form-check-label" for="do_users">Users</label>
                </div>
            </div>
            </div>
        </div>
        </div>`;
        return html;
    }

    populateOptions()
    {
        const d = document.getElementById("githubProjects");
        let html = "";
        for (const project of this.connection.properties.projects)
        {
            html += `<option value="${project}" selected>${project}</option>`;
        }
        d.innerHTML = html;

        const url = `/api/latest/config/connectivity/github/${this.connection.id}/project`;
        $.ajax({
            url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            error: function() {},
            success: function(data)
            {
                const d = document.getElementById("githubProjects");
                let html = "";
                for (const project of data.dataset)
                {
                    if (!this.connection.properties.projects.includes(project.name))
                    {
                        html += `<option value="${project.name}">${project.value}</option>`;
                    }
                }
                d.innerHTML += html;
            }.bind(this)
        });
    }

    setDefaults()
    {
        if (!Object.prototype.hasOwnProperty.call(this.connection, "properties")) this.connection.properties = {};
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "group")) this.connection.properties.group = "";
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "projects")) this.connection.properties.projects = [];
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_builds")) this.connection.properties.do_builds = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_commits")) this.connection.properties.do_commits = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_issues")) this.connection.properties.do_issues = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_reviews")) this.connection.properties.do_reviews = false;
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_users")) this.connection.properties.do_users = false;
        return this.connection;
    }

    update()
    {
        // update the in-memory version of the connection object so that it can be persisted via REST
        this.connection.properties.group = (document.getElementById("githubGroup") ? document.getElementById("githubGroup").value : "");
        this.connection.properties.do_builds = document.getElementById("do_builds").checked;
        this.connection.properties.do_commits = document.getElementById("do_commits").checked;
        this.connection.properties.do_issues = document.getElementById("do_issues").checked;
        this.connection.properties.do_reviews = document.getElementById("do_reviews").checked;
        this.connection.properties.do_users = document.getElementById("do_users").checked;

        const projects = [];
        for (const project of document.getElementById("githubProjects").getElementsByTagName("option"))
        {
            if (project.selected)
            {
                projects.push(project.value);
            }
        }
        this.connection.properties.projects = projects;
        return this.connection;
    }
}
dashboardManager.connections.github = { class: GitHubConnection };
