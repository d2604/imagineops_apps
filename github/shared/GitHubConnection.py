from __future__ import annotations
from github import Github
from pathlib import Path


class GitHubConnection(Github):
    '''
    GitHubConnection simplifies accessing data from GitHub by extending the
    PyGitHub library https://pygithub.readthedocs.io/en/latest/reference.html

    **Usage**

    .. code-block:: python

        connection_config = DefaultGitHubConfiguration
        git = GitHubConnection(hub, connection_config)
        git.clone_project("group/project")

    **Notes**

    - running system has git installed
    - running system has SSH keys pre-configured with Git server
    - Hub is pre-configured with: ConfigService, Logger
    '''
    def __init__(self, hub, connection_config: dict):
        self.hub = hub
        self.rag = "R"
        self.connection_config = connection_config
        self.hub.logger.info(f"""Connecting to Git {connection_config["server"]}""")
        Github.__init__(self, connection_config["password"])
        self.check_connection(do_refresh=False)

    ###########################################################################
    # Public API
    ###########################################################################

    def build_commit_url(self, server_url: str, group: str, project: str, commit_hash: str) -> str:
        return f"{server_url}/{group}/{project}/commit/{commit_hash}"

    def build_file_url(self, group: str, project: str, filepath: Path, branch: str = "main") -> str:
        relative_path = str(filepath)[str(filepath).find(project) + len(project) + 1:]
        relative_path = relative_path.replace("\\", "/")
        if relative_path != "":
            relative_path = "/" + relative_path
        url = f"""{self.connection_config["server"]}/{group}/{project}/tree/{branch}/{relative_path}"""
        url = url.replace("//", "/")
        return url

    def check_connection(self, do_refresh: bool = True) -> bool:
        '''
        Determines connection status

        Args:
            do_refresh(bool):               Whether to refresh a bad connection
        Returns:
            boolean
        '''
        # assume the best and prove otherwise
        success = True
        self.rag = "G"
        self.get_rate_limit()

        # connection is !reliable if we have <10% of rate limit remaining
        if 100.0 * self.rate_limiting[0] / self.rate_limiting[1] < 10:
            self.rag = "A"
            success = False

        if not success and not do_refresh:
            self.__init__(self.hub, self.connection_config)
        return success

    def get_project_names(self) -> list[dict]:
        '''
        Get projects for group

        Args:
            include_archived(bool):         Whether to included archived projects
            include_stale(bool):            Whether to include archives that have not been updated in over 1 year
        Returns:
            List of project names
        '''
        project_names = []
        group = self.connection_config["properties"]["group"]
        git_group = None
        try:
            git_group = self.get_organization(group)
        except Exception as e:  # noqa: F841
            pass

        if git_group is None:
            try:
                git_group = self.get_user(group)
            except Exception as e:  # noqa: F841
                return project_names

        git_repositories = git_group.get_repos(type="all", sort="full_name", direction="asc")
        for git_repository in git_repositories:
            if git_repository.archived:
                continue
            project_names.append({"name": git_repository.name, "path": git_repository.name})
        return project_names
