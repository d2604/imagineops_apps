import os
import time
import traceback
from datetime import datetime
from ImagineOpsBaseTask import ImagineOpsBaseTask


class GitHubEtlTask(ImagineOpsBaseTask):
    '''
    GitHubEtlTask synchronizes the following facts from GitHub:

     - commits
     - issues
     - pull requests (as reviews)
     - workflows (as builds)

    **Notes**

    - Depends on PyGithub library (https://pygithub.readthedocs.io/en/latest/)
    '''
    def __init__(self, hub):
        super(GitHubEtlTask, self).__init__(hub)
        self.app_name = "imagineops_github"
        self.default_page_size = 100
        self.grade_to_rag_map = {
            "A": "g",
            "B": "g",
            "C": "a",
            "D": "a",
            "E": "r",
            "F": "r",
            " ": " "
        }
        self.database = self.hub.connections["database"].new_connection()

    ###########################################################################
    # task_runner.BaseTask API
    ###########################################################################

    def do_discovery(self):
        self.discover_projects()
        self.discover_job_stages()
        self.discover_users()

    def do_hourly(self):
        self.synchronize_commits()
        self.synchronize_reviews()
        self.synchronize_builds()
        self.synchronize_issues()

    def do_daily(self):
        self.do_discovery()

    def do_test(self):
        self.do_discovery()
        self.do_hourly()

    def do_bespoke(self):
        if "task" not in self.args:
            return
        if self.args["task"] == "commits":
            self.synchronize_commits()
        elif self.args["task"] == "builds":
            self.synchronize_builds()
        elif self.args["task"] == "issues":
            self.synchronize_issues()
        elif self.args["task"] == "reviews":
            self.synchronize_reviews()

    ###########################################################################
    # Tasks
    ###########################################################################

    def discover_job_stages(self):
        '''Discover job stages'''
        self.hub.logger.info("discover_job_stages started")
        for connection in self.hub.get_connections_by_type("github"):
            for project in connection.connection_config["properties"]["projects"]:
                self.discover_job_stages_for_project(connection, project)
        self.hub.logger.info("discover_job_stages done")

    def discover_job_stages_for_project(self, connection, project: str):
        '''Discover job stages for project'''
        self.hub.logger.info(f"discover_job_stages_for_project {project} started")
        start = time.monotonic()
        group = connection.connection_config["properties"]["group"]
        repository = f"{group}/{project}"
        # github does not support a "since" query filter, so we use pagination
        task_progress = self.get_task_progress("GitHubEtlTask", "discover_job_stages", repository)
        task_instances = 0
        try:
            git_repository = connection.get_repo(repository)
            git_repository._requester.per_page = self.default_page_size
            git_workflows = git_repository.get_workflows().get_page(task_progress["progress"])

            for git_workflow in git_workflows:
                if git_workflow.state != "active":
                    task_instances += 1
                    continue
                self.hub.logger.debug(f"Discovering stage {git_workflow.name}")
                self.synchronize_dimension("status", git_workflow.name)
                self.synchronize_dimension_attribute("status", git_workflow.name, "is_github", "true")
                task_instances += 1

                # add check runs
                git_workflow_run = git_workflow.get_runs()[0]
                workflow_raw = git_workflow_run.raw_data
                git_check_suite = git_repository.get_check_suite(workflow_raw["check_suite_id"])
                git_check_runs = git_check_suite.get_check_runs()
                for git_check_run in git_check_runs:
                    self.hub.logger.debug(f"Discovering stage {git_check_run.name}")
                    self.synchronize_dimension("status", git_check_run.name)
                    self.synchronize_dimension_attribute("status", git_check_run.name, "is_github", "true")
        except Exception as e:
            self.hub.logger.warn(f"Error synchronizing {repository}")
            self.hub.logger.trace(str(e))
            self.hub.logger.trace(traceback.format_exc())

        # if we have consumed a full page, save progress to resume from for next iteration
        task_progress["progress"] = task_progress["progress"] + (task_instances // self.default_page_size) if task_instances == self.default_page_size else task_progress["progress"]
        self.update_task_progress(task_progress)
        self.hub.logger.perf(f"discover_job_stages_for_project {project} done", task_instances, time.monotonic() - start)

    def discover_projects(self):
        '''Discover GitHub projects'''
        self.hub.logger.info("discover_projects started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("github"):
            group = connection.connection_config["properties"]["group"]
            git_group = None
            # map the group to an organization, otherwise, to a user
            try:
                git_group = connection.get_organization(group)
            except Exception as e:  # noqa: F841
                pass
            try:
                git_group = git_group if git_group is not None else connection.get_user(group)
            except Exception as e:
                self.hub.logger.warn(f"Invalid group {group}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
                continue

            git_projects = git_group.get_repos(type="all", sort="full_name", direction="asc")
            for git_project in git_projects:
                if git_project.archived:
                    continue
                self.hub.logger.debug(f"Discovering project {git_project.name}")
                self.synchronize_dimension("project", git_project.name)
                self.synchronize_dimension_attribute("project", git_project.name, "is_github", "true")
                events += 1
        self.hub.logger.perf("discover_projects done", events, time.monotonic() - start)

    def discover_users(self):
        '''Discover GitHub users'''
        self.hub.logger.info("discover_users started")
        start = time.monotonic()
        events = 0
        for connection in self.hub.get_connections_by_type("github"):
            if not self._is_fact_enabled(connection.connection_config, "users"):
                continue
            group = connection.connection_config["properties"]["group"]

            for project in connection.connection_config["properties"]["projects"]:
                repository = f"{group}/{project}"
                # continue from last known progress for this repository
                task_progress = self.get_task_progress("GitHubEtlTask", "discover_users", repository)
                task_instances = 0
                try:
                    git_repository = connection.get_repo(repository)
                    git_repository._requester.per_page = self.default_page_size
                    git_users = git_repository.get_contributors(anon=False).get_page(task_progress["progress"])
                    for git_user in git_users:
                        self.hub.logger.debug(f"Discovering user {git_user.login}")
                        self.synchronize_dimension("user", git_user.login)
                        self.synchronize_dimension_attribute("user", git_user.login, "is_github", "true")
                        task_instances += 1
                        events += 1
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())

                # if we have consumed a full page, save progress to resume from for next iteration
                task_progress["progress"] = task_progress["progress"] + (task_instances // self.default_page_size) if task_instances == self.default_page_size else task_progress["progress"]
                self.update_task_progress(task_progress)
        self.hub.logger.perf("discover_users done", events, time.monotonic() - start)

    def synchronize_builds(self):
        '''Synchronize GitHub workflows as builds'''
        self.hub.logger.info("synchronize_builds started")
        for connection in self.hub.get_connections_by_type("github"):
            if not self._is_fact_enabled(connection.connection_config, "builds"):
                continue
            group = connection.connection_config["properties"]["group"]
            for project in connection.connection_config["properties"]["projects"]:
                repository = f"{group}/{project}"
                try:
                    git_repository = connection.get_repo(repository)
                    git_repository._requester.per_page = self.default_page_size
                    self.synchronize_builds_for_repository(connection, git_repository)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_builds done")

    def synchronize_builds_for_repository(self, connection, git_repository):
        '''Synchronize GitHub builds for repository'''
        self.hub.logger.info(f"synchronize_builds_for_repository {git_repository.full_name} started")
        start = time.monotonic()
        task_instances = 0

        # continue from last known progress for this repository, need to reverse order since github likes to send MRU results
        task_progress = self.get_task_progress("GitHubEtlTask", "synchronize_workflows", f"{git_repository.full_name}")
        git_workflow_runs = git_repository.get_workflow_runs(status="completed")
        current_page = (git_workflow_runs.totalCount // self.default_page_size) - int(task_progress["progress"])
        gwr = git_workflow_runs.get_page(current_page)
        for git_workflow_run in gwr:
            build = self.parse_build(connection, git_repository, git_workflow_run)
            self.synchronize_build(build)
            task_instances += 1

        # if we have consumed a full page, save progress to resume from for next iteration
        task_progress["progress"] = int(task_progress["progress"]) + (task_instances // self.default_page_size) if task_instances >= self.default_page_size else task_progress["progress"]
        self.update_task_progress(task_progress)
        self.hub.logger.perf(f"synchronize_builds_for_repository {git_repository.full_name} done", task_instances, time.monotonic() - start)

    def parse_build(self, connection, git_repository, git_workflow_run):
        '''
        Parse GitHub workflow

        =TODO=
        IMGN-453 builds.artifacts
        IMGN-452 builds.tests
        '''
        stages = stages_pass = stages_fail = stages_skipped = 0
        started_at = ended_at = built_at = tested_at = deployed_at = None
        build_hours = test_hours = deploy_hours = 0
        build_stage_started_at = build_stage_ended_at = None
        test_stage_started_at = test_stage_ended_at = None
        deploy_stage_started_at = deploy_stage_ended_at = None
        # tests_pass = tests_fail = tests_skipped = 0
        # build_slack = test_slack = deploy_slack = 0

        workflow_raw = git_workflow_run.raw_data
        git_check_suite = git_repository.get_check_suite(workflow_raw["check_suite_id"])
        git_check_runs = git_check_suite.get_check_runs()
        for git_check_run in git_check_runs:
            stages += 1
            stages_pass += 1 if git_check_run.conclusion == "success" else 0
            stages_fail += 1 if git_check_run.conclusion != "success" else 0
            started_at = git_check_run.started_at if started_at is None or git_check_run.started_at < started_at else started_at
            ended_at = git_check_run.completed_at if ended_at is None or git_check_run.completed_at > ended_at else ended_at
            check_run_phase = self.hub.helpers["MetaHelper"].map_dimension("status", git_check_run.name)
            if check_run_phase == "Building":
                built_at = git_check_run.completed_at if check_run_phase == "Building" and (built_at is None or git_check_run.completed_at > built_at) else built_at
                build_hours += (git_check_run.completed_at - git_check_run.started_at).total_seconds()
                build_stage_started_at = min(git_check_run.started_at, build_stage_started_at or git_check_run.started_at)
                build_stage_ended_at = max(git_check_run.completed_at, build_stage_ended_at or git_check_run.completed_at)
            elif check_run_phase == "Testing":
                tested_at = git_check_run.completed_at if tested_at is None or git_check_run.completed_at > tested_at else tested_at
                test_hours += (git_check_run.completed_at - git_check_run.started_at).total_seconds()
                test_stage_started_at = min(git_check_run.started_at, test_stage_started_at or git_check_run.started_at)
                test_stage_ended_at = max(git_check_run.completed_at, test_stage_ended_at or git_check_run.completed_at)
            elif check_run_phase == "Deploying":
                deployed_at = git_check_run.completed_at if deployed_at is None or git_check_run.completed_at > deployed_at else deployed_at
                deploy_hours += (git_check_run.completed_at - git_check_run.started_at).total_seconds()
                deploy_stage_started_at = min(git_check_run.started_at, deploy_stage_started_at or git_check_run.started_at)
                deploy_stage_ended_at = max(git_check_run.completed_at, deploy_stage_ended_at or git_check_run.completed_at)

        repository = self.hub.helpers["MetaHelper"].map_dimension("project", git_repository.full_name)
        build = {
            "url": git_workflow_run.html_url,
            "connection": connection.connection_config["id"],
            "repository": repository,
            "branch": git_workflow_run.head_branch[:255],
            "build_type": "",
            "commit_id": git_workflow_run.head_sha,
            "version": self.hub.helpers["SourceCodeHelper"].parse_version(git_workflow_run.head_branch),
            "status": self.hub.helpers["MetaHelper"].map_dimension("status", git_workflow_run.status),
            "component": "",
            "username": None if workflow_raw["actor"] is None else self.hub.helpers["MetaHelper"].map_dimension("user", workflow_raw["actor"]["login"]),
            "created_at": git_workflow_run.created_at,
            "started_at": started_at,
            "built_at": built_at or tested_at or ended_at,
            "tested_at": tested_at,
            "deployed_at": deployed_at,
            "ended_at": ended_at,
            "updated_at": ended_at,
            "artifacts": 0,
            "stages": stages,
            "stages_pass": stages_pass,
            "stages_fail": stages_fail,
            "stages_skipped": stages_skipped,
            "tests": 0,
            "tests_pass": 0,
            "tests_fail": 0,
            "tests_skipped": 0,
            "open_hours": 0 if ended_at is None else (ended_at - git_workflow_run.created_at).total_seconds() / 3600.0,
            "slack": 0,
            "build_hours": build_hours / 3600.0,
            "build_slack": 0,
            "build_duration": 0 if None in [build_stage_started_at, build_stage_ended_at] else (build_stage_ended_at - build_stage_started_at).total_seconds() / 3600.0,
            "test_hours": test_hours / 3600.0,
            "test_slack": 0,
            "test_duration": 0 if None in [test_stage_started_at, test_stage_ended_at] else (test_stage_ended_at - test_stage_started_at).total_seconds() / 3600.0,
            "deploy_hours": deploy_hours / 3600.0,
            "deploy_slack": 0,
            "deploy_duration": 0 if None in [deploy_stage_started_at, deploy_stage_ended_at] else (deploy_stage_ended_at - deploy_stage_started_at).total_seconds() / 3600.0,
            "is_deployed": deploy_hours > 0.0,
            "is_pass": git_workflow_run.conclusion == "success",
            "is_trunk": git_workflow_run.head_branch == git_repository.default_branch,
            "is_tested": False
        }
        build["grade"] = self.hub.helpers["GradingHelper"].get_build_grade(build)
        build["rag"] = self.grade_to_rag_map[build["grade"]]
        return build

    def synchronize_build(self, build):
        '''Synchronize GitHub build'''
        self.hub.logger.debug(f"""Synchronizing build {build["url"]}""")
        isql = """
            INSERT INTO builds(url, connection,
                repository, branch, build_type, commit_id,
                component, version,
                status, rag, grade,
                team, username,
                created_at, started_at, built_at, tested_at, deployed_at, ended_at, updated_at,
                artifacts,
                stages, stages_pass, stages_fail, stages_skipped,
                tests, tests_pass, tests_fail, tests_skipped,
                open_hours, slack,
                build_hours, build_slack, build_duration,
                test_hours, test_slack, test_duration,
                deploy_hours, deploy_slack, deploy_duration,
                is_deployed, is_pass, is_trunk, is_tested)
            VALUES ({{ url }}, {{ connection }},
                {{ repository }}, {{ branch }}, {{ build_type }}, {{ commit_id }},
                {{ component }}, {{ version }},
                {{ status }}, {{ rag }}, {{ grade }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ username }},
                {{ created_at }}, {{ started_at }}, {{ built_at }}, {{ tested_at }}, {{ deployed_at }}, {{ ended_at }}, {{ updated_at }},
                {{ artifacts }},
                {{ stages }}, {{ stages_pass }}, {{ stages_fail }}, {{ stages_skipped }},
                {{ tests }}, {{ tests_pass }}, {{ tests_fail }}, {{ tests_skipped }},
                {{ open_hours }}, {{ slack }},
                {{ build_hours }}, {{ build_slack }}, {{ build_duration }},
                {{ test_hours }}, {{ test_slack }}, {{ test_duration }},
                {{ deploy_hours }}, {{ deploy_slack }}, {{ deploy_duration }},
                {{ is_deployed }}, {{ is_pass }}, {{ is_trunk }}, {{ is_tested }})
            ON CONFLICT (url) DO
            UPDATE SET
                repository = EXCLUDED.repository, branch = EXCLUDED.branch, build_type = EXCLUDED.build_type, commit_id = EXCLUDED.commit_id,
                component = EXCLUDED.component, version = EXCLUDED.version,
                status = EXCLUDED.status, rag = EXCLUDED.rag, grade = EXCLUDED.grade,
                team = EXCLUDED.team, username = EXCLUDED.username,
                created_at = EXCLUDED.created_at, started_at = EXCLUDED.started_at,
                built_at = EXCLUDED.built_at, tested_at = EXCLUDED.tested_at, deployed_at = EXCLUDED.deployed_at,
                ended_at = EXCLUDED.ended_at, updated_at = EXCLUDED.updated_at,
                artifacts = EXCLUDED.artifacts,
                stages = EXCLUDED.stages, stages_pass = EXCLUDED.stages_pass, stages_fail = EXCLUDED.stages_fail, stages_skipped = EXCLUDED.stages_skipped,
                tests = EXCLUDED.tests, tests_pass = EXCLUDED.tests_pass, tests_fail = EXCLUDED.tests_fail, tests_skipped = EXCLUDED.tests_skipped,
                open_hours = EXCLUDED.open_hours, slack = EXCLUDED.slack,
                build_hours = EXCLUDED.build_hours, build_slack = EXCLUDED.build_slack, build_duration = EXCLUDED.build_duration,
                test_hours = EXCLUDED.test_hours, test_slack = EXCLUDED.test_slack, test_duration = EXCLUDED.test_duration,
                deploy_hours = EXCLUDED.deploy_hours, deploy_slack = EXCLUDED.deploy_slack, deploy_duration = EXCLUDED.deploy_duration,
                is_deployed = EXCLUDED.is_deployed, is_pass = EXCLUDED.is_pass, is_trunk = EXCLUDED.is_trunk, is_tested = EXCLUDED.is_tested;"""
        self._upsert(isql, build)

    def synchronize_commits(self):
        '''Synchronize GitHub commits'''
        self.hub.logger.info("synchronize_commits started")
        for connection in self.hub.get_connections_by_type("github"):
            if not self._is_fact_enabled(connection.connection_config, "commits"):
                continue
            group = connection.connection_config["properties"]["group"]
            for project in connection.connection_config["properties"]["projects"]:
                repository = f"{group}/{project}"
                try:
                    git_repository = connection.get_repo(repository)
                    self.synchronize_commits_for_repository(connection, git_repository)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_commits done")

    def synchronize_commits_for_repository(self, connection, git_repository):
        '''Synchronize GitHub commits for repository'''
        self.hub.logger.info(f"synchronize_commits_for_repository {git_repository.full_name} started")
        start = time.monotonic()
        events = 0
        git_repository._requester.per_page = self.default_page_size

        # get commits since last sync
        repository = self.hub.helpers["MetaHelper"].map_dimension("project", git_repository.full_name)
        since = self._get_since("SELECT COALESCE(MAX(started_at) - INTERVAL '2 DAYS', {{ minSyncDate }} ::timestamp) FROM commits WHERE repository = {{ repository }};",
            {"repository": repository, "minSyncDate": self.hub.config["properties"]["minSyncDate"]})
        git_commits = git_repository.get_commits(since=since)
        for git_commit in git_commits:
            try:
                commit = self.parse_commit(connection, git_repository, git_commit)
                self.synchronize_commit(commit)
                self.synchronize_commit_scope(connection, git_repository, git_commit)
                events += 1
            except Exception as e:
                self.hub.logger.warn(f"Error synchronizing {git_repository.full_name}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf(f"synchronize_commits_for_repository {git_repository.full_name} done", events, time.monotonic() - start)

    def parse_commit(self, connection, git_repository, git_commit):
        '''Parse GitHub commit'''
        issue_id = self.hub.helpers["SourceCodeHelper"].parse_issue(git_commit.raw_data["commit"]["message"])
        repository = self.hub.helpers["MetaHelper"].map_dimension("project", git_repository.full_name)
        files_added = files_deleted = files_code = files_test = lines_code = lines_test = 0
        is_trunk = is_unit_tested = False
        languages = []
        unique_filenames = []
        for commit_file in git_commit.files:
            files_added += 1 if commit_file.status == "added" else 0
            files_deleted += 1 if commit_file.status == "removed" else 0

            if self.hub.helpers["SourceCodeHelper"].is_test(commit_file.filename):
                is_unit_tested = True
                files_test += 1 if commit_file.filename not in unique_filenames else 0
                lines_test += commit_file.additions
            elif self.hub.helpers["SourceCodeHelper"].is_code(commit_file.filename):
                languages.append(self.hub.helpers["SourceCodeHelper"].get_language(commit_file.filename))
                files_code += 1 if commit_file.filename not in unique_filenames else 0
                lines_code += commit_file.additions
            unique_filenames.append(commit_file.filename)

        branch = ""
        for pull in git_commit.get_pulls():
            branch = pull.raw_data["base"]["ref"]
            if branch == git_repository.default_branch:
                is_trunk = True

        commit = {
            "url": git_commit.url,
            "connection": connection.connection_config["id"],
            "commit_id": git_commit.sha,
            "repository": repository,
            "branch": branch,
            "language": "" if len(languages) == 0 else max(set(languages), key=languages.count),
            "summary": git_commit.raw_data["commit"]["message"][:255],
            "issue": issue_id if not issue_id.isdigit() else f"{repository}#{issue_id}",
            "status": "Closed",
            "username": git_commit.raw_data["author"]["login"],
            "started_at": git_commit.raw_data["commit"]["author"]["date"].replace("T", " ").replace("Z", ""),
            "ended_at": git_commit.raw_data["commit"]["author"]["date"].replace("T", " ").replace("Z", ""),
            "files": len(unique_filenames),
            "files_added": files_added,
            "files_deleted": files_deleted,
            "files_code": files_code,
            "files_test": files_test,
            "lines_added": git_commit.stats.additions,
            "lines_removed": git_commit.stats.deletions,
            "lines_code": lines_code,
            "lines_test": lines_test,
            "comments": git_commit.raw_data["commit"]["comment_count"],
            "is_merge": git_commit.raw_data["commit"]["message"].find("Merge") > -1,
            "is_trunk": is_trunk,
            "is_unit_tested": is_unit_tested
        }
        commit["grade"] = self.hub.helpers["GradingHelper"].get_commit_grade(commit)
        commit["rag"] = self.grade_to_rag_map[commit["grade"]]
        return commit

    def synchronize_commit(self, commit: dict):
        '''Synchronize GitHub commit'''
        self.hub.logger.debug(f"""Synchronizing build {commit["commit_id"]}""")
        isql = """
            INSERT INTO commits(
                url, connection, commit_id, duplicate_commit_id,
                repository, branch,
                summary,
                status, rag, grade,
                commit_type,
                issue_id, language,
                username, team,
                started_at, ended_at,
                comments,
                files, files_added, files_deleted, files_code, files_test,
                lines_added, lines_removed, lines_code, lines_test,
                is_merge, is_trunk, is_unit_tested,
                is_duplicate)
            VALUES (
                {{ url }}, {{ connection }}, {{ commit_id }},
                COALESCE((SELECT commit_id FROM commits c2 WHERE c2.commit_id <> {{ commit_id }} AND c2.repository = {{ repository }} AND c2.branch = {{ branch }} AND c2.summary = {{ summary }} AND c2.username = {{ username }} AND c2.ended_at < {{ ended_at }} ORDER BY ended_at ASC LIMIT 1), ''),
                {{ repository }}, {{ branch }},
                {{ summary }},
                {{ status }}, {{ rag }}, {{ grade }},
                CASE
                    WHEN COALESCE({{ issue }}, '') <> '' THEN COALESCE((SELECT CASE WHEN issue_type IN ('Bug') THEN 'Maintenance' ELSE 'Creation' END FROM issues WHERE issue_id = {{ issue }} LIMIT 1), 'Unknown')
                    WHEN {{ lines_added }} > 1.25 * {{ lines_removed }} THEN 'Creation'
                    WHEN {{ files_added }} > {{ files_deleted }} THEN 'Creation'
                    WHEN POSITION('fix ' in LOWER({{ summary }})) > 0 THEN 'Maintenance'
                    WHEN POSITION('bug' in LOWER({{ summary }})) > 0 THEN 'Maintenance'
                    WHEN POSITION('defect' in LOWER({{ summary }})) > 0 THEN 'Maintenance'
                    ELSE 'Unknown'
                END,
                {{ issue }}, {{ language }},
                {{ username }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ started_at }}, {{ ended_at }},
                {{ comments }},
                {{ files }}, {{ files_added }}, {{ files_deleted }}, {{ files_code }}, {{ files_test }},
                {{ lines_added }}, {{ lines_removed }}, {{ lines_code }}, {{ lines_test }},
                {{ is_merge }}, {{ is_trunk }}, {{ is_unit_tested }},
                COALESCE((SELECT TRUE FROM commits c2 WHERE c2.commit_id <> {{ commit_id }} AND c2.repository = {{ repository }} AND c2.branch = {{ branch }} AND c2.summary = {{ summary }} AND c2.username = {{ username }} AND c2.ended_at < {{ ended_at }} LIMIT 1), FALSE)
                )
            ON CONFLICT (url) DO
            UPDATE SET
                commit_id = EXCLUDED.commit_id, duplicate_commit_id = EXCLUDED.duplicate_commit_id,
                repository = EXCLUDED.repository, branch = EXCLUDED.branch,
                summary = EXCLUDED.summary,
                status = EXCLUDED.status, rag = EXCLUDED.rag, grade = EXCLUDED.grade,
                commit_type = EXCLUDED.commit_type,
                issue_id = EXCLUDED.issue_id, language = EXCLUDED.language,
                username = EXCLUDED.username, team = EXCLUDED.team,
                started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at,
                comments = EXCLUDED.comments,
                files = EXCLUDED.files, files_added = EXCLUDED.files_added, files_deleted = EXCLUDED.files_deleted, files_code = EXCLUDED.files_code, files_test = EXCLUDED.files_test,
                lines_added = EXCLUDED.lines_added, lines_removed = EXCLUDED.lines_removed, lines_code = EXCLUDED.lines_code, lines_test = EXCLUDED.lines_test,
                is_merge = EXCLUDED.is_merge, is_trunk = EXCLUDED.is_trunk, is_unit_tested = EXCLUDED.is_unit_tested, is_duplicate = EXCLUDED.is_duplicate;"""
        self._upsert(isql, commit)

    def synchronize_commit_scope(self, connection, git_repository, git_commit):
        '''Synchronize GitHub commit scope'''
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO commit_scope(
                commit_url, connection, commit_id, filepath, filename,
                component, language, team,
                line_start, line_end,
                lines_added, lines_removed,
                is_code, is_config, is_test)
            VALUES (
                {{ url }}, {{ connection }}, {{ commit_id }}, {{ filepath }}, {{ filename }},
                {{ component }}, {{ language }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ line_start }}, {{ line_end }},
                {{ lines_added }}, {{ lines_removed }},
                {{ is_code }}, {{ is_config }}, {{ is_test }})
            ON CONFLICT (commit_url, filepath, filename) DO
            UPDATE SET
                commit_id = EXCLUDED.commit_id,
                component = EXCLUDED.component, language = EXCLUDED.language, team = EXCLUDED.team,
                line_start = EXCLUDED.line_start, line_end = EXCLUDED.line_end,
                lines_added = EXCLUDED.lines_added, lines_removed = EXCLUDED.lines_removed,
                is_code = EXCLUDED.is_code, is_config = EXCLUDED.is_config, is_test = EXCLUDED.is_test;"""

        for commit_file in git_commit.files:
            if self.hub.helpers["SourceCodeHelper"].is_excluded(f"{git_repository.full_name}/{commit_file.filename}"):
                continue
            repository = self.hub.helpers["MetaHelper"].map_dimension("project", git_repository.full_name)
            diff_stats = self.hub.helpers["SourceCodeHelper"].parse_diff_stats(commit_file.patch)
            commit_scope = {
                "url": git_commit.url,
                "connection": connection.connection_config["id"],
                "commit_id": git_commit.sha,
                "repository": repository,
                "filepath": os.path.split(commit_file.filename)[0],
                "filename": os.path.split(commit_file.filename)[1],
                "username": git_commit.raw_data["author"]["login"],
                "component": "",
                "language": self.hub.helpers["SourceCodeHelper"].get_language(commit_file.filename),
                "line_start": diff_stats["line_start"],
                "line_end": diff_stats["line_end"],
                "lines_added": commit_file.raw_data["additions"],
                "lines_removed": commit_file.raw_data["deletions"],
                "is_code": self.hub.helpers["SourceCodeHelper"].is_code(commit_file.filename),
                "is_config": self.hub.helpers["SourceCodeHelper"].is_config(commit_file.filename),
                "is_test": self.hub.helpers["SourceCodeHelper"].is_test(commit_file.filename)
            }
            self._upsert(isql, commit_scope)
            events += 1
        self.hub.logger.perf("synchronize_commit_scope done", events, time.monotonic() - start)

    def synchronize_issues(self):
        '''Synchronize GitHub issues'''
        self.hub.logger.info("synchronize_issues started")
        for connection in self.hub.get_connections_by_type("github"):
            if not self._is_fact_enabled(connection.connection_config, "issues"):
                continue
            group = connection.connection_config["properties"]["group"]

            for project in connection.connection_config["properties"]["projects"]:
                repository = f"{group}/{project}"
                try:
                    git_repository = connection.get_repo(repository)
                    self.synchronize_issues_for_repository(connection, git_repository)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_issues done")

    def synchronize_issues_for_repository(self, connection, git_repository):
        '''Synchronize GitHub issues for repository'''
        self.hub.logger.info(f"synchronize_issues_for_repository {git_repository.full_name} started")
        start = time.monotonic()
        events = 0
        # get issues since last sync
        repository = self.hub.helpers["MetaHelper"].map_dimension("project", git_repository.full_name)
        since = self._get_since("SELECT COALESCE(MAX(updated_at) - INTERVAL '2 DAYS', {{ minSyncDate }} ::timestamp) FROM issues WHERE project = {{ repository }};",
            {"repository": repository, "minSyncDate": self.hub.config["properties"]["minSyncDate"]})
        git_issues = git_repository.get_issues(since=since)
        for git_issue in git_issues:
            try:
                issue = self.parse_issue(connection, git_repository, git_issue)
                self.synchronize_issue(issue)
                events += 1
            except Exception as e:
                self.hub.logger.warn(f"Error synchronizing {git_repository.full_name}")
                self.hub.logger.trace(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.perf(f"synchronize_issues_for_repository {git_repository.full_name} done", events, time.monotonic() - start)

    def parse_issue(self, connection, git_repository, git_issue):
        '''Parse GitHub issue'''
        status = self.hub.helpers["MetaHelper"].map_dimension("status", git_issue.state)
        assignee = None if git_issue.assignee is None else git_issue.assignee.login
        closer = None if git_issue.closed_by is None else git_issue.closed_by.login
        labels = "" if git_issue.labels is None else ",".join([d.name for d in git_issue.labels])[:2047]

        issue = {
            "id": f"""{git_repository.name}#{git_issue.number}""",
            "connection": connection.connection_config["id"],
            "url": git_issue.html_url,
            "summary": git_issue.title[:1023],
            "status": status[:63],
            "issue_type": "Task" if labels.lower().find("bug") == -1 else "Bug",
            "project": self.hub.helpers["MetaHelper"].map_dimension("project", git_repository.full_name),
            "priority": "P3",
            "team": self.hub.helpers["MetaHelper"].map_dimension("team", ""),
            "component": self.hub.helpers["MetaHelper"].map_dimension("component", ""),
            "username": None if git_issue.assignee is None else self.hub.helpers["MetaHelper"].map_dimension("user", assignee),
            "reporter": None,
            "closer": None if git_issue.closed_by is None else self.hub.helpers["MetaHelper"].map_dimension("user", closer),
            "created_at": git_issue.created_at,
            "started_at": git_issue.created_at,
            "ended_at": None if git_issue.closed_at is None else git_issue.closed_at,
            "due_at": None,
            "updated_at": git_issue.last_modified,
            "resolution": "Done" if status == "Done" else "",
            "labels": labels,
            "estimate": 0,
            "effort": 0,
            "is_noise": False
        }
        return issue

    def synchronize_issue(self, issue: dict):
        '''Synchronize issue'''
        self.hub.logger.debug(f"""Synchronizing issue {issue["id"]}""")
        isql = """
            INSERT INTO issues(url, connection, issue_id, summary, status, issue_type,
                project, priority, team, component,
                username, reporter, closer,
                created_at, started_at, ended_at, due_at, updated_at,
                resolution, labels,
                effort_estimate, effort,
                is_noise)
            VALUES ({{ url }}, {{ connection }}, {{ id }}, {{ summary }}, {{ status }}, {{ issue_type }},
                {{ project }}, {{ priority }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ project }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ component }},
                {{ username }}, {{ reporter }}, {{ closer }},
                {{ created_at }}, {{ created_at }}, {{ ended_at }},
                {{ due_at }}, {{ updated_at }},
                {{ resolution }}, {{ labels }},
                {{ estimate }}, {{ effort }},
                {{ is_noise }})
            ON CONFLICT (url) DO
            UPDATE SET
                issue_id = EXCLUDED.issue_id, summary = EXCLUDED.summary, status = EXCLUDED.status, issue_type = EXCLUDED.issue_type,
                project = EXCLUDED.project, priority = EXCLUDED.priority, team = EXCLUDED.team, component = EXCLUDED.component,
                username = EXCLUDED.username, reporter = EXCLUDED.reporter, closer = EXCLUDED.closer,
                created_at = EXCLUDED.created_at, started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at,
                due_at = EXCLUDED.due_at, updated_at = EXCLUDED.updated_at,
                resolution = EXCLUDED.resolution, labels = EXCLUDED.labels,
                effort_estimate = EXCLUDED.effort_estimate, effort = EXCLUDED.effort,
                is_noise = EXCLUDED.is_noise;"""
        self._upsert(isql, issue)

    def synchronize_reviews(self):
        '''Synchronize GitHub Pull Requests'''
        self.hub.logger.info("synchronize_reviews started")
        for connection in self.hub.get_connections_by_type("github"):
            if not self._is_fact_enabled(connection.connection_config, "reviews"):
                continue
            group = connection.connection_config["properties"]["group"]
            for project in connection.connection_config["properties"]["projects"]:
                repository = f"{group}/{project}"
                try:
                    git_repository = connection.get_repo(repository)
                    self.synchronize_reviews_for_repository(connection, git_repository)
                except Exception as e:
                    self.hub.logger.warn(f"Error synchronizing {repository}")
                    self.hub.logger.trace(str(e))
                    self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_reviews done")

    def synchronize_reviews_for_repository(self, connection, git_repository):
        self.hub.logger.info(f"synchronize_reviews_for_repository {git_repository.full_name} started")
        start = time.monotonic()
        # continue from last known progress for this repository
        task_progress = self.get_task_progress("GitHubEtlTask", "synchronize_reviews", f"{git_repository.full_name}")
        task_instances = 0
        git_repository._requester.per_page = self.default_page_size

        git_pull_requests = git_repository.get_pulls(state="all", sort="updated", direction="asc").get_page(task_progress["progress"])
        for git_pull_request in git_pull_requests:
            review = self.parse_review(connection, git_repository, git_pull_request)
            # synchronize_review_feedback is done from within parse_review for efficiency
            self.synchronize_review(review)
            self.synchronize_review_scope(connection, git_pull_request)
            task_instances += 1

        # if we have consumed a full page, save progress to resume from for next iteration
        task_progress["progress"] = task_progress["progress"] + (task_instances // self.default_page_size) if task_instances == self.default_page_size else task_progress["progress"]
        self.update_task_progress(task_progress)
        self.hub.logger.perf(f"synchronize_reviews_for_repository {git_repository.full_name} done", task_instances, time.monotonic() - start)

    def parse_review(self, connection, git_repository, git_pull_request):
        '''Parse GitHub pull request'''
        languages = []
        component = ""
        assigned_at = work_started_at = implemented_at = None
        is_new = is_unit_tested = False
        files_code = files_test = lines_code = lines_test = reviewers = upvotes = downvotes = 0

        for git_commit in git_pull_request.get_commits():
            commit_date = datetime.strptime(git_commit.raw_data["commit"]["author"]["date"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
            work_started_at = min(commit_date, work_started_at or commit_date)
            implemented_at = max(commit_date, work_started_at or commit_date)

        unique_filenames = []
        for change in git_pull_request.get_files():
            is_new = is_new or change.status == "added"
            if self.hub.helpers["SourceCodeHelper"].is_test(change.filename):
                is_unit_tested = True
                files_test += 1 if change.filename not in unique_filenames else 0
                lines_test += change.additions
            elif self.hub.helpers["SourceCodeHelper"].is_code(change.filename):
                languages.append(self.hub.helpers["SourceCodeHelper"].get_language(change.filename))
                files_code += 1 if change.filename not in unique_filenames else 0
                lines_code += change.additions
            unique_filenames.append(change.filename)

        unique_commenters = []
        for comment in git_pull_request.get_comments():
            assigned_at = comment.created_at if assigned_at is None else min(assigned_at, comment.created_at)
            upvotes += comment.raw_data["reactions"]["+1"]
            downvotes += comment.raw_data["reactions"]["-1"]
            if comment.user is not None and comment.user.login not in unique_commenters:
                unique_commenters.append(comment.user.login)
                reviewers += 1
        review = {
            "url": git_pull_request.html_url,
            "connection": connection.connection_config["id"],
            "review_id": git_pull_request.number,
            "repository": self.hub.helpers["MetaHelper"].map_dimension("project", git_repository.full_name[:255]),
            "source_branch": git_pull_request.raw_data["head"]["ref"],
            "branch": git_pull_request.raw_data["base"]["ref"],
            "summary": git_pull_request.title[:255],
            "status": self.hub.helpers["MetaHelper"].map_dimension("status", git_pull_request.state),
            "issue": self.hub.helpers["SourceCodeHelper"].parse_issue(f"{git_pull_request.title}\n{git_pull_request.body}"),
            "username": self.hub.helpers["MetaHelper"].map_dimension("user", git_pull_request.user.login),
            "component": component,
            "language": "" if len(languages) == 0 else max(set(languages), key=languages.count),
            "labels": "" if git_pull_request.labels is None else ",".join([d.name for d in git_pull_request.labels])[:2047],
            "work_started_at": None if work_started_at is None else work_started_at.strftime("%Y-%m-%d %H:%M:%S"),
            "implemented_at": None if implemented_at is None else implemented_at.strftime("%Y-%m-%d %H:%M:%S"),
            "created_at": git_pull_request.created_at,
            "started_at": git_pull_request.created_at,
            "assigned_at": assigned_at,
            "ended_at": git_pull_request.merged_at or git_pull_request.closed_at,
            "updated_at": git_pull_request.updated_at,
            "upvotes": upvotes,
            "downvotes": downvotes,
            "comments": git_pull_request.review_comments,
            "feedback": 0,
            "reviewers": reviewers,
            "uploads": git_pull_request.commits,
            "files": git_pull_request.changed_files,
            "files_code": files_code,
            "files_test": files_test,
            "lines_added": git_pull_request.additions,
            "lines_removed": git_pull_request.deletions,
            "lines_code": lines_code,
            "lines_test": lines_test,
            "review_effort": 0,
            "is_open": False if self.hub.helpers["MetaHelper"].map_dimension("status", git_pull_request.state) == "Closed" else True,
            "is_draft": git_pull_request.draft or git_pull_request.title.lower().find("wip") > -1 or git_pull_request.title.lower().find("draft") > -1,
            "is_fork": False if git_pull_request.raw_data["head"]["repo"] is None else git_pull_request.raw_data["head"]["repo"]["fork"],
            "is_new": is_new,
            "is_noise": not git_pull_request.merged,
            "is_squashed": False,
            "is_trunk": git_pull_request.raw_data["base"]["ref"] == git_repository.default_branch,
            "is_unit_tested": is_unit_tested
        }

        # process MR feedback to simplify calculation logic
        review = self.synchronize_review_feedback(git_pull_request, review)
        review["grade"] = self.hub.helpers["GradingHelper"].get_review_grade(review)
        review["rag"] = self.grade_to_rag_map[review["grade"]]
        return review

    def synchronize_review(self, review: dict):
        '''Synchronize GitHub pull request'''
        self.hub.logger.debug(f"""Synchronizing review {review["review_id"]}""")
        isql = """
            INSERT INTO reviews(url, connection,
                review_id, repository, source_branch, branch,
                summary, status, rag, grade,
                username, issue_id,
                team, component, language, labels,
                work_started_at, implemented_at,
                created_at, assigned_at,
                started_at, reviewed_at, approved_at,
                ended_at, updated_at,
                upvotes, downvotes, comments, feedback,
                defects,
                uploads, files, files_code, files_test,
                lines_added, lines_removed, lines_code, lines_test,
                reviewers, review_effort,
                open_hours, active_hours, review_hours, slack,
                is_open, is_draft, is_fork, is_new, is_noise,
                is_squashed, is_trunk, is_unit_tested,
                is_approved, is_pre_approved, has_feedback)
            SELECT
                {{ url }}, {{ connection }},
                {{ review_id }}, {{ repository }}, {{ source_branch }}, {{ branch }},
                {{ summary }}, {{ status }}, {{ rag }}, {{ grade }},
                {{ username }}, {{ issue }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'project' AND attribute_type = 'team' AND dimension = {{ repository }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'component' AND attribute_type = 'team' AND dimension = {{ component }} AND COALESCE(attribute, '') <> ''),
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    '') AS team,
                {{ component }}, {{ language }}, {{ labels }},
                {{ work_started_at }}, {{ implemented_at }},
                {{ created_at }}, {{ assigned_at }},
                COALESCE(MIN(CASE WHEN crf.feedback_type IN ('Approval', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END), {{ created_at }}::timestamp) AS started_at,
                MAX(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN crf.started_at ELSE NULL END) AS reviewed_at,
                MAX(CASE WHEN crf.feedback_type IN ('Approval') THEN crf.started_at ELSE NULL END) AS approved_at,
                {{ ended_at }}, {{ updated_at }},
                {{ upvotes }}, {{ downvotes }}, {{ comments }}, {{ feedback }},
                SUM(CASE WHEN crf.feedback_type IN ('Bug') THEN 1 ELSE 0 END) AS defects,
                {{ uploads }}, {{ files }}, {{ files_code }}, {{ files_test }},
                {{ lines_added }}, {{ lines_removed }}, {{ lines_code }}, {{ lines_test }},
                {{ reviewers }},
                {{ review_effort }},
                EXTRACT(epoch FROM LEAST({{ ended_at }}::timestamp, now()) - {{ created_at }}::timestamp) / 3600 AS open_hours,
                COALESCE(EXTRACT(epoch FROM
                    MAX(CASE WHEN crf.feedback_type IN ('Approved', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END) -
                    MIN(CASE WHEN crf.feedback_type IN ('Approved', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END)) / 3600, 0) AS active_hours,
                COALESCE(EXTRACT(epoch FROM
                    MAX(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN crf.started_at ELSE NULL END) -
                    MIN(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN crf.started_at ELSE NULL END)) / 3600, 0) AS review_hours,
                COALESCE(EXTRACT(epoch FROM
                    MIN(CASE WHEN crf.feedback_type IN ('Approval', 'Bug', 'Comment') THEN crf.started_at ELSE NULL END) -
                    {{ created_at }}::timestamp) / 3600, 0) AS slack,
                {{ is_open }}, {{ is_draft }}, {{ is_fork }}, {{ is_new }}, {{ is_noise }},
                {{ is_squashed }}, {{ is_trunk }}, {{ is_unit_tested }},
                SUM(CASE WHEN crf.feedback_type IN ('Approval') THEN 1 ELSE 0 END) > 0 AS is_approved,
                SUM(CASE WHEN crf.feedback_type IN ('Approval') AND crf.started_at >= {{ implemented_at }}::timestamp THEN 1 ELSE 0 END) > 0 AS is_preapproved,
                SUM(CASE WHEN crf.feedback_type IN ('Bug', 'Comment') THEN 1 ELSE 0 END) > 0 AS has_feedback
            FROM
                (SELECT {{ url }} AS url) AS x
                LEFT OUTER JOIN review_feedback crf ON
                    x.url = crf.url
            GROUP BY x.url, crf.url
            ON CONFLICT (url) DO
            UPDATE SET
                review_id = EXCLUDED.review_id, repository = EXCLUDED.repository, source_branch = EXCLUDED.source_branch, branch = EXCLUDED.branch,
                summary = EXCLUDED.summary, status = EXCLUDED.status, rag = EXCLUDED.rag, grade = EXCLUDED.grade,
                username = EXCLUDED.username, issue_id = EXCLUDED.issue_id,
                team = EXCLUDED.team, component = EXCLUDED.component, language = EXCLUDED.language, labels = EXCLUDED.labels,
                work_started_at = EXCLUDED.work_started_at, implemented_at = EXCLUDED.implemented_at, created_at = EXCLUDED.created_at,
                assigned_at = EXCLUDED.assigned_at, started_at = EXCLUDED.started_at, reviewed_at = EXCLUDED.reviewed_at,
                approved_at = EXCLUDED.approved_at, ended_at = EXCLUDED.ended_at, updated_at = EXCLUDED.updated_at,
                upvotes = EXCLUDED.upvotes, downvotes = EXCLUDED.downvotes, comments = EXCLUDED.comments, feedback = EXCLUDED.feedback,
                defects = EXCLUDED.defects,
                uploads = EXCLUDED.uploads, files = EXCLUDED.files, files_code = EXCLUDED.files_code, files_test = EXCLUDED.files_test,
                lines_added = EXCLUDED.lines_added, lines_removed = EXCLUDED.lines_removed, lines_code = EXCLUDED.lines_code, lines_test = EXCLUDED.lines_test,
                reviewers = EXCLUDED.reviewers, review_effort = EXCLUDED.review_effort,
                open_hours = EXCLUDED.open_hours, active_hours = EXCLUDED.active_hours, review_hours = EXCLUDED.review_hours, slack = EXCLUDED.slack,
                is_open = EXCLUDED.is_open, is_draft = EXCLUDED.is_draft, is_fork = EXCLUDED.is_fork, is_new = EXCLUDED.is_new, is_noise = EXCLUDED.is_noise,
                is_squashed = EXCLUDED.is_squashed, is_trunk = EXCLUDED.is_trunk, is_unit_tested = EXCLUDED.is_unit_tested,
                is_approved = EXCLUDED.is_approved, is_pre_approved = EXCLUDED.is_pre_approved, has_feedback = EXCLUDED.has_feedback;"""
        self._upsert(isql, review)

    def synchronize_review_feedback(self, pull_request, review: dict):
        '''Synchronize GitHub pull request feedback'''
        self.hub.logger.debug("synchronize_review_feedback started")
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO review_feedback(url, connection, status, feedback_type,
                username, role,
                started_at, ended_at,
                is_open)
            VALUES({{ url }}, {{ connection }}, {{ feedback_status }}, {{ feedback_type }},
                {{ feedback_username }}, {{ feedback_role }},
                {{ feedback_started }}, {{ feedback_ended }},
                {{ feedback_is_open }});"""

        # delete & re-create for simplicity
        self.database.execute(f"""DELETE FROM review_feedback WHERE url = '{pull_request.html_url}';""")
        self.database.commit()

        # process code review feedback
        started_at = None
        feedback = reviewer_count = 0
        unique_reviewers = []
        author = pull_request.user.login
        for comment in pull_request.get_comments():
            reviewer = "Anonymous" if comment.user is None else self.hub.helpers["MetaHelper"].map_dimension("user", comment.user.login)
            feedback += 1 if reviewer != review["username"] else 0
            if reviewer not in unique_reviewers:
                unique_reviewers.append(reviewer)
            if started_at is None or started_at > comment.created_at:
                started_at = comment.created_at
            review.update({
                "feedback_started": comment.created_at,
                "feedback_ended": comment.created_at,
                "feedback_role": "Reviewer" if author != reviewer else "Author",
                "feedback_status": "Closed",
                "feedback_type": "Approval" if "approve" in comment.raw_data["body"] else "Comment",
                "feedback_username": self.hub.helpers["MetaHelper"].map_dimension("user", reviewer),
                "feedback_is_open": False
            })
            self._upsert(isql, review)
            events += 1

        # process approvals
        for git_review in pull_request.get_reviews():
            reviewer = "Anonymous" if git_review.user is None else git_review.user.login
            feedback_type = "Comment"
            if git_review.state == "APPROVAL":
                feedback_type = "Approval"
            elif git_review.state == "CHANGES REQUESTED":
                feedback_type = "Bug"
            review.update({
                "feedback_started": git_review.submitted_at,
                "feedback_ended": git_review.last_modified,
                "feedback_role": "Reviewer" if author != reviewer else "Author",
                "feedback_status": "Closed",
                "feedback_type": feedback_type,
                "feedback_username": self.hub.helpers["MetaHelper"].map_dimension("user", reviewer),
                "feedback_is_open": False
            })
            self._upsert(isql, review)
            events += 1

        if started_at is not None:
            review["started_at"] = started_at
        review["reviewers"] = reviewer_count
        review["feedback"] = feedback
        self.hub.logger.perf("synchronize_review_feedback done", events, time.monotonic() - start)
        return review

    def synchronize_review_scope(self, connection, git_pull_request):
        '''Synchronize GitHub pull request scope'''
        start = time.monotonic()
        events = 0
        isql = """
            INSERT INTO review_scope(url, connection, review_id, commit_id, scoped_at, implemented_at)
            VALUES ({{ url }}, {{ connection }}, {{ review_id }}, {{ id }}, {{ scoped_at }}, {{ implemented_at }})
            ON CONFLICT (url, commit_id) DO NOTHING;"""
        for git_commit in git_pull_request.get_commits():
            review_scope = {
                "id": git_commit.sha,
                "connection": connection.connection_config["id"],
                "url": git_pull_request.html_url,
                "review_id": str(git_pull_request.number),
                "scoped_at": git_pull_request.created_at,
                "implemented_at": datetime.strptime(git_commit.raw_data["commit"]["author"]["date"][0:19].replace("T", " "), "%Y-%m-%d %H:%M:%S")
            }
            self._upsert(isql, review_scope)
            events += 1
        self.hub.logger.perf("synchronize_review_scope done", events, time.monotonic() - start)
