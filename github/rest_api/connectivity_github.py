from argparse import Namespace
from flask_apispec import doc, marshal_with, use_kwargs
from core.hub import Hub
from ext.shared.GitHubConnection import GitHubConnection
from api.core_api import BaseResource
from api.core_model import BaseResponseSchema
from api.connectivity import ConnectionTestRequestSchema


args = Namespace(config_file="config.json")
hub = Hub.get_instance(args, "Flask")


class GitHubTest(BaseResource):
    @doc(description="GitHub Connnection Test", tags=["connectivity"])
    @use_kwargs(ConnectionTestRequestSchema, location="query")
    @marshal_with(BaseResponseSchema)
    def put(self, **kwargs):
        http_response_code = 202
        success = True

        try:
            tester = GitHubConnection(hub, kwargs)  # noqa: F841
        except Exception as e:  # noqa: F841
            http_response_code = 401
            success = False
        return {"value": success}, http_response_code


# register API
hub.helpers["flask_api"].add_resource(GitHubTest, "/api/latest/config/connectivity/test/github")
hub.helpers["swagger"].register(GitHubTest)
