from datetime import datetime
import traceback
from ImagineOpsBaseTask import ImagineOpsBaseTask


class Office365EtlTask(ImagineOpsBaseTask):
    '''
    Office365EtlTask synchronizes calendar data from Microsoft Outlook
    '''
    def __init__(self, hub):
        super(Office365EtlTask, self).__init__(hub)
        self.app_name = "imagineops_office365"

    ###########################################################################
    # task_runner.BaseTask API
    ###########################################################################

    def do_discovery(self):
        pass

    def do_daily(self):
        self.do_discovery()
        self.synchronize_calendars()

    def do_test(self):
        self.do_discovery()
        self.do_daily()

    ###########################################################################
    # Tasks
    ###########################################################################

    def synchronize_calendars(self):
        '''Synchronize calendars
        TODO:
            subscribe to more than 1 calendar
        '''
        self.hub.logger.info("synchronize_calendars started")
        for office365_connection in self.hub.get_connections_by_type("office365"):
            if not self._is_fact_enabled(office365_connection.connection_config, "meetings"):
                continue
            try:
                office365_schedule = office365_connection.account.schedule()
                office365_calendar = office365_schedule.get_default_calendar()
                self.synchronize_calendar(office365_connection, office365_calendar)
            except Exception as e:
                self.hub.logger.warn("Error synchronizing")
                self.hub.logger.debug(str(e))
                self.hub.logger.trace(traceback.format_exc())
        self.hub.logger.info("synchronize_calendars done")

    def synchronize_calendar(self, office365_connection, office365_calendar):
        since = self._get_since("SELECT COALESCE(MAX(started_at), {{ minSyncDate }}) FROM meetings WHERE url LIKE '%microsoft%';",
            {"minSyncDate": self.hub.config["properties"]["minSyncDate"]})
        event_query = office365_calendar.new_query("start").greater_equal(since)
        event_query.chain("and").on_attribute("end").less_equal(datetime.now())
        office365_events = office365_calendar.get_events(query=event_query, limit=999, include_recurring=True, order_by="start", download_attachments=False)
        for office365_event in office365_events:
            meeting = self.parse_calendar_event(office365_calendar, office365_event)
            self.synchronize_meeting(meeting)

    def parse_calendar_event(self, office365_calendar, office365_event):
        meeting = {
            "url": f"https://graph.microsoft.com/v1.0/events/{office365_event.object_id}",
            "summary": office365_event.subject()[:255],
            "username": office365_calendar.owner,
            "organizer": office365_event.organizer().address(),
            "location": office365_event.location(),
            "importance": office365_event.importance(),
            "event_response": office365_event.response_status().status,
            "attendance_type": None,
            "sensitivity": office365_event.sensitivity(),
            "availability": office365_event.show_as(),
            "created_at": office365_event.created(),
            "started_at": office365_event.start(),
            "ended_at": office365_event.end(),
            "updated_at": office365_event.modified(),
            "duration": (office365_event.end() - office365_event.start()).total_seconds() / 3600.0,
            "invited": len(office365_event.attendees()),
            "required_attendees": sum(map(lambda x: x.attendee_type() in ["required"], office365_event.attendees())),
            "confirmed_attendees": sum(map(lambda x: x.response_status().status in ["accepted", "organizer"], office365_event.attendees())),
            "is_all_day": office365_event.is_all_day(),
            "is_busy": office365_event.show_as() in ["busy", "oof", "workingElsewhere"],
            "is_cancelled": office365_event.is_cancelled,
            "is_private": office365_event.sensitivity() in ["private", "confidential"],
            "is_personal": office365_event.sensitivity() in ["personal"],
            "is_organizer": office365_event.is_organizer,
            "is_online": office365_event.is_online_meeting(),
            "is_recurring": office365_event.event_type() in ["seriesMaster", "occurrence"]
        }
        return meeting

    def synchronize_meeting(self, meeting: dict):
        self.hub.logger.debug(f"""Synchronizing meeting {meeting["summary"]}""")
        isql = """
            INSERT INTO meetings(url,
                summary, organizer, username, team,
                location, importance, event_response,
                attendance_type, sensitivity, availability,
                created_at, started_at, ended_at, updated_at,
                duration, invited, required_attendees, confirmed_attendees,
                is_all_day, is_busy, is_cancelled,
                is_private, is_personal, is_organizer,
                is_online, is_recurring)
            VALUES ({{ url }},
                {{ summary }}, {{organizer }}, {{ username }},
                COALESCE(
                    (SELECT attribute FROM dimension_attributes WHERE dimension_type = 'user' AND attribute_type = 'team' AND dimension = {{ username }} AND COALESCE(attribute, '') <> ''),
                    ''),
                {{ location }}, {{ importance }}, {{ event_response }},
                {{ attendance_type }}, {{ sensitivity }}, {{ availability }},
                {{ created_at }}, {{ started_at }}, {{ ended_at }}, {{ updated_at }},
                {{ duration }}, {{ invited }}, {{ required_attendees }}, {{ confirmed_attendees }},
                {{ is_all_day }}, {{ is_busy }}, {{ is_cancelled }},
                {{ is_private }}, {{ is_personal }}, {{ is_organizer }},
                {{ is_online }}, {{ is_recurring }})
            ON CONFLICT (url) DO
            UPDATE SET
                summary = EXCLUDED.summary, organizer = EXCLUDED.organizer, username = EXCLUDED.username, team = EXCLUDED.team,
                location = EXCLUDED.location, importance = EXCLUDED.importance, event_response = EXCLUDED.event_response,
                attendance_type = EXCLUDED.attendance_type, sensitivity = EXCLUDED.sensitivity, availability = EXCLUDED.availability,
                created_at = EXCLUDED.created_at, started_at = EXCLUDED.started_at, ended_at = EXCLUDED.ended_at, updated_at = EXCLUDED.updated_at,
                duration = EXCLUDED.duration, invited = EXCLUDED.invited, required_attendees = EXCLUDED.required_attendees, confirmed_attendees = EXCLUDED.confirmed_attendees,
                is_all_day = EXCLUDED.is_all_day, is_busy = EXCLUDED.is_busy, is_cancelled = EXCLUDED.is_cancelled,
                is_private = EXCLUDED.is_private, is_personal = EXCLUDED.is_personal, is_organizer = EXCLUDED.is_organizer,
                is_online = EXCLUDED.is_online, is_recurring = EXCLUDED.is_recurring;"""
        # upsert
        self._upsert(isql, meeting)
