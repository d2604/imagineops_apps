from __future__ import annotations
from O365 import Account, MSGraphProtocol


class Office365Connection(MSGraphProtocol):
    '''
    Office365 Connection
    https://pietrowicz-eric.medium.com/how-to-read-microsoft-outlook-calendars-with-python-bdf257132318
    '''
    def __init__(self, hub, connection_config):
        self.hub = hub
        self.rag = "R"
        self.connection_config = connection_config
        self.api_access = ["Calendars.Read.Shared"]
        self.hub.logger.info("Connecting to Office365")
        super(Office365Connection, self).__init__()
        credentials = (connection_config["username"], connection_config["password"])
        self.account = Account(credentials, protocol=self)
        self.check_connection(on_init=True)

    ###########################################################################
    # Public API
    ###########################################################################

    def check_connection(self, on_init=False):
        '''
        Determines connection status
            R = unable to connect (bad username/password, etc)
            A = connection isnt reliable (rate limiting, etc)
            G = good
        '''
        success = self.account.authenticate(scopes=self.api_access)
        self.rag = "G" if success else "R"
        return success
