/* global $, BaseConnection, dashboardManager */
class Office365Connection extends BaseConnection
{
    edit()
    {
        this.setDefaults();
        let html = this.getDefaultEditHtml();
        html += `
        <h5>Advanced</h5>
        <div id="connectivityAccordion" class="accordion accordion-flush">
    
        <div class="accordion-item">
            <h2 id="connectivityAdvanced" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityAdvancedConfig" aria-expanded="false" aria-controls="connectivityAdvancedConfig">
            Calendars
            </button>
            </h2>
            <div id="connectivityAdvancedConfig" class="accordion-collapse collapse" aria-labelledby="connectivityAdvanced" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <select id="calendars" class="form-select" multiple aria-label="Calendars">
                </select>
            </div>
            </div>
        </div>
    
        <div class="accordion-item">
            <h2 id="connectivityFactsHeader" class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#connectivityFactsConfig" aria-expanded="false" aria-controls="connectivityFactsConfig">
            Facts
            </button>
            </h2>
            <div id="connectivityFactsConfig" class="accordion-collapse collapse" aria-labelledby="connectivityFactsHeader" data-bs-parent="#connectivityAccordion">
            <div class="accordion-body">
                <div class="form-check"><input id="do_meetings" class="form-check-input" type="checkbox" value="" ${(this.connection.properties.do_meetings ? "checked" : "")}>
                <label class="form-check-label" for="do_meetings">Meetings</label>
                </div>
            </div>
            </div>
        </div>
        </div>`;
        return html;
    }

    populateOptions()
    {
        this.connection.properties.do_meetings = document.getElementById("do_meetings").checked;
    }

    setDefaults()
    {
        if (!Object.prototype.hasOwnProperty.call(this.connection, "properties")) this.connection.properties = {};
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "calendars")) this.connection.properties.group = [];
        if (!Object.prototype.hasOwnProperty.call(this.connection.properties, "do_meetings")) this.connection.properties.do_meetings = false;
        return this.connection;
    }

    update()
    {
        return this.connection;
    }
}
dashboardManager.connections.office365 = { class: Office365Connection };
